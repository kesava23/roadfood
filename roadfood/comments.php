<?php

// Do not delete these lines
if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
	die ('Please do not load this page directly. Thanks!');

if ( post_password_required() ) {
	?> <p><?php _e('This post is password protected. Enter the password to view comments.', 'base'); ?></p> <?php
	return;
}
	
function theme_comment($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment; ?>
	
	<div class="discuss-item">
		<div <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
			<div class="discuss__item-head">			
				<div class="discuss__item-avatar bg-parent js-image-fit">
					<?php echo get_avatar( $comment, 48 ); ?>
				</div>
				<div class="discuss__item-meta">
					<h6 class="discuss__item-name"><?php comment_author_link(); ?></h6><!-- /.discuss__item-name -->

					<p class="discuss__item-date"><?php comment_date('F jS, Y'); ?></p><!-- /.discuss__item-date -->
				</div>
			</div>
			<div class="disscuss-item-body">
				<?php if ($comment->comment_approved == '0') : ?>
				<p><?php _e('Your comment is awaiting moderation.', 'base'); ?></p>
				<?php else: ?>
				<?php comment_text(); ?>
				<?php endif; ?>

				<?php
					comment_reply_link(array_merge( $args, array(
						'reply_text' => __('Reply', 'base'),
						'before' => '<p>',
						'after' => '</p>',
						'depth' => $depth,
						'max_depth' => $args['max_depth']
					))); ?>
			</div>		
			
		</div>
	<?php }
	
	function theme_comment_end() { ?>
		</div>
	<?php }
?>


<?php if ( comments_open() ) : ?>

<div class="form-discuss" id="respond">
	<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform">
				
			<div class="cancel-comment-reply"><?php cancel_comment_reply_link(); ?></div>
		
			<?php if ( get_option('comment_registration') && !is_user_logged_in() ) : ?>
			<p class="comment-author-link">You must be <a href="<?php echo wp_login_url( get_permalink() ); ?>"><?php _e('logged in', 'base'); ?></a> <?php _e('to post a comment.', 'base'); ?></p>
			<?php else : ?>
		
			<?php if ( is_user_logged_in() ) : ?>
		
			<p class="comment-author-link"><?php _e('Logged in as', 'base'); ?> <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>. <a href="<?php echo wp_logout_url(get_permalink()); ?>" title="<?php _e('Log out of this account', 'base'); ?>"><?php _e('Log out &raquo;', 'base'); ?></a></p>
				<?php
				   global $current_user;
				   get_currentuserinfo();     
				?>
			<div class="form-row-comment">	
				<div class="form__avatar bg-parent js-image-fit bg-parent">
					<? echo get_avatar( $current_user->ID, 64 ); ?>
				</div>
			
				<div class="form__body">
					<dl><dd class="mb-0">
						<textarea name="comment" id="comment" class="field expand" placeholder="Add your comment here"></textarea>
					</dd></dl>
				</div>
				<div class="form__actions">
					<input name="submit" type="submit" id="submit" value="<?php _e('Send', 'base'); ?>" class="form__btn" />				
				</div>

			<?php else : ?>
			   <div class="form-row-comment ">	
					<div class="form__avatar bg-parent js-image-fit bg-parent">
						<? echo get_avatar( $current_user->ID, 64 ); ?>
					</div>
					<div class="form__body">
						<dl>
						<dd class="mb-0">
							<textarea name="comment" id="comment" class="field expand" placeholder="Add your comment here"></textarea>
						</dd>

						</dl>
					</div>
					<div class="form__actions">
						<input name="submit" type="submit" id="submit" value="<?php _e('Send', 'base'); ?>" class="form__btn" />				
					</div>
					<div class="form__additional">
						<dl>
							<dt><label for="author"><?php _e('Name', 'base'); ?></label></dt>
						<dd class="mb-3"><input type="text" name="author" placeholder="Add your name here" id="author" value="<?php echo esc_attr($comment_author); ?>" class="field" /></dd>
						<dt><label for="email"><?php _e('E-Mail (will not be published)', 'base'); ?></label></dt>
						<dd><input type="text" name="email" placeholder="Add your email here" id="email" value="<?php echo esc_attr($comment_author_email); ?>" class="field" /></dd>
						</dl>
					</div>
			<?php endif; ?>

				
		</div>	
		<?php
			comment_id_fields();
			do_action('comment_form', $post->ID);
		?>

		<?php endif; ?>
		
	</form>
</div>

<?php endif; ?>

<?php if ( have_comments() ) : ?>

<div class="section comments" id="comments">

	<p><b><?php comments_number(__('No Responses', 'base'), __('One Response', 'base'), __('% Responses', 'base') );?> <?php _e('to', 'base'); ?> &#8220;<?php the_title(); ?>&#8221;</b></p>

	<div class="discuss">
		<?php wp_list_comments(array(
			'callback' => 'theme_comment',
			'end-callback' => 'theme_comment_end',
			'reverse_top_level' => true
			)); ?>
	</div>

	<div class="navigation">
		<div class="next"><?php previous_comments_link(__('&laquo; Older Comments', 'base')) ?></div>
		<div class="prev"><?php next_comments_link(__('Newer Comments &raquo;', 'base')) ?></div>
	</div>

</div>

 <?php else : // this is displayed if there are no comments so far ?>

	<?php if ( comments_open() ) : ?>
		<!-- If comments are open, but there are no comments. -->

	 <?php else : // comments are closed ?>
		<!-- If comments are closed. -->
		<p><?php _e('Comments are closed.', 'base'); ?></p>

	<?php endif; ?>
	
<?php endif; ?>
	
	
<?php if ( is_user_logged_in() ) : ?>
<script>
(function($){
	$('textarea.expand').focus(function () {
		$(this).animate({ height: "10em" }, 500); 
	});
	 $('textarea.expand').blur(function () {
        $(this).animate({
            height: "40px"
        }, 500);       
    });
})(jQuery)
</script>
<?php else: ?>	
<script>
(function($){
	$('textarea.expand').focus(function () {
		$(this).animate({ height: "10em" }, 500); 
		$('.form__additional').show('slow');
	});
	 $('textarea.expand').blur(function () {
        $(this).animate({
            height: "40px"
        }, 500); 
		
    });
	
})(jQuery)
</script>	
	
<?php endif; ?>
	
<script>
(function($){
	discuss = $('.discuss');
	$.each(discuss, function(key,value) {
		$(this)
		  .find('.discuss-item')
		  .slice(3)
		  .addClass('hidden-discuss');
	});
	$('.js-show-more-discuss').click(function(){
		 console.log('test');
	  $( ".hidden-discuss" ).slideDown( "slow", function() {
		$(this).text('Show Less');
		 
	  });
	});
})(jQuery)
</script>