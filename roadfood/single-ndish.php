<?php get_header(); ?>
<?php if(have_posts()): while(have_posts()): the_post();?>
<div class="main">
	<div class="section-breadcrumb">
		<div class="container">
			<?php if(function_exists('bcn_display_list')):?>
			<nav aria-label="breadcrumb" typeof="BreadcrumbList" vocab="https://schema.org/">			    
			    <ol class="breadcrumb">
			    	<?php bcn_display_list();?>
				</ol>
			</nav>
			<?php endif;?>
		</div><!-- /.container -->
	</div><!-- /.section-breadcrumb -->
	<section class="section-title">
		<div class="container">
			<h1><?php the_title();?></h1>
		</div><!-- /.container -->
	</section><!-- /.section-title -->

	<div class="section-dish-single">
		<div class="container">
			<div class="section__inner">
				<div class="section__image">
					<?php echo do_shortcode('[instagram-feed user="smashballoon"]');?>
					<!-- <img src="assets/images/temp/section-dish-single.jpg" alt=""> -->
				</div><!-- /.section__image -->

				<div class="section__actions">
					<a href="#" class="btn btn--orange-border">Show More</a>
				</div><!-- /.section__actions -->
			</div><!-- /.section__inner -->
		</div><!-- /.container -->
	</div><!-- /.section-dish-single -->

	<?php if(get_field('advertisement_widget', 'options')):?>
	<section class="section-banner-mobile d-block d-lg-none">
		<?php the_field('advertisement_widget', 'options')?>
	</section><!-- /.section-banner-mobile -->
	<?php endif;?>

	<section class="section-base-layout">
		<div class="container">
			<div class="row">
				<div class="col-lg-8">
					<div class="section__content">
						<section class="section-plain-text">
							<div class="section__entry">
								<?php echo get_continue_reading()?>											
							</div><!-- /.section__entry -->

							<div class="section__actions">
								<a href="#" class="read-more">Continue Reading</a>
							</div><!-- /.section__actions -->
						</section><!-- /.section-plain-text -->
						<?php 
							$restaurants_ids_arr = get_restaurants_with_dish();
							if(count($restaurants_ids_arr)):
							$restaurants = new WP_Query(array(
								'post_type' 	=> 'restaurants',
								'showposts'		=> 6,
								'post__in'		=> $restaurants_ids_arr,
								'orderby'		=> 'rand', 
							));
							if($restaurants->have_posts()):
						?>
						<section class="section-base">
							<header class="section__head">
								<h2>Restaurants With This Dish</h2>
							</header><!-- /.section__head -->

							<div class="section__body">
								<div class="boxes">
									<div class="row row-small-margin">
										<?php while($restaurants->have_posts()): $restaurants->the_post();?>
										<div class="col-md-4 col-6 col-small-padding">
											<div class="box">
												<div class="box__inner">
													<a href="<?php the_permalink();?>" class="box__link"></a>
													<div class="box__image bg-parent js-image-fit">
														<?php if (has_post_thumbnail()){the_post_thumbnail('restaurant-thumbnail', array('class'=>'bg-image'));}?>
													</div><!-- /.box__image -->
													<div class="box__content">
														<h5><?php the_title();?></h5>
														<?php if(get_field('restaurant_city') || get_field('restaurant_state')):?>
														<p><?php echo strtoupper(get_field('restaurant_city')); if(get_field('restaurant_city') && get_field('restaurant_state')){ echo', ';} echo strtoupper(get_field('restaurant_state'))?></p>
														<?php endif;?>
													</div><!-- /.box__content -->
												</div><!-- /.box__inner -->
											</div><!-- /.box -->
										</div><!-- /.col-md-4 -->
										<?php endwhile;?>
									</div><!-- /.row -->
								</div><!-- /.boxes -->
							</div><!-- /.section__body -->

							<div class="section__actions">
								<a href="/restaurants/" class="btn btn--with-shape">
									<span>More Restaurants</span>

									<svg width="27px" height="20px" viewBox="0 0 27 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="Asset-1" class="svg-color" fill="#FFFFFF" fill-rule="nonzero"><path d="M1.76,10.8 L14.76,13.8 L14.76,18.6 C14.7663086,18.9826391 14.9904405,19.3281223 15.3372831,19.4898403 C15.6841256,19.6515583 16.0928461,19.6011474 16.39,19.36 L26.2,10.54 C26.4186854,10.3500696 26.5442752,10.0746496 26.5442752,9.785 C26.5442752,9.4953504 26.4186854,9.21993042 26.2,9.03 L16.4,0.25 C16.1042499,0.00985886916 15.6977449,-0.0413773288 15.3516561,0.117865966 C15.0055674,0.27710926 14.7800248,0.619164918 14.77,1 L14.77,5.8 L1.77,8.8 C1.21771526,8.7972386 0.76776146,9.24271526 0.764987267,9.795 C0.762238613,10.3472847 1.20771526,10.7972386 1.76,10.8 Z" id="Path"></path></g></g></svg>
								</a>
							</div><!-- /.section__actions -->
						</section>
						<?php 
							endif; 
							wp_reset_postdata();
							$recipes = new WP_Query(array(
								'post_type' 	=> 'recipe',
								'showposts'		=> 4,
								'orderby'		=> 'rand',
								'meta_key'		=> 'restaurant',
								'meta_value'	=> $restaurants_ids_arr,
								'meta_compare'	=> 'IN',
							));
							if($recipes->have_posts()): 
						?>
						<section class="section-base">
							<header class="section__head">
								<h2>Frito Pie Recipes</h2>

								<p>Lorem ipsum dolar sit amet non torda con polos a sin casa</p>
							</header><!-- /.section__head -->

							<div class="section__body">
								<div class="boxes-type-1">
									<div class="row">
										<?php while($recipes->have_posts()): $recipes->the_post();?>
										<div class="col-md-3 col-6">
											<div class="box">
												<div class="box__inner">
													<a href="<?php the_permalink()?>" class="box__link"></a>

													<div class="box__image bg-parent js-image-fit">
														<?php if (has_post_thumbnail()){
															the_post_thumbnail('recipe-thumbnail', array('class'=>'bg-image'));
														} else {
															$images = get_children( 'post_parent='.get_the_ID().'&post_type=attachment&post_mime_type=image&orderby=menu_order&order=ASC' );
															if (count($images)){
																echo wp_get_attachment_image( array_key_first($images), 'recipe-thumbnail', '', array('class'=>'bg-image') ); 
															} else {
																echo '<img src="/wp-content/uploads/2020/11/no-image-icon.jpg" alt="" class="bg-image shareaholic-media-target-hover-state" style="border: 1px solid #ddd;">';
															}
														}?>
													</div><!-- /.box__image -->
													<div class="box__content">
														<p><?php the_title();?></p>
													</div><!-- /.box__content -->
												</div><!-- /.box__inner -->
											</div><!-- /.box -->
										</div><!-- /.col-md-4 -->
										<?php endwhile;?>
									</div><!-- /.row -->
								</div><!-- /.boxes -->
							</div><!-- /.section__body -->

							<div class="section__actions">
								<a href="/recipes/" class="btn btn--with-shape">
									<span>More Frito Pie Recipes</span>

									<svg width="27px" height="20px" viewBox="0 0 27 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="Asset-1" class="svg-color" fill="#FFFFFF" fill-rule="nonzero"><path d="M1.76,10.8 L14.76,13.8 L14.76,18.6 C14.7663086,18.9826391 14.9904405,19.3281223 15.3372831,19.4898403 C15.6841256,19.6515583 16.0928461,19.6011474 16.39,19.36 L26.2,10.54 C26.4186854,10.3500696 26.5442752,10.0746496 26.5442752,9.785 C26.5442752,9.4953504 26.4186854,9.21993042 26.2,9.03 L16.4,0.25 C16.1042499,0.00985886916 15.6977449,-0.0413773288 15.3516561,0.117865966 C15.0055674,0.27710926 14.7800248,0.619164918 14.77,1 L14.77,5.8 L1.77,8.8 C1.21771526,8.7972386 0.76776146,9.24271526 0.764987267,9.795 C0.762238613,10.3472847 1.20771526,10.7972386 1.76,10.8 Z" id="Path"></path></g></g></svg>
								</a>
							</div><!-- /.section__actions -->
						</section>
						<?php endif; wp_reset_postdata();?>
						<?php endif?>
						<section class="section-gallery section-base">
							<div class="section__inner">
								<div class="section__head">
									<h2>More Frito Pie Photos & Videos</h2>
								</div><!-- /.section__head -->

								<div class="section__image">
									<a href="#" class="section__image-link"></a>

									<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/temp/section-gallery-main-image.jpg" alt="">
								</div><!-- /.section__image -->

								<div class="section__aside">
									<div class="section__grid">
										<div class="section__grid-item">
											<a href="#" class="section__grid-item-bg bg-parent js-image-fit">
												<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/temp/grid-item-1.jpg" alt="" class="bg-image">
											</a>
										</div><!-- /.section__grid-item -->

										<div class="section__grid-item">
											<a href="#" class="section__grid-item-bg bg-parent js-image-fit">
												<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/temp/grid-item-2.jpg" alt="" class="bg-image">
											</a>
										</div><!-- /.section__grid-item -->

										<div class="section__grid-item">
											<a href="#" class="section__grid-item-bg bg-parent js-image-fit">
												<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/temp/grid-item-3.jpg" alt="" class="bg-image">
											</a>
										</div><!-- /.section__grid-item -->

										<div class="section__grid-item">
											<a href="#" class="section__grid-item-bg bg-parent js-image-fit">
												<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/temp/grid-item-4.jpg" alt="" class="bg-image">
											</a>
										</div><!-- /.section__grid-item -->

										<div class="section__grid-item">
											<a href="#" class="section__grid-item-bg bg-parent js-image-fit">
												<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/temp/grid-item-5.jpg" alt="" class="bg-image">
											</a>
										</div><!-- /.section__grid-item -->

										<div class="section__grid-item">
											<a href="#" class="section__grid-item-bg bg-parent js-image-fit">
												<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/temp/grid-item-6.jpg" alt="" class="bg-image">
											</a>
										</div><!-- /.section__grid-item -->
									</div><!-- /.section__grid -->

									<div class="section__actions">
										<a href="#" class="btn btn--orange">View Gallery</a>

										<a href="#" class="btn btn--orange-border">Submit Your Own</a>
									</div><!-- /.section__actions -->
								</div><!-- /.section__aside -->
							</div><!-- /.section__inner -->
						</section><!-- /.section-gallery -->

						<section class="section-base">
							<header class="section__head">
								<h2>Discuss</h2>

								<p>What do you think of <?php the_title(); ?>?</p>
							</header><!-- /.section__head -->

							<div class="section__body">
								<?php comments_template(); ?>
								
							</div><!-- /.section__body -->


							<div class="section__actions">
								<a href="javascript:;" class="btn-show-more js-show-more-discuss">Show More</a>
							</div><!-- /.section__actions -->
						</section><!-- /.section-base -->



					</div><!-- /.section__content -->
				</div><!-- /.col-md-8 -->
				<?php if(get_field('advertisement_widget', 'options')):?>
				<div class="col-lg-4 d-none d-lg-block">
					<div class="section__sidebar">
						<ul class="widgets">
							<li class="widget widget--banner">
								<?php the_field('advertisement_widget', 'options')?>
							</li><!-- /.widget widget--banner -->
						</ul><!-- /.widgets -->
					</div><!-- /.section__sidebar -->
				</div><!-- /.col-md-4 -->
				<?php endif;?>
			</div><!-- /.row -->
		</div><!-- /.container -->
	</section><!-- /.section-base-layout -->
	<?php $articles = new WP_Query(array(
		'post_type' => 'post',
		'showposts' => 6,
	));
	if($articles->have_posts()):?>
	<section class="section-articles">
		<div class="container">
			<header class="section__head">
				<h2>Latest Articles & Guides</h2>
			</header><!-- /.section__head -->
			<div class="section__body">
				<div class="articles">
					<div class="row">
						<?php while($articles->have_posts()): $articles->the_post();?>
						<div class="col-md-4 col-6 ">
							<div class="article">
								<div class="article__inner">
									<div class="article__image bg-parent js-image-fit">
										<a href="<?php the_permalink();?>" class="article__image-link"></a>
										<?php if (has_post_thumbnail()){
											the_post_thumbnail('post-thumbnail', array('class'=>'bg-image'));
										} else {
											$images = get_children( 'post_parent='.get_the_ID().'&post_type=attachment&post_mime_type=image&orderby=menu_order&order=ASC' );
											if (count($images)){
												echo wp_get_attachment_image( array_key_first($images), 'post-thumbnail', '', array('class'=>'bg-image') ); 
											} 
										}?>
									</div><!-- /.article__image -->

									<div class="article__contnet">
										<div class="article__meta">
											<p><?php the_category(', ');?></p>
										</div><!-- /.article__meta -->

										<div class="article__title">
											<a href="<?php the_permalink();?>"><?php the_title();?></a>
										</div><!-- /.article__title -->
										<?php if ( !empty( get_the_content() ) ): ?>
										<div class="article__entry">											
											<?php echo get_excerpt(); ?>											
										</div><!-- /.article__entry -->
										<?php endif; ?>
									</div><!-- /.article__contnet -->
								</div><!-- /.article__inner -->
							</div><!-- /.article -->
						</div><!-- /.col-md-4 -->
						<?php endwhile;?>
					</div><!-- /.row -->
				</div><!-- /.articles -->
			</div><!-- /.section__body -->
		</div><!-- /.container -->
	</section><!-- /.section-articles -->
	<?php endif; wp_reset_postdata();?>


	<?php  

	$post_id = get_the_ID();

	echo '<pre style="display: none">';
	var_dump( $author_id=$post->post_author );
	var_dump( get_the_author_meta( 'ID' ) );
	var_dump( get_post_meta( $post_id ) );
	echo '</pre>';

	?>
</div><!-- /.main -->
<?php endwhile; endif;?>
<?php get_footer(); ?>