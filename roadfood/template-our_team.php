<?php
/*
Template Name: Our Team Template
*/
get_header(); ?>
<div class="main">
	<section class="section-breadcrumb">
		<div class="container">
			<?php
				if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('
					<div id="breadcrumbs">','</div>');
				}	
			?>
		</div>		
	</section>
	<?php if (have_posts()) : ?>
	<?php while (have_posts()) : the_post(); ?>
		<section class="section-heading">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-8  text-center">
						<h1 class="text-capitalize"><?php the_title(); ?></h1>

						<p><?php echo get_field('sub-heading'); ?></p>
					</div><!-- /.col-lg-8 -->
				</div><!-- /.row -->
			</div><!-- /.container -->
		</section>
		<section class="section-fullwidth-image">
			<?php the_post_thumbnail('full'); ?>
		</section>
		<section class="section-small-text">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-8">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
		</section>
		<section class="section-members">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-8">
						<div class="section__head">
							<h2>Writers / Contributors</h2>
						</div><!-- /.section__head -->

						<div class="section__body">
							<div class="row">
								<?php 
								$rows = get_field('team_list');
								if( $rows ):
									foreach( $rows as $row ):
									?>								
									<div class="col-md-3 col-6">
										<div class="member">
											<div class="member__image bg-parent js-image-fit">												
												<?php  $image = $row['image']; ?>
												<?php if($row['url']): ?>
													<a href="<?php echo $row['url']; ?>">
													<img src="<?php echo $row['image']['url']; ?>" alt="<?php echo $row['image']['alt']; ?>" />
													</a>
												<?php else: ?>
													<img src="<?php echo $row['image']['url']; ?>" alt="<?php echo $row['image']['alt']; ?>" />
												<?php endif; ?>
												
												
											</div><!-- /.member__image -->

											<div class="member__name">												
												<?php if($row['url']): ?>
													<p><a href="<?php echo $row['url']; ?>" class="text-decoration-none">
														<?php echo $row['name']; ?>
													</a></p>
												<?php else: ?>
													<p><?php echo $row['name']; ?></p>
												<?php endif; ?>
											</div><!-- /.member__name -->
										</div><!-- /.member -->
									</div><!-- /.col-lg-3 col-md-6 -->
									<?php	
									endforeach;
								endif;
								?>
								
							</div><!-- /.row -->
						</div><!-- /.section__body -->
					</div><!-- /.col-lg-8 -->
				</div><!-- /.row -->
			</div><!-- /.container -->
		</section>
		<section class="section-cta">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-8">
						<div class="row align-items-center text-center">
							<?php 
							$rows = get_field('cta_list');
							if( $rows ):
								foreach( $rows as $row ):
							?>
							<div class="col-md-6">
								<div class="col__inner">
									<h2><?php echo $row['sub-heading']; ?></h2>
									<p><?php echo $row['short_text']; ?></p>
									<a href="<?php echo $row['button_url']; ?>" class="btn"><?php echo $row['button_text']; ?></a>
								</div><!-- /.col__inner -->
							</div><!-- /.col-md-6 -->
							<?php 
								endforeach;
							endif;
							?>
						
							
						</div><!-- /.row -->
					</div><!-- /.col-lg-8 -->
				</div><!-- /.row -->
			</div><!-- /.container -->
		</section>
	<?php endwhile; ?>
	<?php endif; ?>
</div>
<?php get_footer(); ?>