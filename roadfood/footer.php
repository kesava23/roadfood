				<?php if( get_field('facebook', 'option') || get_field('instagram', 'option') || get_field('pinterest', 'option') || get_field('twitter', 'option') ):?>
				<section class="section-socials">
					<div class="container">
						<div class="row">
							<div class="col-sm-12">
									<div class="socials">
										<ul>
											<?php if( get_field('facebook', 'option') ):?>
											<li>
												<a href="<?php the_field('facebook', 'option')?>">
													<i class="fab fa-facebook-f"></i>
												</a>
											</li>
											<?php endif;?>
											<?php if( get_field('instagram', 'option') ):?>
											<li>
												<a href="<?php the_field('instagram', 'option')?>">
													<i class="fab fa-instagram"></i>
												</a>
											</li>
											<?php endif;?>
											<?php if( get_field('pinterest', 'option') ):?>
											<li>
												<a href="<?php the_field('pinterest', 'option')?>">
													<i class="fab fa-pinterest"></i>
												</a>
											</li>
											<?php endif;?>
											<?php if( get_field('twitter', 'option') ):?>
											<li>
												<a href="<?php the_field('twitter', 'option')?>">
													<i class="fab fa-twitter"></i>
												</a>
											</li>
											<?php endif;?>
										</ul>
										<p>
											Connect with us <a href="#">#Roadfood</a>
										</p>
									</div><!-- /.socials -->
							</div><!-- /.col-sm-12 -->
						</div><!-- /.row -->
					</div><!-- /.container -->
				</section><!-- /.section-socials -->
				<!-- /.main -->
				<?php endif;?>
				<footer class="footer">
					<div class="footer__inner">
						<div class="container">
							<div class="footer__content">
								<div class="footer__logo">
									<a href="<?php echo site_url();?>" class="logo">
										<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/logo-footer.svg" alt="">
									</a>
								</div><!-- /.footer__logo -->

								<div class="footer__nav">
									<?php 
									wp_nav_menu( array('container' => false,
										 'theme_location' => 'footer_explore',
										 'menu_id' => '',
										 'menu_class' => '',
										 'items_wrap' => '<div class="footer__nav-item"><p class="footer__title">Explore</p><ul id="%1$s" class="%2$s">%3$s</ul></div>',
										 ) );
									wp_nav_menu( array('container' => false,
										 'theme_location' => 'footer_learn_more',
										 'menu_id' => '',
										 'menu_class' => '',
										 'items_wrap' => '<div class="footer__nav-item"><p class="footer__title">Learn More</p><ul id="%1$s" class="%2$s">%3$s</ul></div>',
										 ) );
									?>
								</div><!-- /.footer__nav -->
								<div class="footer__form">
									<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/logo-icon.svg" alt="" class="footer__form-icon">

									<?php if(have_rows('newsletter', 'option')):?>
										<?php if(get_field('newsletter_heading', 'option')):?>
											<p class="footer__title"><?php the_field('newsletter_heading', 'option')?></p><!-- /.footer__title -->
										<?php endif;?>
										<?php if(get_field('newsletter_body', 'option')):?>
											<p><?php the_field('newsletter_body', 'option')?></p>
										<?php endif;?>
									<?php endif;?>
								</div><!-- /.footer__form -->
							</div><!-- /.footer__content -->
						</div><!-- /.container -->
					</div><!-- /.footer__inner -->
					<div class="footer__bar">
						<div class="container">
							<div class="footer__bar-inner">
								<div class="copyright">
									<p>&copy;<?php echo date('Y')?> Roadfood by <a href="https://fexy.com">Fexy Media</a></p>
								</div><!-- /.copyright -->
								<?php wp_nav_menu( array('container' => false,
									 'theme_location' => 'footer_privacy_policy',
									 'menu_id' => '',
									 'menu_class' => '',
									 'items_wrap' => '<nav class="footer__bar-nav"><ul id="%1$s" class="%2$s">%3$s</ul></nav>',
									 ) );?>								
							</div><!-- /.footer__bar-inner -->
						</div><!-- /.container -->
					</div><!-- /.footer__bar -->
				</footer><!-- /.footer -->
			</div><!-- /.wrapper__inner -->
		</div><!-- /.wrapper -->
		<?php wp_footer(); ?>
	</body>
</html>