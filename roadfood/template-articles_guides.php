<?php
/*
Template Name: Articles & Guides Template
*/
get_header(); ?>
<div class="main">
	<section class="section-breadcrumb">
		<div class="container">
			<?php
				if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('
					<div id="breadcrumbs">','</div>');
				}	
			?>
		</div>		
	</section>
	
	<?php if (have_posts()) : ?>
	<?php while (have_posts()) : the_post(); ?>
		<section class="section-heading">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-8  text-center">
						<h1 class="text-uppercase"><?php the_title(); ?></h1>

						<p><?php echo get_field('sub-heading'); ?></p>
					</div><!-- /.col-lg-8 -->
				</div><!-- /.row -->
			</div><!-- /.container -->
		</section>
	
		<?php if(get_field('advertisement_widget', 'options')):?>
		<section class="section-banner-mobile d-block d-lg-none">
			<?php the_field('advertisement_widget', 'options')?>							
		</section>
		<?php endif;?>
	
		<?php if (!empty( get_the_content())): ?>
		<section class="section-small-text">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-8">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
		</section>
		<?php endif; ?>
	
		<section class="section-base-layout section-base-layout--with-border">
			<div class="container">
				<div class="row">
					<div class="col-lg-8">
						<div class="section__content">
							<section class="section-articles-featured">
								<header class="section__head">
									<h2>Featured</h2>
								</header><!-- /.section__head -->
							
								<div class="section__body">
									<div class="row">
										<?php
										$posts = get_field('featured_post');
										if( $posts ):
											foreach( $posts as $post):										
											?>											
											<div class="col-12 col-sm-6">
												<article class="article-featured">
													<div class="article__image bg-parent js-image-fit">
														<a href="<?php the_permalink(); ?>" class="article__image-link"></a>
														<?php if(has_post_thumbnail()){ ?>
															<img src="<?php echo the_post_thumbnail_url('Medium'); ?>" alt="" class="bg-image">
														<?php }else{ ?>
															<img src="/wp-content/uploads/2020/11/no-image-icon.jpg" alt="" class="bg-image" style="border: 1px solid #ddd;">
														<?php } ?>														
													</div><!-- /.article__image -->

													<div class="article__content">
														<h3 class="article__title">
															<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
														</h3><!-- /.article__title -->

														<div class="article__entry">
															<?php echo excerpt(30); ?>
														</div><!-- /.article__entry -->
													</div><!-- /.article__content -->
												</article><!-- /.article-featured -->
											</div><!-- /.col-6 -->
											<?php
											endforeach;
										endif;
										?>	
										
										
									</div><!-- /.row -->
								</div><!-- /.section__body -->
							</section><!-- /.section-articles-featured -->

							<section class="section-boxes-cta">
								<header class="section__head">
									<h2>Roadfood Columns</h2>
								</header><!-- /.section__head -->

								<div class="section__body">
									<div class="row">
										<div class="col-6">
											<div class="box-cta">
												<a href="<?php echo site_url(); ?>/tours/" class="box__cta-link"></a>

												<div class="box__cta-image bg-parent js-image-fit">
													<img src="/wp-content/uploads/2020/11/box-cta-1.jpg" alt="" class="bg-image">
												</div><!-- /.box__cta-image -->

												<div class="box__cta-title">
													<h5>Tours</h5>
												</div><!-- /.box__cta-title -->
											</div><!-- /.box-cta -->
										</div><!-- /.col-6 -->

										<div class="col-6">
											<div class="box-cta">
												<a href="<?php echo site_url(); ?>/bests/" class="box__cta-link"></a>

												<div class="box__cta-image bg-parent js-image-fit">
													<img src="/wp-content/uploads/2020/11/box-cta-2.jpg" alt="" class="bg-image">
												</div><!-- /.box__cta-image -->

												<div class="box__cta-title">
													<h5>Best Of</h5>
												</div><!-- /.box__cta-title -->
											</div><!-- /.box-cta -->
										</div><!-- /.col-6 -->

										<div class="col-6">
											<div class="box-cta">
												<a href="<?php echo site_url(); ?>/gourmet/" class="box__cta-link"></a>

												<div class="box__cta-image bg-parent js-image-fit">
													<img src="/wp-content/uploads/2020/11/box-cta-3.jpg" alt="" class="bg-image">
												</div><!-- /.box__cta-image -->

												<div class="box__cta-title">
													<h5>Gourmet</h5>
												</div><!-- /.box__cta-title -->
											</div><!-- /.box-cta -->
										</div><!-- /.col-6 -->

										<div class="col-6">
											<div class="box-cta">
												<a href="<?php echo site_url(); ?>/janes-diary/" class="box__cta-link"></a>

												<div class="box__cta-image bg-parent js-image-fit">
													<img src="/wp-content/uploads/2020/11/box-cta-4.jpg" alt="" class="bg-image">
												</div><!-- /.box__cta-image -->

												<div class="box__cta-title">
													<h5>Jane’s Diary</h5>
												</div><!-- /.box__cta-title -->
											</div><!-- /.box-cta -->
										</div><!-- /.col-6 -->
									</div><!-- /.row -->
								</div><!-- /.section__body -->
							</section><!-- /.section-boxes-cta -->

							<section class="section-articles-default">
								<div class="row">
								<?php
								$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
								 $args = array(
									'post_type' => array('post'),
									'posts_per_page' => 21,
									'orderby' => 'date',
									'order'   => 'DESC',
									'post_status' => 'publish',
									'paged' => $paged
								);
								$query = new WP_Query( $args );								
								?>
									<?php if ($query->have_posts()) : ?>
										<?php while ($query->have_posts()) : $query->the_post(); 
											
										?>
											<div class="col-lg-4 col-6">
												<article class="article-default">
													<div class="article__image bg-parent js-image-fit">
														<a href="<?php the_permalink(); ?>" class="article__image-link"></a>
														<?php if(has_post_thumbnail()){ ?>
															<img src="<?php echo the_post_thumbnail_url('medium'); ?>" alt="" class="bg-image">
														<?php }else if(get_field('dish_image')){
															$images = get_field('dish_image'); 
															if(is_array($images)){
																$g_image = $images[0];
															}else{
																$g_image = $images;
															}
														?>
															<img src="<?php echo esc_url(wp_get_attachment_image_src($g_image, 'thumbnail')[0]); ?>" alt="" class="bg-image">
														<?php }else{ ?>
															<img src="/wp-content/uploads/2020/11/no-image-icon.jpg" alt="" class="bg-image" style="border: 1px solid #ddd;">
														<?php } ?>				
													</div><!-- /.article__image -->

													<div class="article__content">
														<h3>
															<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
														</h3>
														<?php if ( !empty( get_the_content() ) ): ?>
														<?php echo get_excerpt(); ?>
														<?php endif; ?>
													</div><!-- /.article__content -->
												</article><!-- /.article-default -->
											</div><!-- /.col-md-4 -->
										<?php endwhile; ?>	
									<?php endif; ?>
									<?php wp_reset_query(); ?>   
									
								</div><!-- /.row -->

								<div class="section__actions">
									<a href="javascript:;" class="btn-show-more js-loadmore-article">Show More</a>
								</div>
							</section><!-- /.section-articles-default -->
						</div><!-- /.section__content -->
					</div><!-- /.col-md-8 -->

					<?php if(get_field('advertisement_widget', 'options')):?>
						<div class="col-lg-4 d-none d-lg-block">
							<div class="section__sidebar">
								<ul class="widgets">
									<li class="widget widget--banner">
										<?php the_field('advertisement_widget', 'options')?>
									</li><!-- /.widget widget--banner -->
								</ul><!-- /.widgets -->
							</div><!-- /.section__sidebar -->
						</div><!-- /.col-md-4 -->
					<?php endif;?>
					
				</div><!-- /.row -->
			</div><!-- /.container -->
		</section>
		
	<?php endwhile; ?>
	<?php endif; ?>
</div>
<?php get_footer(); ?>