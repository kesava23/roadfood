<?php get_header(); ?>
 <?php
	$name = "Unkown";
    $curauth = (isset($_GET['author_name'])) ? get_user_by('slug', $author_name) : get_userdata(intval($author));
	if(empty($curauth->display_name)){
		$name = $curauth->nickname;
	}else{
		$name = $curauth->display_name;
	}
 ?>
<div class="main">
	<section class="section-breadcrumb">
		<div class="container">		
			<div id="breadcrumbs"><span><span><a href="<?php echo site_url(); ?>">Home</a> / <a href="<?php echo site_url(); ?>/about">About</a> / <a href="<?php echo site_url(); ?>/our-team">Team</a> / <span class="breadcrumb_last" aria-current="page"><?php echo $name; ?></span></span></span></div>
		</div>		
	</section>
	
	<section class="sectiom-member-single">
		<div class="container">
			<div class="row">
				<div class="col-md-4">
					<div class="section__image">
						<img src="<?php echo get_avatar_url($curauth->user_email); ?>" alt="<?php echo $name; ?>" class="w-100">
					</div><!-- /.section__image -->
				</div><!-- /.col-md-4 -->

				<div class="col-md-8">
					<div class="section__content">
						<h1 class="text-capitalize"><?php echo $name; ?></h1>
						<p class="mb-0">
							<?php echo $curauth->user_description; ?>
						</p>
						
					</div><!-- /.section__content -->
				</div><!-- /.col-md-8 -->
			</div><!-- /.row -->
		</div><!-- /.container -->
	</section>
	

	<section class="section-articles">
		<div class="container">
			<header class="section__head">
				<h2>Contributions</h2>
			</header><!-- /.section__head -->

			<div class="section__body">
				<div class="articles">
					<div class="row">
					<?php if (have_posts()) : ?>	
						<?php while (have_posts()) : the_post(); ?>	
						<div class="col-md-4 col-6 ">
							<div class="article">
								<div class="article__inner">
									<div class="article__image bg-parent js-image-fit">
										<a href="<?php the_permalink(); ?>" class="article__image-link"></a>
										<?php if(!empty(get_the_post_thumbnail())){ ?>
											<img src="<?php echo the_post_thumbnail_url('Medium'); ?>" alt="" class="bg-image">
										<?php }else{ ?>
											<img src="/wp-content/uploads/2020/11/no-image-icon.jpg" alt="" class="bg-image">
										<?php } ?>
									</div><!-- /.article__image -->

									<div class="article__contnet">
										<div class="article__meta">
											<p ><?php _e('Posted in', 'base'); ?> <?php the_category(', ') ?> </p>
											
										</div><!-- /.article__meta -->

										<div class="article__title">
											<a href="<?php the_permalink() ?>" rel="bookmark" title="<?php _e('Permanent Link to', 'base'); ?> <?php the_title_attribute(); ?>"><?php the_title(); ?></a>
										</div><!-- /.article__title -->

										<div class="article__entry">
											<?php the_excerpt(); ?>
										</div><!-- /.article__entry -->
									</div><!-- /.article__contnet -->
								</div><!-- /.article__inner -->
							</div><!-- /.article -->
						</div><!-- /.col-md-4 -->
						<?php endwhile; ?>
					<?php else : ?>
						<?php
							header("HTTP/1.1 301 Moved Permanently");
							header("Location: ".get_bloginfo('url')."/404");
							exit();
						?>
<!-- 					<div class="post">
						<div class="col-sm-12">						
							<div class="title">
								<h1><?php _e('No Result Found', 'base'); ?></h1>
							</div>
							<div class="content">
								<p><?php _e('Sorry, but you are looking for something that isn\'t here.', 'base'); ?></p>
							</div>
						</div>
					</div> -->
					<?php endif; ?>
						
					</div><!-- /.row -->
				</div><!-- /.articles -->
			</div><!-- /.section__body -->
		</div><!-- /.container -->
	</section>

<?php get_footer(); ?>
