<?php 
if ( function_exists( 'add_theme_support' ) ) {
	add_theme_support( 'post-thumbnails' );
	set_post_thumbnail_size( 50, 50, true ); // Normal post thumbnails
	add_image_size( 'dish-type-thumb', 66, 66, true );
	add_image_size( 'dishes-thumbnail', 110, 110, true );
	add_image_size( 'recipe-thumbnail', 165, 165, true );
	add_image_size( 'restaurant-thumbnail', 241, 150, true );
	add_image_size( 'post-thumbnail', 360, 252, true );
	add_image_size( 'single-post-thumbnail', 400, 500, true );
	add_image_size( 'hero-thumb', 2600, 601, true );
}

function get_roadfood_post_thumbnail($size='full', $additiona_class=''){
	global $post;

	$thumb = '<img src="'.get_stylesheet_directory_uri().'/images/no-image-icon.jpg" alt="" class="'.$additiona_class.'">';
	
	if(has_post_thumbnail()){
		$thumb = get_the_post_thumbnail('', $size, array('class'=>$additiona_class));
	} else {
		$images = &get_children( 'post_parent='.get_the_ID().'&post_type=attachment&post_mime_type=image&orderby=menu_order&order=ASC' );
		if (count($images)){
			$thumb = wp_get_attachment_image( array_key_first($images), $size, '', array('class' => $additiona_class));
		}
	}
	return $thumb;
}

function get_roadfood_taxonomy_term_thumbnail($term='', $size='full', $additiona_class=''){

	$thumb = '<img src="'.get_stylesheet_directory_uri().'/images/no-image-icon.jpg" alt="" class="'.$additiona_class.'">';
	
	if(get_field('featured_image', $term)){
		$thumb = wp_get_attachment_image( get_field('featured_image', $term), $size, '', array('class'=>$additiona_class ));
	}
	return $thumb;
}