<?php 

function road_food_remove_default_fields() {
    remove_meta_box( 'postexcerpt' , 'restaurants' , 'normal' );
}
add_action( 'admin_menu' , 'road_food_remove_default_fields' );
 
function road_food_add_excerpt_meta_box( $post_type ) {
    if ( in_array( $post_type, array( 'restaurants') ) ) {
        add_meta_box(
            'road_food_postexcerpt',
            // 'postexcerpt',
            __( 'Review Snippet', 'roadfood' ),
            'post_excerpt_meta_box',
            $post_type,
            'after_title_top',
            'high'
        );
    }
}
add_action( 'add_meta_boxes', 'road_food_add_excerpt_meta_box' );
 

function road_food_run_after_title_meta_boxes($post) {
    // global $post, $wp_meta_boxes;
    do_meta_boxes( get_current_screen(), 'after_title_top', $post );
}
add_action( 'edit_form_after_title', 'road_food_run_after_title_meta_boxes' );


