<?php 

//Disable Gutenberg editor 
add_filter('use_block_editor_for_post', '__return_false', 10);


function content($limit) {
  $content = explode(' ', get_the_content(), $limit);
  if (count($content)>=$limit) {
    array_pop($content);
    $content = implode(" ",$content).'...';
  } else {
    $content = implode(" ",$content);
  }	
  $content = preg_replace('/[.+]/','', $content);
  $content = apply_filters('the_content', $content); 
  $content = str_replace(']]>', ']]>', $content);
  return $content;
}
//excerpt counted by 95 character
function get_excerpt(){
	$excerpt = get_the_content();
	$excerpt = preg_replace(" ([.*?])",'',$excerpt);
	$excerpt = strip_shortcodes($excerpt);
	$excerpt = strip_tags($excerpt);
	$excerpt = substr($excerpt, 0, 95);
	$excerpt = substr($excerpt, 0, strripos($excerpt, " "));
	$excerpt = trim(preg_replace( '/\s+/', ' ', $excerpt));
	$excerpt = $excerpt.'...';
	return '<p>'.$excerpt.'</p>';
}

//dynamic excerp counted by words
function excerpt($limit) {
  $excerpt = explode(' ', get_the_excerpt(), $limit);
  if (count($excerpt)>=$limit) {
    array_pop($excerpt);
    $excerpt = implode(" ",$excerpt).'...';
  } else {
    $excerpt = implode(" ",$excerpt);
  }	
  $excerpt = preg_replace('`[[^]]*]`','',$excerpt);
  return '<p>'.$excerpt.'</p>';
}

// "Continue Reading" functionality
function get_continue_reading(){
	global $post;
	$post_content = $post->post_content;
	if(strpos($post_content, '<!--more-->') ){
		$post_content = str_replace('<!--more-->' , '<div class="section__entry-hidden">', $post_content);
		$post_content .= '</div>';
	}
	return apply_filters('the_content', $post_content);
}

//jquery load more post/article guide page
function roadfood_load_more_scripts() {
	// if(is_page_template( 'template-articles_guides.php' ) || is_single()){
		global $wp_query; 

		//wp_enqueue_script('jquery');
		wp_register_script( 'my_customjs', get_stylesheet_directory_uri() . '/assets/js/mycustom.js', array('jquery') );
		wp_register_script( 'my_loadmore', get_stylesheet_directory_uri() . '/assets/js/myloadmore.js', array('jquery') );

		wp_localize_script( 'my_loadmore', 'roadfood_loadmore_params', array(
			'ajaxurl' => site_url() . '/wp-admin/admin-ajax.php', 
			'posts' => json_encode( $wp_query->query_vars ), 
			'current_page' => get_query_var( 'paged' ) ? get_query_var('paged') : 1,
			'max_page' => $wp_query->max_num_pages
		) );

		wp_enqueue_script( 'my_loadmore' );
		wp_enqueue_script( 'my_customjs' );
	// }
}
add_action( 'wp_enqueue_scripts', 'roadfood_load_more_scripts' );

//load more articles, dish, tours and bests
function roadfood_loadmore_ajax_handler(){
 
  $args = array(
		'post_type' => array('post', 'dish', 'tours', 'bests'),
		'posts_per_page' => 21,
		'orderby' => 'date',
		'order'   => 'DESC',
	  	'post_status' => 'publish',
		'paged' => $_POST['page'] + 1
	);
	$query = new WP_Query( $args );	
	if( $query->have_posts() ) :
 
		// run the loop
		while( $query->have_posts() ): $query->the_post();
 		
 			?>
			<div class="col-lg-4 col-6">
				<article class="article-default">
					<div class="article__image bg-parent js-image-fit">
						<a href="<?php the_permalink(); ?>" class="article__image-link"></a>
						<?php if(has_post_thumbnail()){ ?>
							<img src="<?php echo the_post_thumbnail_url('medium'); ?>" alt="" class="bg-image">
						<?php }else if(get_field('dish_image')){
							$images = get_field('dish_image'); 
							if(is_array($images)){
								$g_image = $images[0];
							}else{
								$g_image = $images;
							}
						?>
							<img src="<?php echo esc_url(wp_get_attachment_image_src($g_image, 'medium')[0]); ?>" alt="" class="bg-image">
						<?php }else{ ?>
							<img src="/wp-content/uploads/2020/11/no-image-icon.jpg" alt="" class="bg-image" style="border: 1px solid #ddd;">
						<?php } ?>					
					</div><!-- /.article__image -->

					<div class="article__content">
						<h3>
							<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
						</h3>
						<?php if ( !empty( get_the_content() ) ): ?>
						<?php echo get_excerpt(); ?>
						<?php endif; ?>
					</div><!-- /.article__content -->
				</article><!-- /.article-default -->
			</div><!-- /.col-md-4 -->
	<?php
		endwhile;
   		wp_reset_postdata();
	endif;
	
	die; // here we exit the script
	
}

//load more articles only
function roadfood_loadmore_article_ajax_handler(){
 
  $args = array(
		'post_type' => array('post'),
		'posts_per_page' => 21,
		'orderby' => 'date',
		'order'   => 'DESC',
	  	'post_status' => 'publish',
		'paged' => $_POST['page'] + 1
	);
	$query = new WP_Query( $args );	
	if( $query->have_posts() ) :
 
		// run the loop
		while( $query->have_posts() ): $query->the_post();
 		
 			?>
			<div class="col-lg-4 col-6">
				<article class="article-default">
					<div class="article__image bg-parent js-image-fit">
						<a href="<?php the_permalink(); ?>" class="article__image-link"></a>
						<?php if(has_post_thumbnail()){ ?>
							<img src="<?php echo the_post_thumbnail_url('medium'); ?>" alt="" class="bg-image">
						<?php }else if(get_field('dish_image')){
							$images = get_field('dish_image'); 
							if(is_array($images)){
								$g_image = $images[0];
							}else{
								$g_image = $images;
							}
						?>
							<img src="<?php echo esc_url(wp_get_attachment_image_src($g_image, 'medium')[0]); ?>" alt="" class="bg-image">
						<?php }else{ ?>
							<img src="/wp-content/uploads/2020/11/no-image-icon.jpg" alt="" class="bg-image" style="border: 1px solid #ddd;">
						<?php } ?>					
					</div><!-- /.article__image -->

					<div class="article__content">
						<h3>
							<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
						</h3>
						<?php if ( !empty( get_the_content() ) ): ?>
						<?php echo get_excerpt(); ?>
						<?php endif; ?>
					</div><!-- /.article__content -->
				</article><!-- /.article-default -->
			</div><!-- /.col-md-4 -->
	<?php
		endwhile;
   		wp_reset_postdata();
	endif;
	
	die; // here we exit the script
	
}

add_action('wp_ajax_loadmore_article', 'roadfood_loadmore_article_ajax_handler'); 
add_action('wp_ajax_nopriv_loadmore_article', 'roadfood_loadmore_article_ajax_handler');


//load more articles base on post_type[data-type] from button must have the same element markup
function roadfood_loadmore_article_exclude_current_ajax_handler(){
 
  $args = array(
		'post_type' => array($_POST['post_type']),
		'posts_per_page' => 6,
		'orderby' => 'date',
		'order'   => 'DESC',
	  	'post_status' => 'publish',
	    'post__not_in' => array (get_the_ID()),
		'paged' => $_POST['page'] + 1
	);
	$query = new WP_Query( $args );	
	if( $query->have_posts() ) :
 
		// run the loop
		while( $query->have_posts() ): $query->the_post();
 		
 			?>
			<div class="col-lg-4 col-6">
				<article class="article-default">
					<div class="article__image bg-parent js-image-fit">
						<a href="<?php the_permalink(); ?>" class="article__image-link"></a>
						<?php if(has_post_thumbnail()){ ?>
							<img src="<?php echo the_post_thumbnail_url('medium'); ?>" alt="" class="bg-image">
						<?php }else if(get_field('dish_image')){
							$images = get_field('dish_image'); 
							if(is_array($images)){
								$g_image = $images[0];
							}else{
								$g_image = $images;
							}
						?>
							<img src="<?php echo esc_url(wp_get_attachment_image_src($g_image, 'medium')[0]); ?>" alt="" class="bg-image">
						<?php }else{ ?>
							<img src="/wp-content/uploads/2020/11/no-image-icon.jpg" alt="" class="bg-image" style="border: 1px solid #ddd;">
						<?php } ?>					
					</div><!-- /.article__image -->

					<div class="article__content">
						<h3>
							<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
						</h3>
						<?php if ( !empty( get_the_content() ) ): ?>
						<?php echo get_excerpt(); ?>
						<?php endif; ?>
					</div><!-- /.article__content -->
				</article><!-- /.article-default -->
			</div><!-- /.col-md-4 -->
	<?php
		endwhile;
   		wp_reset_postdata();
	endif;
	
	die; // here we exit the script
	
}

add_action('wp_ajax_loadmore_article_exclude_current', 'roadfood_loadmore_article_exclude_current_ajax_handler'); 
add_action('wp_ajax_nopriv_loadmore_article_exclude_current', 'roadfood_loadmore_article_exclude_current_ajax_handler');

//jquery load more restaurant
function roadfood_loadmore_restaurant_ajax_handler(){
 //post per page should be the same from the main query[template query to show results]
  $args = array(
		'post_type' => array('restaurants'),
		'posts_per_page' => 10, 
		'orderby' => 'date',
		'order'   => 'DESC',
	  	'post_status' => 'publish',
		'paged' => $_POST['page'] + 1
	);
	$query = new WP_Query( $args );	
	if( $query->have_posts() ) :
 
		// run the loop
		while ($query->have_posts()) : $query->the_post(); ?>
		<div class="items-best">
			<div class="item-best">
				<div class="item__best-inner">
					<div class="item__best-image bg-parent js-image-fit">
						<?php if(!empty(get_the_post_thumbnail())){ ?>
							<img src="<?php echo the_post_thumbnail_url('thumbnail'); ?>" alt="" class="bg-image">
						<?php }else{ ?>
							<img src="/wp-content/uploads/2020/11/no-image-icon.jpg" alt="" class="bg-image" style="border: 1px solid #ddd;">
						<?php } ?>	
					</div><!-- /.item__best-image -->

					<div class="item__best-content">
						<h4>
							<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
						</h4>

						<h6>
							<a href="<?php the_permalink(); ?>"><?php echo get_field('restaurant_city'); ?>, <?php echo get_field('restaurant_state'); ?></a>
						</h6>
					</div><!-- /.item__best-content -->
				</div><!-- /.item__best-inner -->

				<div class="item__best-rating">
					<ul>
						<?php $rating = get_field('rf_rstrnt_rating');
							$num_star = 0;
							if($rating == 'legendary'){
								$num_star = 5;
							}else if($rating == 'best'){
								$num_star = 4;
							}else if($rating == 'excellent'){
								$num_star = 3;
							}else if($rating == 'good'){
								$num_star = 2;
							}else if($rating == 'ok'){
								$num_star = 1;
							}else{
								$num_star = 0;
							}

							for($x = 1; $x <= 5; $x++){
								if($x <= $num_star){
									echo '<li><img src="/wp-content/themes/roadfood/assets/images/star-orange.svg" alt=""></li>';	
								}else{
									echo '<li><img src="/wp-content/themes/roadfood/assets/images/star-gray.svg" alt=""></li>';	
								}

							}
						?>												

					</ul>
				</div><!-- /.item__best-rating -->
			</div><!-- /.item-best -->

		</div><!-- /.items-best -->
		<?php endwhile;

	endif;
	die; // here we exit the script
	wp_reset_query();
}

add_action('wp_ajax_loadmore_restaurant', 'roadfood_loadmore_restaurant_ajax_handler'); 
add_action('wp_ajax_nopriv_loadmore_restaurant', 'roadfood_loadmore_restaurant_ajax_handler');

//jquery load more dish
function roadfood_loadmore_dish_ajax_handler(){
 //post per page should be the same from the main query[template query to show results]
	if( !is_null($_POST['query'])){
		$args = $_POST['query'];
		$args['paged'] = $_POST['page'] + 1;
		$args['zzz'] = 'zzz';
	}else{
		$args = array(
			'post_type' => array('ndish'),
			'posts_per_page' => 10, 
			'orderby' => 'date',
			'order'   => 'DESC',
			'post_status' => 'publish',
			'paged' => $_POST['page'] + 1
		);
	}

	$query = new WP_Query( $args );	
	if( $query->have_posts() ) :
 
		// run the loop
		while ($query->have_posts()) : $query->the_post(); ?>
		<div class="item">
			<div class="item__image bg-parent js-image-fit">
				<?php  echo get_roadfood_post_thumbnail('dishes-thumbnail', 'bg-image');?>
			</div><!-- /.item__image -->
			<div class="item__content">
				<h4><a href="<?php the_permalink()?>"><?php the_title();?></a></h4>
				<ul>
					<?php if(get_roadfood_dish_type()):?>
					<li>
						<?php the_roadfood_dish_type();?>
					</li>
					<?php endif;?>
					<?php if(count(get_restaurants_with_dish()) == 1):?>
					<li>
						<?php echo count(get_restaurants_with_dish());?> Restaurant
					</li>
					<?php else:?>
					<li>
						<?php echo count(get_restaurants_with_dish());?> Restaurants
					</li>
					<?php endif?>
					<?php if(count(get_roadfood_recipes()) == 1):?>
					<li>
						<?php echo count(get_roadfood_recipes());?> Recipe
					</li>
					<?php else:?>
					<li>
						<?php echo count(get_roadfood_recipes());?> Recipes
					</li>
					<?php endif;?>
				</ul>
			</div><!-- /.item__content -->
		</div><!-- /.item -->
		<?php endwhile;

	endif;
	die; // here we exit the script
	wp_reset_query();
}

add_action('wp_ajax_loadmore_dish', 'roadfood_loadmore_dish_ajax_handler'); 
add_action('wp_ajax_nopriv_loadmore_dish', 'roadfood_loadmore_dish_ajax_handler');


//jquery load more recipes on single restaurant
function roadfood_loadmore_recipes_ajax_handler(){
 
	$query = new WP_Query(array(											
		"post_type" => "recipe",
		'posts_per_page' => 8,
		'orderby' => 'date',
		'order'   => 'DESC',
		'post_status' => 'publish',
		'paged' => $_POST['page'] + 1,
		"meta_query" => array(
			array(
			'key' => 'restaurant',
			'value' => $_POST['restaurant_id']
				)
		)
	));	
	if( $query->have_posts() ) :
 
		// run the loop
		while ($query->have_posts()) : $query->the_post(); ?>
			<div class="col-md-3 col-6">
				<div class="box">
					<div class="box__inner">
						<a href="<?php the_permalink(); ?>" class="box__link"></a>
						<div class="box__image bg-parent js-image-fit">
						<?php if(!empty(get_the_post_thumbnail())){ ?>
							<img src="<?php echo the_post_thumbnail_url('Medium'); ?>" alt="" class="bg-image">
						<?php }else{ ?>
							<img src="/wp-content/uploads/2020/11/no-image-icon.jpg" alt="" class="bg-image" style="border: 1px solid #ddd;">
						<?php } ?>	
						</div><!-- /.box__image -->
						<div class="box__content">
							<p><?php the_title(); ?></p>

						</div><!-- /.box__content -->
					</div><!-- /.box__inner -->
				</div><!-- /.box -->
			</div><!-- /.col-md-4 -->
		<?php endwhile;

	endif;
	die; // here we exit the script
	wp_reset_query();
}

add_action('wp_ajax_loadmore_recipes', 'roadfood_loadmore_recipes_ajax_handler'); 
add_action('wp_ajax_nopriv_loadmore_recipes', 'roadfood_loadmore_recipes_ajax_handler');



//jquery load more tours
function roadfood_loadmore_tours_ajax_handler(){
 //post per page should be the same from the main query[template query to show results]
   $args = array(
		'post_type' => array('tours'),
		'posts_per_page' => 10,
		'orderby' => 'date',
		'order'   => 'DESC',
		'post_status' => 'publish',
		'paged' => $_POST['page'] + 1
	);
	$query = new WP_Query( $args );	
	if( $query->have_posts() ) :
 
		// run the loop
		while ($query->have_posts()) : $query->the_post(); ?>
		<div class="col-lg-4 col-6">
			<article class="article-default">
				<div class="article__image bg-parent js-image-fit">
					<a href="<?php the_permalink(); ?>" class="article__image-link"></a>
					<?php if(has_post_thumbnail()){ ?>
						<img src="<?php echo the_post_thumbnail_url('medium'); ?>" alt="" class="bg-image">
					<?php }else{ ?>
						<img src="/wp-content/uploads/2020/11/no-image-icon.jpg" alt="" class="bg-image" style="border: 1px solid #ddd;">
					<?php } ?>				
				</div><!-- /.article__image -->

				<div class="article__content">
					<h3>
						<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
					</h3>
					<?php if ( !empty( get_the_content() ) ): ?>
					<?php echo get_excerpt(); ?>
					<?php endif; ?>
				</div><!-- /.article__content -->
			</article><!-- /.article-default -->
		</div><!-- /.col-md-4 -->
		<?php endwhile;

	endif;
	die; // here we exit the script
	wp_reset_query();
}

add_action('wp_ajax_loadmore_tours', 'roadfood_loadmore_tours_ajax_handler'); 
add_action('wp_ajax_nopriv_loadmore_tours', 'roadfood_loadmore_tours_ajax_handler');


// filter modal state
function modal_state_filter_selection_ajax_handler(){
	$filter_type = $_POST['filter_type'];
	?>
	 <div class="col-12">
		 <?php if($filter_type == 'state'): ?>
		  	<h5 class="filter-heading">Refine By Region/State</h5>
		 <?php elseif($filter_type == 'restaurant_type'): ?>
		 	<h5 class="filter-heading">Refine By Restaurant Type</h5>
	 	<?php elseif($filter_type == 'dish_type'): ?>
		 	<h5 class="filter-heading">Refine By Dish Type</h5>
		 <?php else: ?>
		 	<h5 class="filter-heading">Undefined</h5>
		 <?php endif; ?>
	  </div>
	 
		  <?php 
			if($filter_type == 'state'):?>
			 <div class="col-6 col-sm-3">
				<?php
				$restaurant_states = array();					
				$args = array(
					'post_type' => array('restaurants'),
					'posts_per_page' => -1,													
					'post_status' => 'publish'													
				);
				$query = new WP_Query( $args );
				$count = 0;
				$max_states = 0;
				if ($query->have_posts()) : 
				while ($query->have_posts()) : $query->the_post(); 

					$state = get_field('restaurant_state');

					//show unique cities
					if (!in_array($state, $restaurant_states)) {	
						//if 60 cities break the loop
						if($max_states != 60){
							$max_states++;
							$count++;
							echo ' <p><a href="javascript:;" class="js-modal-filter-option" data-type="state" data-select="'.$state.'">'.$state.'</a></p>';
							//array initialize
							$restaurant_states[] = $state;
						}

					}						

					//break to another column
					if($count == 15){
						$count = 0;
						echo '</div><div class="col-6 col-sm-3">';
					}

				endwhile;
				endif;
				wp_reset_postdata(); 
				die();?>
			 </div>
			<?php
			elseif($filter_type == 'restaurant_type'):
	
			  $args = array(
					'taxonomy' => 'category',
					'orderby' => 'name',
					'order'   => 'ASC',
					'post_type' => 'restaurants'
			  );
			  $count = 0;
  			  $terms = get_categories($args);
				if ( $terms && !is_wp_error( $terms ) ) :?>
					<div class="col-6 col-sm-2">
						<?php foreach ( $terms as $term ) { 
						
							echo '<p><a href="javascript:;" class="js-modal-filter-option" data-type="restaurant_type" data-select="'.$term->term_id.'" style="font-size: 14px;">'.$term->name.'</a></p>';
							$count++;
							
							if($count == 16){
								$count = 0;
								echo '</div><div class="col-6 col-sm-2">';
							}
						?>							
						<?php } ?>
					</div>
			  <?php endif;
			  wp_reset_postdata(); 
			  die();
			elseif($filter_type == 'dish_type'):
				$args = array(
					'taxonomy' => 'dish_type',
					'orderby' => 'name',
					'order'   => 'ASC',
					'post_type' => 'restaurants'
				);
				$count = 0;
				$terms = get_terms($args);
				if ( $terms && !is_wp_error( $terms ) ) :?>
					<div class="col-6 col-sm-2">
						<?php foreach ( $terms as $term ) { 

							echo '<p><a href="javascript:;" class="js-modal-filter-option" data-type="dish_type" data-select="'.$term->term_id.'" style="font-size: 14px;">'.$term->name.'</a></p>';
							$count++;

							if($count == 16){
								$count = 0;
								echo '</div><div class="col-6 col-sm-2">';
							}
							?>							
						<?php } ?>
					</div>
				<?php endif;
				wp_reset_postdata(); 
			else:
				echo 'Sorry, no result found.';
			endif;
		wp_die();
		?>
<?php
}

add_action('wp_ajax_filter_selection', 'modal_state_filter_selection_ajax_handler'); 
add_action('wp_ajax_nopriv_filter_selection', 'modal_state_filter_selection_ajax_handler');


//filter modal results state restaurant page
function modal_state_filter_result_ajax_handler(){
	?>
<div class="items-best-list">
	<?php
	$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1; 
	 $args = array(
		'post_type' => array('restaurants'),
		'posts_per_page' => -1,
		'orderby' => 'date',
		'order'   => 'DESC',
		'post_status' => 'publish',
		'paged' => $paged,
		 "meta_query" => array(
			array(
			'key' => 'restaurant_state',
			'value' => $_POST['data_select']
			)
		)

	);
	$query = new WP_Query( $args );	
	?>
	<?php if ($query->have_posts()) : ?>
	<?php while ($query->have_posts()) : $query->the_post(); 

	?>
	<div class="items-best">
		<div class="item-best">
			<div class="item__best-inner">
				<div class="item__best-image bg-parent js-image-fit">
					<?php if(has_post_thumbnail()){ ?>
						<img src="<?php echo the_post_thumbnail_url('Medium'); ?>" alt="" class="bg-image">
					<?php }else{ ?>
						<img src="/wp-content/uploads/2020/11/no-image-icon.jpg" alt="" class="bg-image" style="border: 1px solid #ddd;">
					<?php } ?>	
				</div><!-- /.item__best-image -->

				<div class="item__best-content">
					<h4>
						<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
					</h4>

					<h6>
						<a href="<?php the_permalink(); ?>"><?php echo get_field('restaurant_city'); ?>, <?php echo get_field('restaurant_state'); ?></a>
					</h6>
				</div><!-- /.item__best-content -->
			</div><!-- /.item__best-inner -->

			<div class="item__best-rating">
				<ul>
					<?php $rating = get_field('rf_rstrnt_rating');
						$num_star = 0;
						if($rating == 'legendary'){
							$num_star = 5;
						}else if($rating == 'best'){
							$num_star = 4;
						}else if($rating == 'excellent'){
							$num_star = 3;
						}else if($rating == 'good'){
							$num_star = 2;
						}else if($rating == 'ok'){
							$num_star = 1;
						}else{
							$num_star = 0;
						}

						for($x = 1; $x <= 5; $x++){
							if($x <= $num_star){
								echo '<li><img src="/wp-content/themes/roadfood/assets/images/star-orange.svg" alt=""></li>';	
							}else{
								echo '<li><img src="/wp-content/themes/roadfood/assets/images/star-gray.svg" alt=""></li>';	
							}

						}
					?>												

				</ul>
			</div><!-- /.item__best-rating -->
		</div><!-- /.item-best -->

	</div><!-- /.items-best -->
<?php
	endwhile;	
	endif;
	wp_reset_postdata(); 
	die();
	?>
 </div>
<div class="section__actions">
	
<!-- 	<a href="javascript:;" class="btn-show-more roadfood-loadmore-restaurant">Show More</a> -->
	
</div><!-- /.section__actions -->
<?php
}

add_action('wp_ajax_filter_result', 'modal_state_filter_result_ajax_handler'); 
add_action('wp_ajax_nopriv_filter_result', 'modal_state_filter_result_ajax_handler');


function filter_bcn_attributes_arr( $classes_arr, $breadcrumb_get_types, $breadcrumb_get_id ) {
	array_push($classes_arr['class'], 'breadcrumb-item');
	return $classes_arr;
}
add_filter( 'bcn_display_attribute_array', 'filter_bcn_attributes_arr', 10, 3 ); 

function get_restaurants_with_dish(){
	global $post;

	$menu_item_ids_arr = get_posts(array(
		'post_type' 	=> 'dish',
	    'fields'       	=> 'ids',
	    'numberposts'  	=> -1,
	    'meta_key' 		=> 'ndish',
		'meta_value' 	=> $post->ID,
	));

	foreach ($menu_item_ids_arr as $menu_item_id){
		$meta_query_arr[]= 	array(
            'key'   => 'nrf_dishes',
            'value' => $menu_item_id,
            'compare' => 'LIKE',
        );
	};
	$meta_query_arr['relation'] = 'OR';
	$restaurant_ids_arr = get_posts(array(
		'post_type' 	=> 'restaurants',
		'fields'       	=> 'ids',
		'meta_query' => $meta_query_arr,
		'numberposts'	=> -1,
		// 'orderby'		=> 'rand', 
	));
	return $restaurant_ids_arr;
}

function get_roadfood_recipes(){
	global $post;

	$restaurant_ids_arr = get_restaurants_with_dish();

	foreach ($restaurant_ids_arr as $restaurant_id){
		$meta_query_arr[]= 	array(
            'key'   => 'restaurant',
            'value' => $restaurant_id,
            'compare' => 'LIKE',
        );
	};
	$meta_query_arr['relation'] = 'OR';
	$recipes_ids_arr = get_posts(array(
		'post_type' 	=> 'recipe',
		'fields'       	=> 'ids',
		'meta_query' => $meta_query_arr,
		'numberposts'	=> -1,
		// 'orderby'		=> 'rand', 
	));
	return $recipes_ids_arr;
}

function get_roadfood_dish_type(){
	global $post;
	$html = '';
	$term_list = wp_get_post_terms( $post->ID, 'dish_type', array('fields' => 'all') );
	if(count($term_list)>0){
		foreach($term_list as $key => $term){
			if($key != 0){ $html.=', ';}
			$html .= '<a href="'.get_term_link( $term, $taxonomy = 'dish_type' ).'">'.$term->name.'</a>';
		}
	}	
	return $html;
}

function the_roadfood_dish_type(){

	echo get_roadfood_dish_type();
}