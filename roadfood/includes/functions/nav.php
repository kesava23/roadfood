<?php
/* Menus registration */ 
register_nav_menus( array(
	'primary' => __( 'Primary Navigation', 'roadfood' ),
	'utilities' => __( 'Utilities Navigation', 'roadfood' ),
	'footer_explore' => __( 'Footer Explore Navigation', 'roadfood' ),
	'footer_learn_more' => __( 'Footer Learn More Navigation', 'roadfood' ),
	'footer_privacy_policy' => __( 'Footer Privacy Policy Navigation', 'roadfood' ),
) );


/* Replace Standart WP Menu Classes */
function change_menu_classes($css_classes) {
        $css_classes = str_replace("current-menu-item", "active", $css_classes);
        $css_classes = str_replace("current-menu-parent", "active", $css_classes);
        return $css_classes;
}
add_filter('nav_menu_css_class', 'change_menu_classes');