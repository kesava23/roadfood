<?php

add_action('init', 'restaurants_custom_init');
function restaurants_custom_init() 
{
  $labels = array(
    'name' => _x('Restaurants', 'post type general name'),
    'singular_name' => _x('Restaurant', 'post type singular name'),
    'add_new' => _x('Add New', 'Restaurant'),
    'add_new_item' => __('Add New Restaurant'),
    'edit_item' => __('Edit Restaurant'),
    'new_item' => __('New Restaurant'),
    'view_item' => __('View Restaurant'),
    'search_items' => __('Search Restaurants'),
    'not_found' =>  __('No Restaurants found'),
    'not_found_in_trash' => __('No Restaurants found in Trash'), 
    'parent_item_colon' => '',
    'menu_name' => 'Restaurants'
  );
  
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true, 
    'show_in_menu' => true, 
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'has_archive' => true, 
    'hierarchical' => false,
    'menu_position' => null,
    'menu_icon' => get_stylesheet_directory_uri().'/images/restaurant.png',
    'supports' => array('title','editor','thumbnail','excerpt', 'comments'),
	'rewrite' => array('slug' => 'restaurant_list'),
    'taxonomies' => array('category', 'post_tag')
  ); 
  register_post_type('restaurants',$args);
}

// add_action('init', 'dishes_custom_init');
// function dishes_custom_init() 
// {
//   $labels = array(
//     'name' => _x('Menu items', 'post type general name'),
//     'singular_name' => _x('Menu item', 'post type singular name'),
//     'add_new' => _x('Add New', 'Menu item'),
//     'add_new_item' => __('Add New Menu item'),
//     'edit_item' => __('Edit Menu item'),
//     'new_item' => __('New Menu item'),
//     'view_item' => __('View Menu item'),
//     'search_items' => __('Search Menu items'),
//     'not_found' =>  __('No Menu items found'),
//     'not_found_in_trash' => __('No Menu items found in Trash'), 
//     'parent_item_colon' => '',
//     'menu_name' => 'OldDishes'
//   );
  
//   $args = array(
//     'labels' => $labels,
//     'public' => true,
//     'publicly_queryable' => true,
//     'show_ui' => true, 
//     'show_in_menu' => true, 
//     'query_var' => true,
//     'rewrite' => true,
//     'capability_type' => 'post',
//     'has_archive' => true, 
//     'hierarchical' => false,
//     'menu_position' => null,
//     'menu_icon' => get_stylesheet_directory_uri().'/images/bowl.png',
//     'supports' => array('title','editor','thumbnail', ),
//     'taxonomies' => array('dish_type')
//   ); 
//   register_post_type('dishes',$args);
// }

add_action('init', 'dish_custom_init');
function dish_custom_init() 
{
  $labels = array(
    'name' => _x('Menu Items', 'post type general name'),
    'singular_name' => _x('Menu Item', 'post type singular name'),
    'add_new' => _x('Add New', 'Menu Item'),
    'add_new_item' => __('Add New Menu Item'),
    'edit_item' => __('Edit Menu Item'),
    'new_item' => __('New Menu Item'),
    'view_item' => __('View Menu Item'),
    'search_items' => __('Search Menu Items'),
    'not_found' =>  __('No Menu Items found'),
    'not_found_in_trash' => __('No Menu Items found in Trash'), 
    'parent_item_colon' => '',
    'menu_name' => 'Menu Items'
  );
  
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true, 
    'show_in_menu' => true, 
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'has_archive' => true, 
    'hierarchical' => false,
    'menu_position' => null,
    'menu_icon' => get_stylesheet_directory_uri().'/images/bowl.png',
    'supports' => array('title','editor','thumbnail', ),
    'taxonomies' => array('dish_type')
  ); 
  register_post_type('dish',$args);
}

add_action('init', 'ndish_custom_init');
function ndish_custom_init() 
{
  $labels = array(
    'name' => _x('Dishes', 'post type general name'),
    'singular_name' => _x('Dish', 'post type singular name'),
    'add_new' => _x('Add New', 'Dish'),
    'add_new_item' => __('Add New Dish'),
    'edit_item' => __('Edit Dish'),
    'new_item' => __('New Dish'),
    'view_item' => __('View Dish'),
    'search_items' => __('Search Dishes'),
    'not_found' =>  __('No Dishes found'),
    'not_found_in_trash' => __('No Dishes found in Trash'), 
    'parent_item_colon' => '',
    'menu_name' => 'Dishes'
  );
  
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true, 
    'show_in_menu' => true, 
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'has_archive' => true, 
    'hierarchical' => false,
    'menu_position' => null,
    'menu_icon' => get_stylesheet_directory_uri().'/images/bowl.png',
    'supports' => array('title','editor','thumbnail'),
    'taxonomies' => array('dish_type')
  ); 
  register_post_type('ndish',$args);
}

// add_action('init', 'menu_items_custom_init');
// function menu_items_custom_init() 
// {
//   $labels = array(
//     'name' => _x('Menu Items', 'post type general name'),
//     'singular_name' => _x('Menu Item', 'post type singular name'),
//     'add_new' => _x('Add New', 'Menu Item'),
//     'add_new_item' => __('Add New Menu Item'),
//     'edit_item' => __('Edit Menu Item'),
//     'new_item' => __('New Menu Item'),
//     'view_item' => __('View Menu Item'),
//     'search_items' => __('Search Menu Items'),
//     'not_found' =>  __('No Menu Items found'),
//     'not_found_in_trash' => __('No Menu Items found in Trash'), 
//     'parent_item_colon' => '',
//     'menu_name' => 'Menu Items'
//   );
  
//   $args = array(
//     'labels' => $labels,
//     'public' => true,
//     'publicly_queryable' => true,
//     'show_ui' => true, 
//     'show_in_menu' => true, 
//     'query_var' => true,
//     'rewrite' => true,
//     'capability_type' => 'post',
//     'has_archive' => true, 
//     'hierarchical' => false,
//     'menu_position' => null,
//     'menu_icon' => 'dashicons-excerpt-view',
//     'supports' => array('title','editor','thumbnail'),
//   ); 
//   register_post_type('menu-item',$args);
// }

add_action('init', 'recipes_custom_init');
function recipes_custom_init() 
{
  $labels = array(
    'name' => _x('Recipes', 'post type general name'),
    'singular_name' => _x('Recipe', 'post type singular name'),
    'add_new' => _x('Add New', 'Recipe'),
    'add_new_item' => __('Add New Recipe'),
    'edit_item' => __('Edit Recipe'),
    'new_item' => __('New Recipe'),
    'view_item' => __('View Recipe'),
    'search_items' => __('Search Recipes'),
    'not_found' =>  __('No Recipes found'),
    'not_found_in_trash' => __('No Recipes found in Trash'), 
    'parent_item_colon' => '',
    'menu_name' => 'Recipes'
  );
  
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true, 
    'show_in_menu' => true, 
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'has_archive' => true, 
    'hierarchical' => false,
    'menu_position' => null,
    'menu_icon' => 'dashicons-list-view',
    'supports' => array('title','editor','thumbnail'),
  ); 
  register_post_type('recipe',$args);
}

add_action('init', 'tours_custom_init');
function tours_custom_init() 
{
  $labels = array(
    'name' => _x('Tours', 'post type general name'),
    'singular_name' => _x('Tour', 'post type singular name'),
    'add_new' => _x('Add New', 'Tour'),
    'add_new_item' => __('Add New Tour'),
    'edit_item' => __('Edit Tour'),
    'new_item' => __('New Tour'),
    'view_item' => __('View Tour'),
    'search_items' => __('Search Tours'),
    'not_found' =>  __('No Tours found'),
    'not_found_in_trash' => __('No Tours found in Trash'), 
    'parent_item_colon' => '',
    'menu_name' => 'Tours'
  );
  
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true, 
    'show_in_menu' => true, 
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'has_archive' => true, 
    'hierarchical' => false,
    'menu_position' => null,
    'menu_icon' => get_stylesheet_directory_uri().'/images/trips.png',
    'supports' => array('title','editor','thumbnail'),
	 'rewrite' => array('slug' => 'tour_list')
    // 'taxonomies' => array('category', 'post_tag')
  ); 
  register_post_type('tours',$args);
}

add_action('init', 'bests_custom_init');
function bests_custom_init() 
{
  $labels = array(
    'name' => _x('Bests', 'post type general name'),
    'singular_name' => _x('Best', 'post type singular name'),
    'add_new' => _x('Add New', 'Best'),
    'add_new_item' => __('Add New Best'),
    'edit_item' => __('Edit Best'),
    'new_item' => __('New Best'),
    'view_item' => __('View Best'),
    'search_items' => __('Search Bests'),
    'not_found' =>  __('No Bests found'),
    'not_found_in_trash' => __('No Bests found in Trash'), 
    'parent_item_colon' => '',
    'menu_name' => 'Bests'
  );
  
  $args = array(
    'labels' => $labels,
    'public' => true,
    'publicly_queryable' => true,
    'show_ui' => true, 
    'show_in_menu' => true, 
    'query_var' => true,
    'rewrite' => true,
    'capability_type' => 'post',
    'has_archive' => true, 
    'hierarchical' => false,
    'menu_position' => null,
    'menu_icon' => get_stylesheet_directory_uri().'/images/bests.png',
    'supports' => array('title','editor','thumbnail'),
    // 'taxonomies' => array('category', 'post_tag')
  ); 
  register_post_type('bests',$args);
}



/*****************************************************************************/
// Custom taxonomies

add_action( 'init', 'create_dishes_taxonomy' );
function create_dishes_taxonomy() 
{
  // Add new taxonomy, make it hierarchical (like categories)
  $labels = array(
    'name' => _x( 'Dish Type', 'taxonomy general name' ),
    'singular_name' => _x( 'Dish Type', 'taxonomy singular name' ),
    'search_items' =>  __( 'Search Dish Types' ),
    'all_items' => __( 'All Dish Types' ),
    'parent_item' => __( 'Parent Dish Type' ),
    'parent_item_colon' => __( 'Parent Dish Type:' ),
    'edit_item' => __( 'Edit Dish Type' ), 
    'update_item' => __( 'Update Dish Type' ),
    'add_new_item' => __( 'Add New Dish Type' ),
    'new_item_name' => __( 'New Dish Type Name' ),
    'menu_name' => __( 'Dish Types' ),
  );    

  register_taxonomy('dish_type',array('ndish'), array(
    'public'=>true,
    'hierarchical' => true,
    'labels' => $labels,
    'show_ui' => true,
    'rewrite' => array('slug' => 'ndishes'),
    ));
}





