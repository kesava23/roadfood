<?php get_header(); ?>

<div id="content">
	<?php if (have_posts()) : ?>
	
	<div class="post">
		<div class="title">
			<?php $post = $posts[0]; // Hack. Set $post so that the_date() works. ?>
			<?php /* If this is a category archive */ if (is_category()) { ?>
			<h1><?php printf(__( 'Archive for the &#8216;%s&#8217; Category', 'base' ), single_cat_title('', false)); ?></h1>
			<?php /* If this is a tag archive */ } elseif( is_tag() ) { ?>			
			<h1><?php printf(__( 'Posts Tagged &#8216;%s&#8217;', 'base' ), single_tag_title('', false)); ?></h1>
			<?php /* If this is a daily archive */ } elseif (is_day()) { ?>
			<h1><?php _e('Archive for', 'base'); ?> <?php the_time('F jS, Y'); ?></h1>
			<?php /* If this is a monthly archive */ } elseif (is_month()) { ?>
			<h1><?php _e('Archive for', 'base'); ?> <?php the_time('F, Y'); ?></h1>
			<?php /* If this is a yearly archive */ } elseif (is_year()) { ?>
			<h1><?php _e('Archive for', 'base'); ?> <?php the_time('Y'); ?></h1>
			<?php /* If this is an author archive */ } elseif (is_author()) { ?>
			<h1><?php _e('Author Archive', 'base'); ?></h1>
			<?php /* If this is a paged archive */ } elseif (isset($_GET['paged']) && !empty($_GET['paged'])) { ?>
			<h1><?php _e('Blog Archives', 'base'); ?></h1>
			<?php } ?>
		</div>
	</div>

	<?php while (have_posts()) : the_post(); ?>
	<div class="post" id="post-<?php the_ID(); ?>">
		<div class="title">
			<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php _e('Permanent Link to', 'base'); ?> <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
			<p class="info"><strong class="date"><?php the_time('F jS, Y') ?></strong> <?php _e('by', 'base'); ?> <?php the_author(); ?></p>
		</div>
		<div class="content">
			<?php the_excerpt(); ?>
		</div>
		<div class="meta">
			<ul>
				<li><?php _e('Posted in', 'base'); ?> <?php the_category(', ') ?></li>
				<li><?php comments_popup_link(__('No Comments', 'base'), __('1 Comment', 'base'), __('% Comments', 'base')); ?></li>
				<?php the_tags(__('<li>Tags: ', 'base'), ', ', '</li>'); ?>
			</ul>
		</div>
	</div>
	<?php endwhile; ?>
	
	<div class="navigation">
		<div class="next"><?php next_posts_link(__('Older Entries &raquo;', 'base')) ?></div>
		<div class="prev"><?php previous_posts_link(__('&laquo; Newer Entries', 'base')) ?></div>
	</div>
	
	<?php else : ?>
	<div class="post">
		<div class="title">
			<h1><?php _e('Not Found', 'base'); ?></h1>
		</div>
		<div class="content">
			<p><?php _e('Sorry, but you are looking for something that isn\'t here.', 'base'); ?></p>
		</div>
	</div>
	<?php endif; ?>
	
</div>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
