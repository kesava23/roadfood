<?php
/*
Template Name: Home Template
*/
get_header(); ?>
<div class="main">
	<?php if(have_rows('hero_section')):?>
	<div class="hero bg-parent js-image-fit">
		<?php if(get_field('hero_section_background_image')):?>
			<?php echo wp_get_attachment_image( get_field('hero_section_background_image'), 'hero-thumb', '', array('class'=>'bg-image') ); ?>
		<?php else:?>
			<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/temp/hero-bg-home.jpg" alt="" class="bg-image">
		<?php endif;?>
		<?php if(get_field('hero_section_sub-heading')||get_field('hero_section_heading')||get_field('hero_section_buttons')):?>
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="hero__inner">
						<?php if(get_field('hero_section_sub-heading')||get_field('hero_section_heading')):?>
						<div class="hero__heading">
							<?php if(get_field('hero_section_sub-heading')):?>
							<p class="hero__sub-title"><?php the_field('hero_section_sub-heading')?></p><!-- /.hero__sub-title -->
							<?php endif;?>
							<?php if(get_field('hero_section_heading')):?>
							<h1><?php the_field('hero_section_heading')?></h1>
							<?php endif;?>
						</div><!-- /.hero__heading -->
						<?php endif;?>
						<?php if(have_rows('hero_section_buttons')):?>
						<div class="hero__actions">
							<?php if(get_field('hero_section_buttons_sub-heading')):?>
							<p><?php the_field('hero_section_buttons_sub-heading')?></p>
							<?php endif;?>
							<?php if(have_rows('hero_section_buttons_buttons')):?>
							<ul>
								<?php while(have_rows('hero_section_buttons_buttons')): the_row();?>
								<li>
									<a href="<?php the_sub_field('button_url')?>" class="btn"><?php the_sub_field('button_text')?></a>
								</li>
								<?php endwhile;?>
							</ul>
							<?php endif;?>
						</div><!-- /.hero__actions -->
						<?php endif;?>
					</div><!-- /.hero__inner -->
				</div><!-- /.col-sm-12 -->
			</div><!-- /.row -->
		</div><!-- /.container -->
		<?php endif;?>
	</div><!-- /.hero -->
	<?php endif;?>
	<?php if(have_rows('discover_great_dishes_section')):?>
	<section class="section-dishes">
		<div class="container">
			<header class="section__head">
				<div class="header__head-aside">
					<?php if(get_field('discover_great_dishes_section_heading')):?>
						<h2><?php the_field('discover_great_dishes_section_heading');?></h2>
					<?php else:?>
						<h2>Discover Great Dishes</h2>
					<?php endif?>
					<div class="section__tags">
						<span>Roadfood <br> Favorites</span>
					</div><!-- /.section__tags -->
				</div><!-- /.header__head-aside -->

				<?php if(get_field('discover_great_dishes_section_browse_all_dishes_page')):?>
				<div class="header__head-actions">
					<a href="<?php the_field('discover_great_dishes_section_browse_all_dishes_page');?>">Browse All Dishes</a>
				</div><!-- /.header__head-actions -->
				<?php endif;?>
			</header><!-- /.section__head -->
			<?php
				if($favorite_dishes=get_field('discover_great_dishes_section_favorite_dishes')):
					$dishes_posts = new WP_Query(array(
						'post_type'		=> 'ndish',
						'showposts'		=> -1,
						'post__in'		=> $favorite_dishes,
						'orderby'		=> 'post__in',
						'order'			=> 'ASC',
					));
					if($dishes_posts->have_posts()):
			?>
			<div class="section__body">
				<div class="slider-dishes js-slider-dishes">
					<div class="slider__clip">
						<div class="slider__slides">
							<?php 
								while($dishes_posts->have_posts()): $dishes_posts->the_post();
							?>
							<div class="slider__slide">
								<div class="slider__slide-inner">
									<a href="<?php the_permalink()?>" class="slider__slide-link"></a>

									<div class="slider__slide-image bg-parent js-image-fit">
									<?php  echo get_roadfood_post_thumbnail('recipe-thumbnail', 'bg-image');?>
									</div><!-- /.slider__slide-image -->
									<div class="slider__slide-content">
										<p><?php the_title();?></p>
									</div><!-- /.slider__slide-content -->
								</div><!-- /.slider__slide-inner -->
							</div><!-- /.slider__slide -->
							<?php endwhile;?>
						</div><!-- /.slider__slides -->
					</div><!-- /.slider__clip -->
				</div><!-- /.slider-dishes js-slider-dishes -->
			</div><!-- /.section__body -->
			<?php 
					endif; wp_reset_postdata();
				endif;
			?>
			<?php if(get_field('discover_great_dishes_section_browse_all_dishes_page')):?>
			<div class="section__actions">
				<a href="<?php the_field('discover_great_dishes_section_browse_all_dishes_page');?>">Browse All Dishes</a>
			</div><!-- /.section__actions -->
			<?php endif;?>
		</div><!-- /.container -->
	</section><!-- /.section-dishes -->
	<?php endif;?>
	<?php if(get_field('advertisement_widget', 'options')):?>
	<section class="section-banner-mobile d-block d-lg-none">
		<?php the_field('advertisement_widget', 'options')?>
	</section><!-- /.section-banner-mobile -->
	<?php endif;?>

	<section class="section-base-layout">
		<div class="container">
			<div class="row">
				<div class="col-lg-8">
					<div class="section__content">
						<?php if(have_rows('browse_dishes_by_type')):?>
						<section class="section-base">
							<header class="section__head">
								<?php if(get_field('browse_dishes_by_type_heading')):?>
								<h2><?php the_field('browse_dishes_by_type_heading')?></h2>
								<?php else: ?>
								<h2>Browse Dishes By Type</h2>
								<?php endif;?>
								<?php if(get_field('browse_dishes_by_type_short_description')):?>
								<p><?php the_field('browse_dishes_by_type_short_description')?></p>
								<?php endif;?>
							</header><!-- /.section__head -->
							<?php if(get_field('browse_dishes_by_type_dish_types')){
								$dish_types = get_field('browse_dishes_by_type_dish_types');
							} else{
								$dish_types = get_terms( array(
									'taxonomy' => 'dish_type',
									'number' => 12
								));
							}?>
							<div class="section__body">
								<div class="boxes-horizontal">
									<div class="row row-small-margin">
										<?php foreach($dish_types as $dish_type):?>
										<div class="col-md-4 col-6 col-small-padding">
											<div class="box">
												<div class="box__inner">
													<a href="<?php echo get_term_link( $dish_type, $taxonomy = 'dish_type' );?>" class="box__link"></a>
													<div class="box__image bg-parent js-image-fit">
														<?php echo get_roadfood_taxonomy_term_thumbnail($dish_type, 'dish-type-thumb', 'bg-image');?>
													</div><!-- /.box__image -->
													<div class="box__content">
														<h5><?php echo $dish_type->name;?></h5>
													</div><!-- /.box__content -->
												</div><!-- /.box__inner -->
											</div><!-- /.box -->
										</div><!-- /.col-md-4 -->
										<?php endforeach;?>													
									</div><!-- /.row -->
								</div><!-- /.boxes-horizontal -->
							</div><!-- /.section__body -->

							<div class="section__actions">
								<a href="#" class="btn btn--with-shape">
									<span>More Dish Types</span>

									<svg width="27px" height="20px" viewBox="0 0 27 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="Asset-1" class="svg-color" fill="#FFFFFF" fill-rule="nonzero"><path d="M1.76,10.8 L14.76,13.8 L14.76,18.6 C14.7663086,18.9826391 14.9904405,19.3281223 15.3372831,19.4898403 C15.6841256,19.6515583 16.0928461,19.6011474 16.39,19.36 L26.2,10.54 C26.4186854,10.3500696 26.5442752,10.0746496 26.5442752,9.785 C26.5442752,9.4953504 26.4186854,9.21993042 26.2,9.03 L16.4,0.25 C16.1042499,0.00985886916 15.6977449,-0.0413773288 15.3516561,0.117865966 C15.0055674,0.27710926 14.7800248,0.619164918 14.77,1 L14.77,5.8 L1.77,8.8 C1.21771526,8.7972386 0.76776146,9.24271526 0.764987267,9.795 C0.762238613,10.3472847 1.20771526,10.7972386 1.76,10.8 Z" id="Path"></path></g></g></svg>
								</a>
							</div><!-- /.section__actions -->
						</section><!-- /.section-base -->
						<?php endif?>
						<?php if(have_rows('roadfood_approved_restaurants')):?>
						<section class="section-base">
							<header class="section__head">
								<?php if(get_field('roadfood_approved_restaurants_heading')):?>
								<h2><?php the_field('roadfood_approved_restaurants_heading');?></h2>
								<?php else:?>
								<h2>Roadfood Approved Restaurants</h2>
								<?php endif;?>
								<?php if(get_field('roadfood_approved_restaurants_short_description')):?>
								<p><?php the_field('roadfood_approved_restaurants_short_description');?></p>
								<?php endif;?>
							</header><!-- /.section__head -->
							<?php if(get_field('roadfood_approved_restaurants_restaurants_list')):?>
							<?php $restaurants_list = new WP_Query(array(
								'post_type' => 'restaurants',
								'showposts' => -1,
								'post__in' => get_field('roadfood_approved_restaurants_restaurants_list'),
								'orderby' => 'post__in',
								'order' => 'ASC'
							));
							if($restaurants_list->have_posts()): ?>
							<div class="section__body">
								<div class="boxes">
									<div class="row row-small-margin">
										<?php while($restaurants_list->have_posts()): $restaurants_list->the_post();?>
										<div class="col-md-4 col-6 col-small-padding">
											<div class="box">
												<div class="box__inner">
													<a href="<?php the_permalink();?>" class="box__link"></a>
													<div class="box__image bg-parent js-image-fit">
														<?php  echo get_roadfood_post_thumbnail('restaurant-thumbnail', 'bg-image');?>
													</div><!-- /.box__image -->
													<div class="box__content">
														<h5><?php the_title();?></h5>
														<?php if(get_field('restaurant_city') || get_field('restaurant_state')):?>
														<p><?php echo strtoupper(get_field('restaurant_city')); if(get_field('restaurant_city') && get_field('restaurant_state')){ echo', ';} echo strtoupper(get_field('restaurant_state'))?></p>
														<?php endif;?>
													</div><!-- /.box__content -->
												</div><!-- /.box__inner -->
											</div><!-- /.box -->
										</div><!-- /.col-md-4 -->
										<?php endwhile;?>
									</div><!-- /.row -->
								</div><!-- /.boxes -->
							</div><!-- /.section__body -->
							<?php endif; wp_reset_postdata();?>
							<?php endif;?>
							<div class="section__actions">
								<a href="<?php echo site_url();?>/restaurants/" class="btn btn--with-shape">
									<span>More Restaurants</span>

									<svg width="27px" height="20px" viewBox="0 0 27 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="Asset-1" class="svg-color" fill="#FFFFFF" fill-rule="nonzero"><path d="M1.76,10.8 L14.76,13.8 L14.76,18.6 C14.7663086,18.9826391 14.9904405,19.3281223 15.3372831,19.4898403 C15.6841256,19.6515583 16.0928461,19.6011474 16.39,19.36 L26.2,10.54 C26.4186854,10.3500696 26.5442752,10.0746496 26.5442752,9.785 C26.5442752,9.4953504 26.4186854,9.21993042 26.2,9.03 L16.4,0.25 C16.1042499,0.00985886916 15.6977449,-0.0413773288 15.3516561,0.117865966 C15.0055674,0.27710926 14.7800248,0.619164918 14.77,1 L14.77,5.8 L1.77,8.8 C1.21771526,8.7972386 0.76776146,9.24271526 0.764987267,9.795 C0.762238613,10.3472847 1.20771526,10.7972386 1.76,10.8 Z" id="Path"></path></g></g></svg>
								</a>
							</div><!-- /.section__actions -->
						</section><!-- /.section-base -->
						<?php endif;?>
						<?php if(have_rows('latest_recipes')):?>
						<section class="section-base">
							<header class="section__head">
								<?php if(get_field('latest_recipes_heading')):?>
								<h2><?php the_field('latest_recipes_heading');?></h2>
								<?php else: ?>
								<h2>Latest Recipes</h2>
								<?php endif;?>
								<?php if(get_field('latest_recipes_short_description')):?>
								<p><?php the_field('latest_recipes_short_description');?></p>
								<?php endif;?>
							</header><!-- /.section__head -->
							<?php $latest_recipes = new WP_Query(array(
								'post_type' => 'recipe',
								'showposts' => 4,
							));
							if( $latest_recipes->have_posts()):?>
							<div class="section__body">
								<div class="boxes-type-1">
									<div class="row">
										<?php while( $latest_recipes->have_posts()):  $latest_recipes->the_post();?>
										<div class="col-md-3 col-6">
											<div class="box">
												<div class="box__inner">
													<a href="<?php the_permalink();?>" class="box__link"></a>
													<div class="box__image bg-parent js-image-fit">
														<?php  echo get_roadfood_post_thumbnail('recipe-thumbnail', 'bg-image');?>
													</div><!-- /.box__image -->
													<div class="box__content">
														<p><?php the_title();?></p>
													</div><!-- /.box__content -->
												</div><!-- /.box__inner -->
											</div><!-- /.box -->
										</div><!-- /.col-md-4 -->
										<?php endwhile;?>
									</div><!-- /.row -->
								</div><!-- /.boxes -->
							</div><!-- /.section__body -->
							<?php endif; wp_reset_postdata();?>
							<div class="section__actions">
								<a href="#" class="btn btn--with-shape">
									<span>More Recipes</span>

									<svg width="27px" height="20px" viewBox="0 0 27 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="Asset-1" class="svg-color" fill="#FFFFFF" fill-rule="nonzero"><path d="M1.76,10.8 L14.76,13.8 L14.76,18.6 C14.7663086,18.9826391 14.9904405,19.3281223 15.3372831,19.4898403 C15.6841256,19.6515583 16.0928461,19.6011474 16.39,19.36 L26.2,10.54 C26.4186854,10.3500696 26.5442752,10.0746496 26.5442752,9.785 C26.5442752,9.4953504 26.4186854,9.21993042 26.2,9.03 L16.4,0.25 C16.1042499,0.00985886916 15.6977449,-0.0413773288 15.3516561,0.117865966 C15.0055674,0.27710926 14.7800248,0.619164918 14.77,1 L14.77,5.8 L1.77,8.8 C1.21771526,8.7972386 0.76776146,9.24271526 0.764987267,9.795 C0.762238613,10.3472847 1.20771526,10.7972386 1.76,10.8 Z" id="Path"></path></g></g></svg>
								</a>
							</div><!-- /.section__actions -->
						</section><!-- /.section-base -->
						<?php endif;?>
					</div><!-- /.section__content -->
				</div><!-- /.col-md-8 -->
				<?php if(get_field('advertisement_widget', 'options')):?>
				<div class="col-lg-4 d-none d-lg-block">
					<div class="section__sidebar">
						<ul class="widgets">
							<li class="widget widget--banner">
								<?php the_field('advertisement_widget', 'options')?>
							</li><!-- /.widget widget--banner -->
						</ul><!-- /.widgets -->
					</div><!-- /.section__sidebar -->
				</div><!-- /.col-md-4 -->
				<?php endif;?>
			</div><!-- /.row -->
		</div><!-- /.container -->
	</section><!-- /.section-base-layout -->
	<?php $articles = new WP_Query(array(
		'post_type' => 'post',
		'showposts' => 6,
	));
	if($articles->have_posts()):?>
	<section class="section-articles">
		<div class="container">
			<header class="section__head">
				<h2>Latest Articles & Guides</h2>
				<a href="<?php echo site_url();?>/articles-guides/" class="view-more">View All Articles & Guides</a>
			</header><!-- /.section__head -->
			<div class="section__body">
				<div class="articles">
					<div class="row">
						<?php while($articles->have_posts()): $articles->the_post();?>
						<div class="col-md-4 col-6 ">
							<div class="article">
								<div class="article__inner">
									<div class="article__image bg-parent js-image-fit">
										<a href="<?php the_permalink();?>" class="article__image-link"></a>
										<?php  echo get_roadfood_post_thumbnail('post-thumbnail', 'bg-image');?>
									</div><!-- /.article__image -->

									<div class="article__contnet">
										<div class="article__meta">
											<p><?php the_category(', ');?></p>
										</div><!-- /.article__meta -->

										<div class="article__title">
											<a href="<?php the_permalink();?>"><?php the_title();?></a>
										</div><!-- /.article__title -->
										<?php if ( !empty( get_the_content() ) ): ?>
										<div class="article__entry">											
											<?php echo get_excerpt(); ?>											
										</div><!-- /.article__entry -->
										<?php endif; ?>
									</div><!-- /.article__contnet -->
								</div><!-- /.article__inner -->
							</div><!-- /.article -->
						</div><!-- /.col-md-4 -->
						<?php endwhile;?>
					</div><!-- /.row -->
				</div><!-- /.articles -->
			</div><!-- /.section__body -->
		</div><!-- /.container -->
	</section><!-- /.section-articles -->
	<?php endif; wp_reset_postdata();?>
</div><!-- /.main -->
<?php get_footer(); ?>