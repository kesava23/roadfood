<?php get_header(); ?>

<div id="content">
	<?php if (have_posts()) : ?>

	<div class="post">
		<div class="title">
			<h1><?php _e('Search Results', 'base'); ?></h1>
		</div>
	</div>

	<?php while (have_posts()) : the_post(); ?>
	<div class="post" id="post-<?php the_ID(); ?>">
		<div class="title">
			<h2><a href="<?php the_permalink() ?>" rel="bookmark" title="<?php _e('Permanent Link to', 'base'); ?> <?php the_title_attribute(); ?>"><?php the_title(); ?></a></h2>
		</div>
		<div class="content">
			<?php the_excerpt(); ?>
		</div>
	</div>
	<?php endwhile; ?>
	
	<div class="navigation">
		<div class="next"><?php next_posts_link(__('Older Entries &raquo;', 'base')) ?></div>
		<div class="prev"><?php previous_posts_link(__('&laquo; Newer Entries', 'base')) ?></div>
	</div>
	
	<?php else : ?>
	<div class="post">
		<div class="title">
			<h2><?php _e('No posts found.', 'base'); ?></h2>
		</div>
		<div class="content">
			<p><?php _e('Try a different search?', 'base'); ?></p>
			<?php get_search_form(); ?>
		</div>
	</div>
	<?php endif; ?>
	
</div>

<?php get_sidebar(); ?>

<?php get_footer(); ?>
