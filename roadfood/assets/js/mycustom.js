jQuery(document).ready(function(){
    jQuery('.roadfood_modal').click(function(e){
		 e.preventDefault();
		 e.stopPropagation(); 
		jQuery('body').addClass('modal-open');
		jQuery('body').append('<div class="modal-backdrop fade show"></div>');
		jQuery('#filter-modal').show(function(){
			jQuery(this).addClass('show');
			jQuery('.shareaholic-share-button').css('opacity', '0');
		});
			 	
	 });
	jQuery('button.close').click(function(){
	
		  jQuery('.modal').slideUp();
		  jQuery("body").find(".modal").removeClass("show");
		  jQuery('body').removeClass('modal-open');
		  jQuery('.modal-backdrop').remove(); 
		  jQuery('.shareaholic-share-button').css('opacity', '1');
	});
	
	
});

jQuery(document).click(function(event) {
  //if you click on anything except the modal itself or the "open modal" link, close the modal
  if (!jQuery(event.target).closest(".modal-content").length) {
	   jQuery('.modal').slideUp();
	  jQuery("body").find(".modal").removeClass("show");
	  jQuery('body').removeClass('modal-open');
	  jQuery('.modal-backdrop').remove();
	 
  }
});