jQuery(function($){
	$('.roadfood_loadmore').click(function(){
		
		var button = $(this),
		    data = {
			'action': 'loadmore',
			'query': roadfood_loadmore_params.posts, // get params from wp_localize_script() function
			'page' : roadfood_loadmore_params.current_page
		};
 		//console.log(roadfood_loadmore_params.posts);
		$.ajax({ 
			url : roadfood_loadmore_params.ajaxurl, // AJAX handler
			data : data,
			type : 'POST',
			beforeSend : function ( xhr ) {
				button.text('Loading...'); 
			},
			success : function( data ){
				
				if( data ) { 
					button.text( 'Show More' ).prev().after(data); // insert new posts
					roadfood_loadmore_params.current_page++;
 
					if ( roadfood_loadmore_params.current_page == roadfood_loadmore_params.max_page ) 
						button.remove(); // if last page, remove the button

					jQuery('.section-articles-default .row').append(data);
				} else {
					button.remove(); // if no data, remove the button as well
				}
			},
			error: function(xhr, status, error) {
			  var err = eval("(" + xhr.responseText + ")");
			  console.log(err.Message);
			}
		});
	});
	
	$('.roadfood-loadmore-restaurant').click(function(){
		
		var button = $(this),
		    data = {
			'action': 'loadmore_restaurant',
			'query': roadfood_loadmore_params.posts, // get params from wp_localize_script() function
			'page' : roadfood_loadmore_params.current_page
		};
//  		console.log(roadfood_loadmore_params.posts);
		$.ajax({ 
			url : roadfood_loadmore_params.ajaxurl, // AJAX handler
			data : data,
			type : 'POST',
			beforeSend : function ( xhr ) {
				button.text('Loading...'); 
			},
			success : function( data ){
				//console.log('success: ' + data);
				if( data ) { 
					button.text( 'Show More' ).prev().after(data); // insert new posts
					roadfood_loadmore_params.current_page++;
 
					if ( roadfood_loadmore_params.current_page == roadfood_loadmore_params.max_page ) 
						button.remove(); // if last page, remove the button

					jQuery('.section-best-items .items-best-list').append(data); 
					
				} else {
					button.remove(); // if no data, remove the button as well
				}
				total_items = jQuery('.section-best-items .items-best').length;
				jQuery('.total-best-items').text(total_items);
			},
			error: function(xhr, status, error) {
			  var err = eval("(" + xhr.responseText + ")");
			  console.log(err.Message);
			}
		});
	});
	
	//load more restaurant with filter
	$('.loadmore-restaurant-filter').click(function(){
		var data_type = $(this).attr('data_type');
		var data_select = $(this).attr('data_select');
		var data_paged = parseInt($(this).attr('data_paged'));
		var button = $(this),
		    data = {
			'action': 'loadmore_restaurant_from_filter',
			'query': roadfood_loadmore_params.posts, // get params from wp_localize_script() function
			'page' : data_paged,
			'data_type': data_type,
			'data_select': data_select
		};
 		
		$.ajax({ 
			url : roadfood_loadmore_params.ajaxurl, // AJAX handler
			data : data,
			type : 'POST',
			beforeSend : function ( xhr ) {
				button.text('Loading...'); 
			},
			success : function( data ){
				//console.log('success: ' + data);
				if( data ) { 
					button.text( 'Show More' ).prev().after(data); // insert new posts
					data_paged++;
 
					if ( data_paged == roadfood_loadmore_params.max_page ) 
						button.hide(); // if last page, remove the button
					else{
						button.show();
					}
					jQuery('.section-best-items .items-best-list').append(data); 
					button.attr('data_paged', data_paged);
				} else {
					button.hide(); // if no data, hide the button as well
				}
				total_items = jQuery('.section-best-items .items-best').length;
				jQuery('.total-best-items').text(total_items);
			},
			error: function(xhr, status, error) {
			  var err = eval("(" + xhr.responseText + ")");
			  console.log(err.Message);
			}
		});
	});

	// $('.roadfood-loadmore-dishes').click(function(){
	$('body').on('click', '.roadfood-loadmore-dishes', function(){
		var button = $(this),
		    data = {
			'action': 'loadmore_dish',
			'query': roadfood_loadmore_params.posts, 
			// 'query': roadfood_loadmore_params.query_temp, 
			'page' : roadfood_loadmore_params.current_page
		};
 		//console.log(roadfood_loadmore_params.posts);
		$.ajax({ 
			url : roadfood_loadmore_params.ajaxurl, // AJAX handler
			data : data,
			type : 'POST',
			beforeSend : function ( xhr ) {
				button.text('Loading...'); 
			},
			success : function( response_data ){
				//console.log('success: ' + data);
				if( response_data ) { 
					// button.text( 'Show More' ).prev().after(response_data); // insert new posts
					button.text( 'Show More' );
					button_cont = button.parent();
					button_cont.remove();
					jQuery('.section-items .items').append(response_data); 
					jQuery('.section-items .items').append(button_cont); 

					roadfood_loadmore_params.current_page++;
 
					if ( roadfood_loadmore_params.current_page == roadfood_loadmore_params.max_page ) 
						button.remove(); // if last page, remove the button
					
				} else {
					button.remove(); // if no data, remove the button as well
				}
				total_items = jQuery('.section-items .item').length;
				jQuery('.total-items').text(total_items);
			},
			error: function(xhr, status, error) {
			  var err = eval("(" + xhr.responseText + ")");
			  console.log(err.Message);
			}
		});
	});
	
	$('.js-loadmore-recipes').click(function(e){
		e.preventDefault();
		var restaurant_id = $(this).attr('data-id');
		
		var button = $(this),
		    data = {
			'action': 'loadmore_recipes',
			'query': roadfood_loadmore_params.posts, // get params from wp_localize_script() function
			'page' : roadfood_loadmore_params.current_page,
			'restaurant_id': restaurant_id
		};
//  		console.log(roadfood_loadmore_params.posts);
		$.ajax({ 
			url : roadfood_loadmore_params.ajaxurl, // AJAX handler
			data : data,
			type : 'POST',
			beforeSend : function ( xhr ) {
				button.text('Loading...'); 
			},
			success : function( data ){
				//console.log('success: ' + data);
				if( data ) { 
					button.text( 'Show More' ).prev().after(data); // insert new posts
					roadfood_loadmore_params.current_page++;
 
					if ( roadfood_loadmore_params.current_page == roadfood_loadmore_params.max_page ) 
						button.remove(); // if last page, remove the button

					jQuery('.boxes-type-1 .row').append(data); 
					
				} else {
					button.remove(); // if no data, remove the button as well
				}
				
			},
			error: function(xhr, status, error) {
			  var err = eval("(" + xhr.responseText + ")");
			  console.log(err.Message);
			}
		});
	});
	
	$('.js-loadmore-tours').click(function(){
		
		var button = $(this),
		    data = {
			'action': 'loadmore_tours',
			'query': roadfood_loadmore_params.posts, // get params from wp_localize_script() function
			'page' : roadfood_loadmore_params.current_page
		};
//  		console.log(roadfood_loadmore_params.posts);
		$.ajax({ 
			url : roadfood_loadmore_params.ajaxurl, // AJAX handler
			data : data,
			type : 'POST',
			beforeSend : function ( xhr ) {
				button.text('Loading...'); 
			},
			success : function( data ){
				//console.log('success: ' + data);
				if( data ) { 
					button.text( 'Show More' ).prev().after(data); // insert new posts
					roadfood_loadmore_params.current_page++;
 
					if ( roadfood_loadmore_params.current_page == roadfood_loadmore_params.max_page ) 
						button.remove(); // if last page, remove the button

					jQuery('.section-articles-default.tours .row').append(data); 
					
				} else {
					button.remove(); // if no data, remove the button as well
				}
			
			},
			error: function(xhr, status, error) {
			  var err = eval("(" + xhr.responseText + ")");
			  console.log(err.Message);
			}
		});
	});
	
	$('.js-loadmore-article').click(function(){
		
		var button = $(this),
		    data = {
			'action': 'loadmore_article',
			'query': roadfood_loadmore_params.posts, // get params from wp_localize_script() function
			'page' : roadfood_loadmore_params.current_page
		};
 		//console.log(roadfood_loadmore_params.posts);
		$.ajax({ 
			url : roadfood_loadmore_params.ajaxurl, // AJAX handler
			data : data,
			type : 'POST',
			beforeSend : function ( xhr ) {
				button.text('Loading...'); 
			},
			success : function( data ){
				
				if( data ) { 
					button.text( 'Show More' ).prev().after(data); // insert new posts
					roadfood_loadmore_params.current_page++;
 
					if ( roadfood_loadmore_params.current_page == roadfood_loadmore_params.max_page ) 
						button.remove(); // if last page, remove the button

					jQuery('.section-articles-default .row').append(data);
				} else {
					button.remove(); // if no data, remove the button as well
				}
			},
			error: function(xhr, status, error) {
			  var err = eval("(" + xhr.responseText + ")");
			  console.log(err.Message);
			}
		});
	});
	
	$('.js-loadmore-article-exclude-current').click(function(){
		var post_type = $(this).attr('data-type');
		var button = $(this),
		    data = {
			'action': 'loadmore_article_exclude_current',
			'query': roadfood_loadmore_params.posts, // get params from wp_localize_script() function
			'page' : roadfood_loadmore_params.current_page,
		    'post_type': post_type
		};
 		//console.log(roadfood_loadmore_params.posts);
		$.ajax({ 
			url : roadfood_loadmore_params.ajaxurl, // AJAX handler
			data : data,
			type : 'POST',
			beforeSend : function ( xhr ) {
				button.text('Loading...'); 
			},
			success : function( data ){
				
				if( data ) { 
					button.text( 'Show More' ).prev().after(data); // insert new posts
					roadfood_loadmore_params.current_page++;
 
					if ( roadfood_loadmore_params.current_page == roadfood_loadmore_params.max_page ) 
						button.remove(); // if last page, remove the button

					jQuery('.section-articles-default .row').append(data);
				} else {
					button.remove(); // if no data, remove the button as well
				}
			},
			error: function(xhr, status, error) {
			  var err = eval("(" + xhr.responseText + ")");
			  console.log(err.Message);
			}
		});
	});
	
	//filter selection
	$('.roadfood_modal').click(function(){
		var filter_type = $(this).attr('data-filter');
		var filter_container = $('.filter-selection');
		var button = $(this),
		    data = {
			'action': 'filter_selection',
		    'filter_type': filter_type
		};
 		//console.log(roadfood_loadmore_params.posts);
		$.ajax({ 
			url : roadfood_loadmore_params.ajaxurl, // AJAX handler
			data : data,
			type : 'POST',
			beforeSend : function ( xhr ) {
				filter_container.html('<h5 class="filter-heading">Loading...</h5>'); 
			},
			success : function( data ){
				filter_container.html('');
				filter_container.append(data);
			
			},
			error: function(xhr, status, error) {
			  var err = eval("(" + xhr.responseText + ")");
			  console.log(err.Message);
			}
		});
	});
	
	//filter modal options
	jQuery('.modal-content').on('click', '.js-modal-filter-option', function() {
		var data_type = $(this).attr('data-type');
		var data_select = $(this).attr('data-select');
		var button = $(this),
		    data = {
			'action': 'filter_result',					
		    'data_select': data_select,
			'data_type' : data_type,
		};
 		//console.log(roadfood_loadmore_params.posts);
		$.ajax({ 
			url : roadfood_loadmore_params.ajaxurl, // AJAX handler
			data : data,
			type : 'POST',
			beforeSend : function ( xhr ) {
				jQuery('.section-best-items').html('');
			},
			success : function( response_data ){
				jQuery('.modal').slideUp();
				  jQuery("body").find(".modal").removeClass("show");
				  jQuery('body').removeClass('modal-open');
				  jQuery('.modal-backdrop').remove(); 
				  jQuery('.shareaholic-share-button').css('opacity', '1');
				
				if( response_data ) { 
					jQuery('.section-best-items').append(response_data);
					if(data.data_type == 'dish_type'){
						jQuery('.section-sort').remove();
						jQuery('.section-items').remove();
						jQuery('.section__actions').remove();
						jQuery('.section__content').prepend(response_data);
					}
				} else {
					jQuery('.section-best-items').html('<p>No result found.</p>'); // if no data, remove the button as well
				}
			},
			error: function(xhr, status, error) {
			  var err = eval("(" + xhr.responseText + ")");
			  console.log(err.Message);
			}
		});
	});
	
	//load more restaurant from restaurant near me modal
	$('.loadmore-restaurant-nearme').click(function(){
		var data_type = $(this).attr('data_type');
		var data_city = $(this).attr('data_city');
		var data_state = $(this).attr('data_state');
		var data_paged = parseInt($(this).attr('data_paged'));
		var button = $(this),
		    data = {
			'action': 'loadmore_restaurant_nearme',
			'query': roadfood_loadmore_params.posts, // get params from wp_localize_script() function
			'page' : data_paged,
			'data_type': data_type,
			'data_city': data_city,
			'data_state': data_state,
		};
 		
		$.ajax({ 
			url : roadfood_loadmore_params.ajaxurl, // AJAX handler
			data : data,
			type : 'POST',
			beforeSend : function ( xhr ) {
				button.text('Loading...'); 
			},
			success : function( data ){
				//console.log('success: ' + data);
				if( data ) { 
					button.text( 'Show More' ).prev().after(data); // insert new posts
					data_paged++;
 
					if ( data_paged == roadfood_loadmore_params.max_page ) 
						button.hide(); // if last page, remove the button
					else{
						button.show();
					}
					jQuery('.section-nearme-items .items-best-list').append(data); 
					button.attr('data_paged', data_paged);
				} else {
					button.hide(); // if no data, hide the button as well
				}
				total_items = jQuery('.section-nearme-items .items-best').length;
				jQuery('.total-nearme-items').text(total_items);
			},
			error: function(xhr, status, error) {
			  var err = eval("(" + xhr.responseText + ")");
			  console.log(err.Message);
			}
		});
	});
	
});