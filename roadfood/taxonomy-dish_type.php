<?php get_header(); ?>
<div class="main">
	<?php if(function_exists('bcn_display')):?>
	<div class="section-breadcrumb">
		<div class="container">
			<nav aria-label="breadcrumb">
				<ol class="breadcrumb breadcrumbs" typeof="BreadcrumbList" vocab="https://schema.org/">
					<?php custom_bcn_display_list();?>
				</ol>
			</nav>
		</div><!-- /.container -->
	</div><!-- /.section-breadcrumb -->
	<?php endif;?>
	<section class="section-title">
		<div class="container">
			<h1><?php single_term_title('',true); ?></h1>
		</div><!-- /.container -->
	</section><!-- /.section-title -->

	<section class="section-fullwidth-feed">
		<div class="container-fluid">
			<div class="row">
				<div class="col-md-2 col-4 col-no-padding">
					<div class="feed-item bg-parent js-image-fit">
						<a href="#" class="feed__item-link"></a>

						<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/temp/pizza-1.jpg" alt="" class="bg-image feed__item-bg">
					</div><!-- /.feed-item -->
				</div><!-- /.col-md-2 -->

				<div class="col-md-2 col-4 col-no-padding">
					<div class="feed-item bg-parent js-image-fit">
						<a href="#" class="feed__item-link"></a>

						<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/temp/pizza-2.jpg" alt="" class="bg-image feed__item-bg">
					</div><!-- /.feed-item -->
				</div><!-- /.col-md-2 -->

				<div class="col-md-2 col-4 col-no-padding">
					<div class="feed-item bg-parent js-image-fit">
						<a href="#" class="feed__item-link"></a>

						<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/temp/pizza-3.jpg" alt="" class="bg-image feed__item-bg">
					</div><!-- /.feed-item -->
				</div><!-- /.col-md-2 -->

				<div class="col-md-2 col-4 col-no-padding">
					<div class="feed-item bg-parent js-image-fit">
						<a href="#" class="feed__item-link"></a>

						<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/temp/pizza-4.jpg" alt="" class="bg-image feed__item-bg">
					</div><!-- /.feed-item -->
				</div><!-- /.col-md-2 -->

				<div class="col-md-2 col-4 col-no-padding">
					<div class="feed-item bg-parent js-image-fit">
						<a href="#" class="feed__item-link"></a>

						<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/temp/pizza-5.jpg" alt="" class="bg-image feed__item-bg">
					</div><!-- /.feed-item -->
				</div><!-- /.col-md-2 -->

				<div class="col-md-2 col-4 col-no-padding">
					<div class="feed-item bg-parent js-image-fit">
						<a href="#" class="feed__item-link"></a>

						<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/temp/pizza-6.jpg" alt="" class="bg-image feed__item-bg">
					</div><!-- /.feed-item -->
				</div><!-- /.col-md-2 -->

				<div class="col-md-2 col-4 col-no-padding">
					<div class="feed-item bg-parent js-image-fit">
						<a href="#" class="feed__item-link"></a>

						<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/temp/pizza-7.jpg" alt="" class="bg-image feed__item-bg">
					</div><!-- /.feed-item -->
				</div><!-- /.col-md-2 -->

				<div class="col-md-2 col-4 col-no-padding">
					<div class="feed-item bg-parent js-image-fit">
						<a href="#" class="feed__item-link"></a>

						<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/temp/pizza-8.jpg" alt="" class="bg-image feed__item-bg">
					</div><!-- /.feed-item -->
				</div><!-- /.col-md-2 -->

				<div class="col-md-2 col-4 col-no-padding">
					<div class="feed-item bg-parent js-image-fit">
						<a href="#" class="feed__item-link"></a>

						<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/temp/pizza-9.jpg" alt="" class="bg-image feed__item-bg">
					</div><!-- /.feed-item -->
				</div><!-- /.col-md-2 -->

				<div class="col-md-2 col-4 col-no-padding">
					<div class="feed-item bg-parent js-image-fit">
						<a href="#" class="feed__item-link"></a>

						<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/temp/pizza-10.jpg" alt="" class="bg-image feed__item-bg">
					</div><!-- /.feed-item -->
				</div><!-- /.col-md-2 -->

				<div class="col-md-2 col-4 col-no-padding">
					<div class="feed-item bg-parent js-image-fit">
						<a href="#" class="feed__item-link"></a>

						<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/temp/pizza-11.jpg" alt="" class="bg-image feed__item-bg">
					</div><!-- /.feed-item -->
				</div><!-- /.col-md-2 -->

				<div class="col-md-2 col-4 col-no-padding">
					<div class="feed-item bg-parent js-image-fit">
						<a href="#" class="feed__item-link"></a>

						<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/temp/pizza-12.jpg" alt="" class="bg-image feed__item-bg">
					</div><!-- /.feed-item -->
				</div><!-- /.col-md-2 -->
			</div><!-- /.row -->
		</div><!-- /.container-fluid -->
	</section><!-- /.section-fullwidth-feed -->

	<section class="section-banner-mobile d-block d-lg-none">
		<a href="#">
			<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/temp/banner-mobile.jpg" alt="">
		</a>
	</section><!-- /.section-banner-mobile -->

	<section class="section-base-layout">
		<div class="container">
			<div class="row">
				<div class="col-lg-8">
					<div class="section__content">
						<section class="section-plain-text">
							<div class="section__entry">
								<?php $description =  term_description();?>
								<?php echo str_replace('<!--more-->','<div class="section__entry-hidden">',$description);?></div>
								<!-- /.section__entry-hidden -->
							</div><!-- /.section__entry -->

							<div class="section__actions">
								<a href="#" class="read-more">Continue Reading</a>
							</div><!-- /.section__actions -->
						</section><!-- /.section-plain-text -->

						<section class="section-base">
							<div class="section__head">
								<h2>America’s Most Famous Pizza Dishes</h2>

								<p>Lorem ipsum dolar sit amet non torda con polos a sin casa</p>
							</div><!-- /.section__head -->

							<div class="section__body">
								<div class="boxes-large">
									<div class="row">
										<div class="col-md-6">
											<div class="box-large">
												<a href="#" class="box__link"></a>
												<div class="box__image bg-parent js-image-fit">
													<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/temp/pizza-large-1.jpg" alt="" class="bg-image">
												</div><!-- /.box__image -->

												<div class="box__text">
													<p>Ipdar Corsalos A MinDar For Naddos</p>
												</div><!-- /.box__text -->
											</div><!-- /.box-large -->
										</div><!-- /.col-md-6 -->

										<div class="col-md-6">
											<div class="box-large">
												<a href="#" class="box__link"></a>
												<div class="box__image bg-parent js-image-fit">
													<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/temp/pizza-large-2.jpg" alt="" class="bg-image">
												</div><!-- /.box__image -->

												<div class="box__text">
													<p>Lorem Ipsum Dolar Sit Amet Non Torda</p>
												</div><!-- /.box__text -->
											</div><!-- /.box-large -->
										</div><!-- /.col-md-6 -->

										<div class="col-md-6">
											<div class="box-large">
												<a href="#" class="box__link"></a>
												<div class="box__image bg-parent js-image-fit">
													<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/temp/pizza-large-3.jpg" alt="" class="bg-image">
												</div><!-- /.box__image -->

												<div class="box__text">
													<p>Ipdar Corsalos A MinDar For Naddos</p>
												</div><!-- /.box__text -->
											</div><!-- /.box-large -->
										</div><!-- /.col-md-6 -->

										<div class="col-md-6">
											<div class="box-large">
												<a href="#" class="box__link"></a>
												<div class="box__image bg-parent js-image-fit">
													<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/temp/pizza-large-4.jpg" alt="" class="bg-image">
												</div><!-- /.box__image -->

												<div class="box__text">
													<p>Lorem Ipsum Dolar Sit Amet Non Torda</p>
												</div><!-- /.box__text -->
											</div><!-- /.box-large -->
										</div><!-- /.col-md-6 -->
									</div><!-- /.row -->
								</div><!-- /.boxes-large -->
							</div><!-- /.section__body -->

							<div class="section__actions">
								<a href="#" class="btn btn--small btn--with-shape">
									<span>View More</span>

									<svg width="27px" height="20px" viewBox="0 0 27 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="Asset-1" class="svg-color" fill="#FFFFFF" fill-rule="nonzero"><path d="M1.76,10.8 L14.76,13.8 L14.76,18.6 C14.7663086,18.9826391 14.9904405,19.3281223 15.3372831,19.4898403 C15.6841256,19.6515583 16.0928461,19.6011474 16.39,19.36 L26.2,10.54 C26.4186854,10.3500696 26.5442752,10.0746496 26.5442752,9.785 C26.5442752,9.4953504 26.4186854,9.21993042 26.2,9.03 L16.4,0.25 C16.1042499,0.00985886916 15.6977449,-0.0413773288 15.3516561,0.117865966 C15.0055674,0.27710926 14.7800248,0.619164918 14.77,1 L14.77,5.8 L1.77,8.8 C1.21771526,8.7972386 0.76776146,9.24271526 0.764987267,9.795 C0.762238613,10.3472847 1.20771526,10.7972386 1.76,10.8 Z" id="Path"></path></g></g></svg>
								</a>
							</div>
						</section><!-- /.section-base -->

						<section class="section-base">
							<header class="section__head">
								<h2>Best Pizza Restaurants</h2>
							</header><!-- /.section__head -->

							<div class="section__body">
								<div class="boxes">
									<div class="row row-small-margin">
										<div class="col-md-4 col-6 col-small-padding">
											<div class="box">
												<div class="box__inner">
													<a href="#" class="box__link"></a>

													<div class="box__image bg-parent js-image-fit">
														<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/temp/pizza-restaurant-1.jpg" alt="" class="bg-image">
													</div><!-- /.box__image -->

													<div class="box__content">
														<h5>Restaurant Name That Has Two Lines</h5>

														<p>SEATTLE, WA</p>
													</div><!-- /.box__content -->
												</div><!-- /.box__inner -->
											</div><!-- /.box -->
										</div><!-- /.col-md-4 -->

										<div class="col-md-4 col-6 col-small-padding">
											<div class="box">
												<div class="box__inner">
													<a href="#" class="box__link"></a>

													<div class="box__image bg-parent js-image-fit">
														<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/temp/pizza-restaurant-2.jpg" alt="" class="bg-image">
													</div><!-- /.box__image -->

													<div class="box__content">
														<h5>Restaurant Name One Line</h5>

														<p>BOSTON, MA</p>
													</div><!-- /.box__content -->
												</div><!-- /.box__inner -->
											</div><!-- /.box -->
										</div><!-- /.col-md-4 -->

										<div class="col-md-4 col-6 col-small-padding">
											<div class="box">
												<div class="box__inner">
													<a href="#" class="box__link"></a>

													<div class="box__image bg-parent js-image-fit">
														<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/temp/pizza-restaurant-3.jpg" alt="" class="bg-image">
													</div><!-- /.box__image -->

													<div class="box__content">
														<h5>Restaurant Name One Line</h5>

														<p>PORTLAND, OR</p>
													</div><!-- /.box__content -->
												</div><!-- /.box__inner -->
											</div><!-- /.box -->
										</div><!-- /.col-md-4 -->

										<div class="col-md-4 col-6 col-small-padding">
											<div class="box">
												<div class="box__inner">
													<a href="#" class="box__link"></a>

													<div class="box__image bg-parent js-image-fit">
														<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/temp/pizza-restaurant-4.jpg" alt="" class="bg-image">
													</div><!-- /.box__image -->

													<div class="box__content">
														<h5>Restaurant Name One Line</h5>

														<p>WASHINGTON, DC</p>
													</div><!-- /.box__content -->
												</div><!-- /.box__inner -->
											</div><!-- /.box -->
										</div><!-- /.col-md-4 -->

										<div class="col-md-4 col-6 col-small-padding">
											<div class="box">
												<div class="box__inner">
													<a href="#" class="box__link"></a>

													<div class="box__image bg-parent js-image-fit">
														<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/temp/pizza-restaurant-5.jpg" alt="" class="bg-image">
													</div><!-- /.box__image -->

													<div class="box__content">
														<h5>Restaurant Name One Line</h5>

														<p>PITTSBURGH, PA</p>
													</div><!-- /.box__content -->
												</div><!-- /.box__inner -->
											</div><!-- /.box -->
										</div><!-- /.col-md-4 -->

										<div class="col-md-4 col-6 col-small-padding">
											<div class="box">
												<div class="box__inner">
													<a href="#" class="box__link"></a>

													<div class="box__image bg-parent js-image-fit">
														<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/temp/pizza-restaurant-6.jpg" alt="" class="bg-image">
													</div><!-- /.box__image -->

													<div class="box__content">
														<h5>Restaurant Name One Line</h5>

														<p>SAN FRANCISCO, CA</p>
													</div><!-- /.box__content -->
												</div><!-- /.box__inner -->
											</div><!-- /.box -->
										</div><!-- /.col-md-4 -->
									</div><!-- /.row -->
								</div><!-- /.boxes -->
							</div><!-- /.section__body -->

							<div class="section__actions">
								<a href="#" class="btn btn--with-shape">
									<span>More Restaurants</span>

									<svg width="27px" height="20px" viewBox="0 0 27 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="Asset-1" class="svg-color" fill="#FFFFFF" fill-rule="nonzero"><path d="M1.76,10.8 L14.76,13.8 L14.76,18.6 C14.7663086,18.9826391 14.9904405,19.3281223 15.3372831,19.4898403 C15.6841256,19.6515583 16.0928461,19.6011474 16.39,19.36 L26.2,10.54 C26.4186854,10.3500696 26.5442752,10.0746496 26.5442752,9.785 C26.5442752,9.4953504 26.4186854,9.21993042 26.2,9.03 L16.4,0.25 C16.1042499,0.00985886916 15.6977449,-0.0413773288 15.3516561,0.117865966 C15.0055674,0.27710926 14.7800248,0.619164918 14.77,1 L14.77,5.8 L1.77,8.8 C1.21771526,8.7972386 0.76776146,9.24271526 0.764987267,9.795 C0.762238613,10.3472847 1.20771526,10.7972386 1.76,10.8 Z" id="Path"></path></g></g></svg>
								</a>
							</div><!-- /.section__actions -->
						</section>

						<section class="section-base">
							<header class="section__head">
								<h2>Featured Pizza Recipes</h2>

								<p>Lorem ipsum dolar sit amet non torda con polos a sin casa</p>
							</header><!-- /.section__head -->

							<div class="section__body">
								<div class="boxes-type-1">
									<div class="row">
										<div class="col-md-3 col-6">
											<div class="box">
												<div class="box__inner">
													<a href="#" class="box__link"></a>

													<div class="box__image bg-parent js-image-fit">
														<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/temp/featured-pizza-recipe-1.jpg" alt="" class="bg-image">
													</div><!-- /.box__image -->

													<div class="box__content">
														<p>Ipdar Corsalos A MinDar For Naddos</p>

													</div><!-- /.box__content -->
												</div><!-- /.box__inner -->
											</div><!-- /.box -->
										</div><!-- /.col-md-4 -->

										<div class="col-md-3 col-6">
											<div class="box">
												<div class="box__inner">
													<a href="#" class="box__link"></a>

													<div class="box__image bg-parent js-image-fit">
														<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/temp/featured-pizza-recipe-2.jpg" alt="" class="bg-image">
													</div><!-- /.box__image -->

													<div class="box__content">
														<p>Lorem Ipsum Dolar Sit Amet Non Torda</p>
													</div><!-- /.box__content -->
												</div><!-- /.box__inner -->
											</div><!-- /.box -->
										</div><!-- /.col-md-4 -->

										<div class="col-md-3 col-6">
											<div class="box">
												<div class="box__inner">
													<a href="#" class="box__link"></a>

													<div class="box__image bg-parent js-image-fit">
														<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/temp/featured-pizza-recipe-3.jpg" alt="" class="bg-image">
													</div><!-- /.box__image -->

													<div class="box__content">
														<p>Ipsum Dolar Sit Amet Non Torda</p>
													</div><!-- /.box__content -->
												</div><!-- /.box__inner -->
											</div><!-- /.box -->
										</div><!-- /.col-md-4 -->

										<div class="col-md-3 col-6">
											<div class="box">
												<div class="box__inner">
													<a href="#" class="box__link"></a>

													<div class="box__image bg-parent js-image-fit">
														<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/temp/featured-pizza-recipe-4.jpg" alt="" class="bg-image">
													</div><!-- /.box__image -->

													<div class="box__content">
														<p>Torda Sor Cantos Lin Pasa For Santos</p>
													</div><!-- /.box__content -->
												</div><!-- /.box__inner -->
											</div><!-- /.box -->
										</div><!-- /.col-md-4 -->
									</div><!-- /.row -->
								</div><!-- /.boxes -->
							</div><!-- /.section__body -->

							<div class="section__actions">
								<a href="#" class="btn btn--with-shape">
									<span>More Recipes</span>

									<svg width="27px" height="20px" viewBox="0 0 27 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="Asset-1" class="svg-color" fill="#FFFFFF" fill-rule="nonzero"><path d="M1.76,10.8 L14.76,13.8 L14.76,18.6 C14.7663086,18.9826391 14.9904405,19.3281223 15.3372831,19.4898403 C15.6841256,19.6515583 16.0928461,19.6011474 16.39,19.36 L26.2,10.54 C26.4186854,10.3500696 26.5442752,10.0746496 26.5442752,9.785 C26.5442752,9.4953504 26.4186854,9.21993042 26.2,9.03 L16.4,0.25 C16.1042499,0.00985886916 15.6977449,-0.0413773288 15.3516561,0.117865966 C15.0055674,0.27710926 14.7800248,0.619164918 14.77,1 L14.77,5.8 L1.77,8.8 C1.21771526,8.7972386 0.76776146,9.24271526 0.764987267,9.795 C0.762238613,10.3472847 1.20771526,10.7972386 1.76,10.8 Z" id="Path"></path></g></g></svg>
								</a>
							</div><!-- /.section__actions -->
						</section>
					</div><!-- /.section__content -->
				</div><!-- /.col-md-8 -->

				<div class="col-lg-4 d-none d-lg-block">
					<div class="section__sidebar">
						<ul class="widgets">
							<li class="widget widget--banner">
								<a href="#">
									<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/temp/banner.jpg" alt="">
								</a>
							</li><!-- /.widget widget--banner -->
						</ul><!-- /.widgets -->
					</div><!-- /.section__sidebar -->
				</div><!-- /.col-md-4 -->
			</div><!-- /.row -->
		</div><!-- /.container -->
	</section><!-- /.section-base-layout -->

	<section class="section-articles">
		<div class="container">
			<header class="section__head">
				<h2>Latest Articles & Guides</h2>

				<a href="#" class="view-more">View All Articles & Guides</a>
			</header><!-- /.section__head -->

			<div class="section__body">
				<div class="articles">
					<div class="row">
						<div class="col-md-4 col-6 ">
							<div class="article">
								<div class="article__inner">
									<div class="article__image bg-parent js-image-fit">
										<a href="#" class="article__image-link"></a>
										<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/temp/article-1.jpg" alt="" class="bg-image">
									</div><!-- /.article__image -->

									<div class="article__contnet">
										<div class="article__meta">
											<p>LORASO SIN CASA</p>
										</div><!-- /.article__meta -->

										<div class="article__title">
											<a href="#">Eating Around the Carolina Foothills</a>
										</div><!-- /.article__title -->

										<div class="article__entry">
											<p>Lorem ipsum dolar sit amet non torda con polos a sin casa min darros a laso zoncosa fin naddos a lorm ipsum dolar...</p>
										</div><!-- /.article__entry -->
									</div><!-- /.article__contnet -->
								</div><!-- /.article__inner -->
							</div><!-- /.article -->
						</div><!-- /.col-md-4 -->

						<div class="col-md-4 col-6 ">
							<div class="article">
								<div class="article__inner">
									<div class="article__image bg-parent js-image-fit">
										<a href="#" class="article__image-link"></a>

										<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/temp/article-2.jpg" alt="" class="bg-image">
									</div><!-- /.article__image -->

									<div class="article__contnet">
										<div class="article__meta">
											<p>LOREM IPSUM </p>
										</div><!-- /.article__meta -->

										<div class="article__title">
											<a href="#">The Best Tastes of Savannah, Georgia</a>
										</div><!-- /.article__title -->

										<div class="article__entry">
											<p>Lorem ipsum dolar sit amet non torda con polos a sin casa min darros a laso zoncosa fin naddos a lorm ipsum dolar...</p>
										</div><!-- /.article__entry -->
									</div><!-- /.article__contnet -->
								</div><!-- /.article__inner -->
							</div><!-- /.article -->
						</div><!-- /.col-md-4 -->

						<div class="col-md-4 col-6 ">
							<div class="article">
								<div class="article__inner">
									<div class="article__image bg-parent js-image-fit">
										<a href="#" class="article__image-link"></a>
										<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/temp/article-3.jpg" alt="" class="bg-image">
									</div><!-- /.article__image -->

									<div class="article__contnet">
										<div class="article__meta">
											<p>LOREM IPSUM DOLAR SIT </p>
										</div><!-- /.article__meta -->

										<div class="article__title">
											<a href="#">Best Eats in Pigeon Forge, Tennessee</a>
										</div><!-- /.article__title -->

										<div class="article__entry">
											<p>Lorem ipsum dolar sit amet non torda con polos a sin casa min darros a laso zoncosa fin naddos a lorm ipsum dolar...</p>
										</div><!-- /.article__entry -->
									</div><!-- /.article__contnet -->
								</div><!-- /.article__inner -->
							</div><!-- /.article -->
						</div><!-- /.col-md-4 -->

						<div class="col-md-4 col-6 ">
							<div class="article">
								<div class="article__inner">
									<div class="article__image bg-parent js-image-fit">
										<a href="#" class="article__image-link"></a>
										<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/temp/article-4.jpg" alt="" class="bg-image">
									</div><!-- /.article__image -->

									<div class="article__contnet">
										<div class="article__meta">
											<p>LOREM IPSUM DOLAR SIT AMET</p>
										</div><!-- /.article__meta -->

										<div class="article__title">
											<a href="#">Wilmington, North Carolina Roadfood Favorites</a>
										</div><!-- /.article__title -->

										<div class="article__entry">
											<p>Lorem ipsum dolar sit amet non torda con polos a sin casa min darros a laso zoncosa fin naddos a lorm ipsum dolar...</p>
										</div><!-- /.article__entry -->
									</div><!-- /.article__contnet -->
								</div><!-- /.article__inner -->
							</div><!-- /.article -->
						</div><!-- /.col-md-4 -->

						<div class="col-md-4 col-6 ">
							<div class="article">
								<div class="article__inner">
									<div class="article__image bg-parent js-image-fit">
										<a href="#" class="article__image-link"></a>
										<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/temp/article-5.jpg" alt="" class="bg-image">
									</div><!-- /.article__image -->

									<div class="article__contnet">
										<div class="article__meta">
											<p>LOREM IPSUM </p>
										</div><!-- /.article__meta -->

										<div class="article__title">
											<a href="#">Myrtle Beach, South Carolina’s Hidden Gems</a>
										</div><!-- /.article__title -->

										<div class="article__entry">
											<p>Lorem ipsum dolar sit amet non torda con polos a sin casa min darros a laso zoncosa fin naddos a lorm ipsum dolar...</p>
										</div><!-- /.article__entry -->
									</div><!-- /.article__contnet -->
								</div><!-- /.article__inner -->
							</div><!-- /.article -->
						</div><!-- /.col-md-4 -->

						<div class="col-md-4 col-6 ">
							<div class="article">
								<div class="article__inner">
									<div class="article__image bg-parent js-image-fit">
										<a href="#" class="article__image-link"></a>
										<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/temp/article-6.jpg" alt="" class="bg-image">
									</div><!-- /.article__image -->

									<div class="article__contnet">
										<div class="article__meta">
											<p>LOREM IPSUM DOLAR SIT </p>
										</div><!-- /.article__meta -->

										<div class="article__title">
											<a href="#">Best Bets: Hidden Gem  Restaurants in Las Vegas</a>
										</div><!-- /.article__title -->

										<div class="article__entry">
											<p>Lorem ipsum dolar sit amet non torda con polos a sin casa min darros a laso zoncosa fin naddos a lorm ipsum dolar...</p>
										</div><!-- /.article__entry -->
									</div><!-- /.article__contnet -->
								</div><!-- /.article__inner -->
							</div><!-- /.article -->
						</div><!-- /.col-md-4 -->
					</div><!-- /.row -->
				</div><!-- /.articles -->
			</div><!-- /.section__body -->
		</div><!-- /.container -->
	</section><!-- /.section-articles -->
</div><!-- /.main -->
<?php get_footer(); ?>