<?php get_header(); ?>

<div class="main">
	<section class="section-breadcrumb">
		<div class="container">
			<?php
				if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('
					<div id="breadcrumbs">','</div>');
				}	
			?>
		</div>		
	</section>
 <?php if (have_posts()) : ?>	
	<?php while (have_posts()) : the_post(); ?>
	<div class="post" id="post-<?php the_ID(); ?>">
	<section class="section-article-single-heading" >
		<div class="container">
			<div class="section__meta">
				<div class="section__name">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo-icon.svg" alt="">
					<a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>" class="text-decoration-none link-hover-primary"><h5><?php the_author(); ?></h5></a>
				</div><!-- /.section__name -->
				<div class="section__date">
					<p><?php the_time('F j, Y') ?></p>
				</div><!-- /.section__date -->
			</div><!-- /.section__meta -->

			<div class="section__head">
				<div class="row">
					<div class="col-lg-8">
						<h1><?php the_title(); ?></h1>

						<p><?php echo get_field('sub-heading'); ?></p>
					</div><!-- /.col-lg-8 -->
				</div><!-- /.row -->
			</div><!-- /.section__head -->
		</div><!-- /.container -->
	</section>
	<?php if(get_field('advertisement_widget', 'options')):?>
	<section class="section-banner-mobile d-block d-lg-none">
		<?php the_field('advertisement_widget', 'options')?>							
	</section>
	<?php endif;?>
	
	<section class="section-base-layout">
		<div class="container">
			<div class="row">
				<div class="col-lg-8">
					<div class="section__content">
						
						<section class="section-article-single-content">
							<?php if(!empty(get_the_post_thumbnail())){ ?>
							<div class="mb-4">
								<?php the_post_thumbnail('full'); ?>
							</div>
							<?php } ?>
							<div class="section__entry">
								<?php the_content(); ?>
							</div><!-- /.section__entry -->
							<?php
							$posts = get_field('nrf_stops'); 
							//suffle the post id
							shuffle($posts);
							$posts = array_slice($posts, 0, 2); 
							if( $posts ):
							foreach( $posts as $post):										
							?>	
							<div class="section__row">
								<div class="section__row-image">
									<a href="<?php the_permalink(); ?>" class="article__image-link"></a>
									<?php if(!empty(get_the_post_thumbnail())){ ?>
										<img src="<?php echo the_post_thumbnail_url('Medium'); ?>" alt="" class="bg-image">
									<?php }else{ ?>
										<img src="/wp-content/uploads/2020/11/no-image-icon.jpg" alt="" class="bg-image" style="border: 1px solid #ddd;">
									<?php } ?>	
								</div><!-- /.section__row-image -->

								<div class="section__row-content">
									<h2>										
										<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
									</h2>
									<?php echo excerpt(30); ?>

									<p>
										<a href="<?php the_permalink(); ?>">More Info</a>
									</p>
								</div><!-- /.section__row-content -->
							</div>
							<?php
								endforeach;
							endif;
							wp_reset_query();?>
							
							
							<div class="pt-4"></div>
							<div class="section__meta">
								<div class="section__name">
									<img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo-icon.svg" alt="">
									<a href="<?php echo get_author_posts_url(get_the_author_meta('ID')); ?>" class="text-decoration-none link-hover-primary"><h5><?php the_author(); ?></h5></a>
								</div><!-- /.section__name -->
								<div class="section__date">
									<p><?php the_time('F j, Y') ?></p>
								</div><!-- /.section__date -->
							</div><!-- /.section__meta -->
						</section><!-- /.section-article-single-content -->

						<section class="section-base">
							<header class="section__head">
								<h2>Discuss</h2>

								<p>What do you think of <?php the_title(); ?>?</p>
							</header><!-- /.section__head -->

							<div class="section__body">
								<?php comments_template(); ?>
								
							</div><!-- /.section__body -->


							<div class="section__actions">
								<a href="javascript:;" class="btn-show-more js-show-more-discuss">Show More</a>
							</div><!-- /.section__actions -->
						</section><!-- /.section-base -->

						<section class="section-articles-default">
							<header class="section__head">
								<h2>Related Articles</h2>
							</header><!-- /.section__head -->

							<div class="row">
								<?php
								$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
								 $args = array(
									'post_type' => array('bests'),
									'posts_per_page' => 6,
									'orderby' => 'date',
									'order'   => 'DESC',
									'post_status' => 'publish',
									'post__not_in' => array (get_the_ID()),
									'paged' => $paged
								);
								$query = new WP_Query( $args );								
								?>
									<?php if ($query->have_posts()) : ?>
										<?php while ($query->have_posts()) : $query->the_post(); ?>
											<div class="col-lg-4 col-6">
												<article class="article-default">
													<div class="article__image bg-parent js-image-fit">
														<a href="<?php the_permalink(); ?>" class="article__image-link"></a>
														<?php if(has_post_thumbnail()){ ?>
															<img src="<?php echo the_post_thumbnail_url('medium'); ?>" alt="" class="bg-image">
														<?php }else if(get_field('dish_image')){
															$images = get_field('dish_image'); 
															if(is_array($images)){
																$g_image = $images[0];
															}else{
																$g_image = $images;
															}
														?>
															<img src="<?php echo esc_url(wp_get_attachment_image_src($g_image, 'medium')[0]); ?>" alt="" class="bg-image">
														<?php }else{ ?>
															<img src="/wp-content/uploads/2020/11/no-image-icon.jpg" alt="" class="bg-image" style="border: 1px solid #ddd;">
														<?php } ?>					
													</div><!-- /.article__image -->

													<div class="article__content">
														<h3>
															<a href="#"><?php the_title(); ?></a>
														</h3>
														<?php if ( !empty( get_the_content() ) ): ?>
														<?php echo get_excerpt(); ?>
														<?php endif; ?>
													</div><!-- /.article__content -->
												</article><!-- /.article-default -->
											</div><!-- /.col-md-4 -->
										<?php endwhile; ?>	
									<?php endif; ?>
									<?php wp_reset_query(); ?>   
									
								</div><!-- /.row -->

							<div class="section__actions">
								<a href="javascript:;" class="btn-show-more js-loadmore-article-exclude-current" data-type="bests">Show More</a>
							</div>
						</section><!-- /.section-articles-default -->
					</div><!-- /.section__content -->
				</div><!-- /.col-md-8 -->

				<?php if(get_field('advertisement_widget', 'options')):?>
					<div class="col-lg-4 d-none d-lg-block">
						<div class="section__sidebar">
							<ul class="widgets">
								<li class="widget widget--banner">
									<?php the_field('advertisement_widget', 'options')?>
								</li><!-- /.widget widget--banner -->
							</ul><!-- /.widgets -->
						</div><!-- /.section__sidebar -->
					</div><!-- /.col-md-4 -->
				<?php endif;?>
				
			</div><!-- /.row -->
		</div><!-- /.container -->
	</section>
	</div>
  <?php endwhile; ?>
	<div class="container" style="display:none;">
		<div class="navigation" >
			<div class="next"><?php previous_post_link(__('%link &raquo;', 'base')) ?></div>
			<div class="prev"><?php next_post_link(__('&laquo; %link', 'base')) ?></div>
		</div>
	</div>
	
<?php else : ?>
	<div class="post">
		<div class="title">
			<h1><?php _e('Not Found', 'base'); ?></h1>
		</div>
		<div class="content">
			<p><?php _e('Sorry, but you are looking for something that isn\'t here.', 'base'); ?></p>
		</div>
	</div>
		
<?php endif; ?>

<?php  

$post_id = get_the_ID();
echo '<pre style="display: none">';
var_dump( get_field('nrf_stops', $post_id ) );
var_dump( get_post_meta( $post_id ) );
echo '</pre>';

?>
<?php //get_sidebar(); ?>
	
<script>
(function($){
	discuss = $('.discuss');
	$.each(discuss, function(key,value) {
		$(this)
		  .find('.discuss-item')
		  .slice(3)
		  .addClass('hidden-discuss');
		
	});
	//hide show more button if comments less than 4
	var numdiscuss = $('.discuss-item').length
	if(numdiscuss < 4){
	   $('.js-show-more-discuss').hide();
	 }
	
	$('.js-show-more-discuss').click(function(){		
	  $( ".hidden-discuss" ).slideToggle( "slow", function() {});
	  $(this).text('Show Less');
	});
})(jQuery)
</script>
<?php get_footer(); ?>
