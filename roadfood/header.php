<!DOCTYPE html>
<html lang="en">
	<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<title><?php wp_title();?></title>

	<link rel="shortcut icon" type="image/x-icon" href="<?php echo get_stylesheet_directory_uri();?>/assets/images/favicon.ico">
	<?php wp_head(); ?>
</head>

<body <?php body_class()?>>
	<div class="wrapper">
		<div class="wrapper__inner">
			<header class="header">
				<div class="container container-xl">
					<div class="header__bar">
						<?php if( get_field('facebook', 'option') || get_field('instagram', 'option') || get_field('pinterest', 'option') || get_field('twitter', 'option') ):?>
						<div class="socials">
							<ul>
								<?php if( get_field('facebook', 'option') ):?>
								<li>
									<a href="<?php the_field('facebook', 'option')?>">
										<i class="fab fa-facebook-f"></i>
									</a>
								</li>
								<?php endif;?>
								<?php if( get_field('instagram', 'option') ):?>
								<li>
									<a href="<?php the_field('instagram', 'option')?>">
										<i class="fab fa-instagram"></i>
									</a>
								</li>
								<?php endif;?>
								<?php if( get_field('pinterest', 'option') ):?>
								<li>
									<a href="<?php the_field('pinterest', 'option')?>">
										<i class="fab fa-pinterest"></i>
									</a>
								</li>
								<?php endif;?>
								<?php if( get_field('twitter', 'option') ):?>
								<li>
									<a href="<?php the_field('twitter', 'option')?>">
										<i class="fab fa-twitter"></i>
									</a>
								</li>
								<?php endif;?>
							</ul>
						</div><!-- /.socials -->
						<?php endif;?>
					</div><!-- /.header__bar -->

					<div class="header__content">
						<div class="header__content-bar">
							<a href="<?php echo site_url();?>" class="logo">
								<img src="<?php echo get_stylesheet_directory_uri();?>/assets/images/logo.svg" alt="">
							</a>

							<div class="header__content-actions">
								<div class="header__search header__search--mobile">
									<a href="#" class="btn-search">
										<svg width="51px" height="51px" viewBox="0 0 51 51" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round"><g id="search" transform="translate(3.000000, 3.000000)" stroke="#0B2A4A" stroke-width="6"><circle id="Oval" cx="20" cy="20" r="20"></circle><line x1="44.07" y1="44.07" x2="34.14" y2="34.14" id="Path"></line></g></g></svg>
									</a>
								</div><!-- /.header__search -->

								<a href="#" class="nav-trigger">
									<span></span>
									<span></span>
									<span></span>
								</a>
							</div><!-- /.header__content-actions -->
						</div><!-- /.header__content-bar -->

						<div class="header__inner">
							<div class="header__container">
								<div class="header__actions header__actions--mobile">
									<div class="header__loc">
										<a href="#">
											<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 8.59 11.86"><path d="M4.29,0A4.29,4.29,0,0,0,0,4.29c0,3.07,4.29,7.57,4.29,7.57s4.3-4.5,4.3-7.57A4.3,4.3,0,0,0,4.29,0Zm0,5.72A1.43,1.43,0,1,1,5.72,4.29,1.43,1.43,0,0,1,4.29,5.72Z" style="fill:#0b2a4a"></path></svg>

											Restaurants Near Me
										</a>
									</div><!-- /.header__loc -->

									<div class="header__links">
										<ul>
											<li>
												<a href="#">Join</a>
											</li>

											<li>
												<a href="#">Sign In</a>
											</li>
										</ul>
									</div><!-- /.header__links -->
								</div><!-- /.header__actions -->

								<div class="header__nav">
								<?php 
									wp_nav_menu( array('container' => false,
										 'theme_location' => 'primary',
										 'menu_id' => 'navigation',
										 'menu_class' => 'menu',
										 'items_wrap' => '<nav class="nav"><ul id="%1$s" class="%2$s">%3$s</ul></nav>',
										 ) );
									wp_nav_menu( array('container' => false,
										 'theme_location' => 'utilities',
										 'menu_id' => '',
										 'menu_class' => '',
										 'items_wrap' => '<nav class="nav-utilities"><ul id="%1$s" class="%2$s">%3$s</ul></nav>',
										 ) ); ?>
									<?php if( get_field('facebook', 'option') || get_field('instagram', 'option') || get_field('pinterest', 'option') || get_field('twitter', 'option') ):?>
									<div class="header__socials">
										<div class="socials">
											<ul>
												<?php if( get_field('facebook', 'option') ):?>
												<li>
													<a href="<?php the_field('facebook', 'option')?>">
														<i class="fab fa-facebook-f"></i>
													</a>
												</li>
												<?php endif;?>
												<?php if( get_field('instagram', 'option') ):?>
												<li>
													<a href="<?php the_field('instagram', 'option')?>">
														<i class="fab fa-instagram"></i>
													</a>
												</li>
												<?php endif;?>
												<?php if( get_field('pinterest', 'option') ):?>
												<li>
													<a href="<?php the_field('pinterest', 'option')?>">
														<i class="fab fa-pinterest"></i>
													</a>
												</li>
												<?php endif;?>
												<?php if( get_field('twitter', 'option') ):?>
												<li>
													<a href="<?php the_field('twitter', 'option')?>">
														<i class="fab fa-twitter"></i>
													</a>
												</li>
												<?php endif;?>
											</ul>

											<p>
												Connect with us <a href="#">#Roadfood</a>
											</p>
										</div><!-- /.socials -->
									</div><!-- /.header__socials -->
									<?php endif;?>
								</div><!-- /.header__nav -->

								<div class="header__actions">
									<div class="header__loc">
										<a href="#">
											<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 8.59 11.86"><path d="M4.29,0A4.29,4.29,0,0,0,0,4.29c0,3.07,4.29,7.57,4.29,7.57s4.3-4.5,4.3-7.57A4.3,4.3,0,0,0,4.29,0Zm0,5.72A1.43,1.43,0,1,1,5.72,4.29,1.43,1.43,0,0,1,4.29,5.72Z" style="fill:#0b2a4a"></path></svg>

											Restaurants Near Me
										</a>
									</div><!-- /.header__loc -->

									<div class="header__search">
										<a href="#" class="btn-search">
											<svg width="51px" height="51px" viewBox="0 0 51 51" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd" stroke-linecap="round"><g id="search" transform="translate(3.000000, 3.000000)" stroke="#0B2A4A" stroke-width="6"><circle id="Oval" cx="20" cy="20" r="20"></circle><line x1="44.07" y1="44.07" x2="34.14" y2="34.14" id="Path"></line></g></g></svg>
										</a>
									</div><!-- /.header__search -->

									<div class="header__links">
										<ul>
											<li>
												<a href="#">Join</a>
											</li>

											<li>
												<a href="#">Sign In</a>
											</li>
										</ul>
									</div><!-- /.header__links -->
								</div><!-- /.header__actions -->
							</div><!-- /.header__container -->

						</div><!-- /.header__inner -->
					</div><!-- /.header__content -->
				</div><!-- /.container -->
			</header><!-- /.header -->