<?php
/**
* Template Name: Convert Restaurants
*
* @package WordPress
*/
wp_head();

$args = array(
	'post_type' => array('restaurants'),
	'posts_per_page' => -1,
	'post_status' => 'any',
);

$posts_query = new WP_Query($args);
$posts = $posts_query->posts;

// echo '<pre>';
// var_dump( count($posts) );
// // var_dump( $posts );
// echo '</pre>';

// add all new dishes->ID to JS array
$posts_id = '<script>array_of_ids = [';
$posts_id_ =  array();
foreach ($posts as $post) {
	array_push($posts_id_, $post->ID);
}
$posts_id .= implode(',',$posts_id_);
$posts_id .= ']</script>';
echo $posts_id; 


?>

<form action="#">
	<button id="import">Convert Restaurants</button>
	<button id="stop">Stop</button>
</form>
<div id="report"></div>
<script>
	var import_status = false;
	var row = 0;
	var count_step_max = array_of_ids.length;
	function loop_item_of_array(item_id){
		jQuery.ajax({
		    method: 'POST',
		    // url: myajax.url,
		    url : '<?php echo admin_url( 'admin-ajax.php' );?>',
		    data: {
		        'action': 'actionConvertRestaurants',
		        'post_id' : item_id,
		    }, 
		    success: function (response) {
		    	row =  row + 1;
		    	jQuery('#report').prepend('<p>' + row + '/' + count_step_max + ' | ' + response.log + '</p>');
		    	// if( row <= 3) {
		    	if( (row <= count_step_max) && (import_status != false) ) {
		    		loop_item_of_array(array_of_ids[row]);
		    	}
		    	
		    },
		    error: function (error) {
		        console.log('error= '+ error);
		    }
		});
	}

	jQuery(document).ready(function(){
		jQuery('#import').click(function(e){
			e.preventDefault();
			e.stopPropagation();
			import_status = true;
			loop_item_of_array(array_of_ids[row]);
		})
		jQuery('#stop').click(function(e){
			e.preventDefault();
			e.stopPropagation();
			import_status = false;
		})
	})


</script>



<?php
wp_footer();