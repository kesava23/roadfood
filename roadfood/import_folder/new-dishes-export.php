<?php
/**
* Template Name: NewDishes Export
*
* @package WordPress
*/

$args = array(
	'post_type'	=> 'dish',
	'posts_per_page' => -1
);

$query = new WP_Query($args);

$dishes = $query->posts;

echo '<pre>';
print_r( $dishes );
echo '</pre>';

$fp = fopen(__DIR__.'/old_dishes.csv', 'a');
foreach ($dishes as $dish) {
	fwrite( $fp, '"' . $dish->ID . '","' .$dish->post_name. '"' .PHP_EOL );
}
fclose($fp);