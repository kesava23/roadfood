<?php
/**
* Template Name: CheckOldDishes
*
* @package WordPress
*/
wp_head();
$args = array(
	'post_type' => 'dishes',
	// 'post_type' => 'ndish',
	'posts_per_page' => -1,
	// 'name' => '',
);

$dishes_query = new WP_Query($args);
$dishes = $dishes_query->posts;

// echo '<pre>';
// var_dump( count($dishes) );
// // var_dump( $dishes );
// echo '</pre>';

// add all new dishes->ID to JS array
$dishes_id = '<script>new_dishes = [';
$dishes_id_ =  array();
foreach ($dishes as $dish) {
	array_push($dishes_id_, $dish->ID);
}
$dishes_id .= implode(',',$dishes_id_);
$dishes_id .= ']</script>';
echo $dishes_id; 

// echo '<pre>';
// print_r( $dishes_id_ );
// echo '</pre>';

?>

<form action="#">
	<button id="import">Reimport old dishes</button>
	<button id="stop">Stop</button>
</form>
<div id="report"></div>
<script>
	

	count_step_max = new_dishes.length;
	console.log( new_dishes );
	console.log( count_step_max );
	import_status = false;

	function import_old_dishes(row){
		jQuery.ajax({
		    method: 'POST',
		    url : '<?php echo admin_url( 'admin-ajax.php' );?>',
		    data: {
		        'action': 'actionDishesOldToNew',
		        'post_id' : new_dishes[row],
		    }, 
		    success: function (response) {
		    	row =  row + 1;
		    	jQuery('#report').prepend(response.log);
		    	// if( row <= 3) {
		    	if(( row <= count_step_max) && (import_status !== false)) {
		    		import_old_dishes(row);
		    	}
		    	
		    },
		    error: function (error) {
		        console.log('error= '+ error);
		    }
		});
	}

	jQuery(document).ready(function(){
		jQuery('#import').click(function(e){
			e.preventDefault();
			e.stopPropagation();
			import_status = true;
			import_old_dishes(0);
		})
		jQuery('#stop').click(function(e){
			e.preventDefault();
			e.stopPropagation();
			import_status = false;
		})
	})


</script>



<?php
wp_footer();