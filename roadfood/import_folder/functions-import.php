<?php
// error_reporting(E_ALL);
// ini_set('display_errors', 1);

function the_slug_exists($post_name, $post_type) {
    global $wpdb;
    // echo "SELECT post_name FROM ".$wpdb->prefix."posts WHERE post_name = '" . $post_name . "' AND post_type = '" . $post_type . "'" .PHP_EOL;
    if($wpdb->get_row("SELECT post_name FROM wp_posts WHERE post_name = '" . $post_name . "' AND post_type = '" . $post_type . "'", 'ARRAY_A')) {
        return true;
    } else {
        return false;
    }
}


add_action('wp_ajax_actionImportCSV', 'callbackImportCSV');
add_action('wp_ajax_nopriv_actionImportCSV', 'callbackImportCSV');
function callbackImportCSV(){
    $log ='';

    $filepath = $_POST['filepath'];

    if (($handle = fopen($filepath, "r")) !== FALSE) {
        $line_key = 0;
        $row = 0;
        while (($data = fgetcsv($handle, 1000, "`")) !== FALSE) {
            $row++;
        }
        echo $row;
        fclose($handle);
    }
    echo $script;
    // return  $arrResult;
    wp_die();
}




add_action('wp_ajax_actionImportCSVRow', 'callbackImportCSVRow');
add_action('wp_ajax_nopriv_actionImportCSVRow', 'callbackImportCSVRow');
function callbackImportCSVRow(){
    $i = 0;
    $start_line = $_POST['count'];
    $filepath = $_POST['filepath'];
    $n_lines = $_POST['rows'];
    $csv = read_csv_update_row($start_line, $n_lines, $filepath);

    foreach ($csv as $csv_row) {
        // $csv_row["SLUG"],
        // $csv_row["TITLE"],
        // $csv_row["PARENT_PAGE"],
        // $csv_row["TERM_ID]"

        

        // insert user
        
        $fp = fopen(__DIR__.'/import_report.csv', 'a');
        if( (get_class($post_id) !== 'WP_Error') && ( $post_id != 0 )  ){
            // update User data
            update_field('user_imported', 1 ,'user_'.$post_id);

            // save passwrod to the log file
            $post_update = array();
            $post_update['ID'] = $csv_row['CHARITY ID'];
            $post_update['post_author'] = $post_id;
            // update Charity post
            $post_id = wp_update_post( wp_slash($post_update) );
            update_field('address', $csv_row['ADDRESSS'] ,$post_id);
            update_field('phone', $csv_row['PHONE'] ,$post_id);


            
            if( $post_id ){
                // if post is updated
                fwrite($fp, '"'.$start_line.'"'.','.'"'.$csv_row['EMAIL'].'"'.','.'"'.$post_id.'"'.','.'"'.$password.'"'.','.'"'.$post_id.'"'.PHP_EOL);
            }else{
                // if post isn't updated
                fwrite($fp, '"'.$start_line.'"'.','.'"'.$csv_row['EMAIL'].'"'.','.'"'.$post_id.'"'.','.'"'.$password.'"'.','.'"post update failed"'.PHP_EOL);
            }
        }else{
            fwrite($fp, '"'.$start_line.'"'.','.'"'.$csv_row['EMAIL'].'"'.','.'"user failed"'.','.'""'.','.'""'.PHP_EOL);
        }


        fclose($fp);
    }
    wp_die();
}

function read_csv_update_row($start_line, $n_lines, $filepath ){   
    $heading_line = array(
        // "TITLE",
        // "DISH_TYPE_ID"
        
        // 'dish_id',
        // 'ndish_id',
        
        // 'ndish_id',
        // 'content',
        
        'ndish_id',
        'related_recipes',
        'instagram_hash',

    );
    $csv = array();
    $tmpName = $filepath;
    if(($handle = fopen($tmpName, 'r')) !== FALSE) {
        // necessary if a large csv file
        $row = 0;
        while(($data = fgetcsv($handle, 100000, ',')) !== FALSE) {
            // number of fields in the csv
            $col_count = count($data);
            $row++;
            if( ( $row >= $start_line ) && ( $row <= ($start_line + $n_lines -1) ) ){
                // get the values from the csv
                foreach ($heading_line as $key => $heading_line_value) {
                    $csv[$row][$heading_line_value] = $data[$key];
                }
                // echo '<pre>';
                // print_r( $csv[$row] );
                // echo '</pre>';
            }
        }
        fclose($handle);
    }
    return $csv;
}


add_action('wp_ajax_actionImportCSVRowDish', 'callbackImportCSVRowDish');
add_action('wp_ajax_nopriv_actionImportCSVRowDish', 'callbackImportCSVRowDish');
function callbackImportCSVRowDish(){

    $start_line = $_POST['count'];
    $filepath = $_POST['filepath'];
    $n_lines = $_POST['rows'];
    $csv = read_csv_update_row($start_line, $n_lines, $filepath);
    // "TITLE","DISH_TYPE_ID"
    // if( !the_slug_exists($csv[$start_line]['SLUG'], 'dish') ){
        $post_args = array(
            // 'comment_status' => 'closed',
            // 'ping_status'    => 'closed',
            'post_author'    => 1,
            // 'post_name'      => $csv[$start_line]['SLUG'],
            // 'post_name'      => ,
            'post_status'    => 'publish',
            'post_title'     => $csv[$start_line]['TITLE'],
            'post_type'      => 'ndish',
            // 'post_parent'    => $csv[$start_line]['PARENT_PAGE'],
            // 'tax_input'      => array( 'class' => $class_terms ),
        );
        $post_id = wp_insert_post( $post_args, true );
        

        if( $post_id  ){
            if(( $csv[$start_line]['DISH_TYPE_ID'] != 0 )&&( $csv[$start_line]['DISH_TYPE_ID'] != '0' )){
                wp_set_post_terms( $post_id, array( $csv[$start_line]['DISH_TYPE_ID'] ), 'dish_type');    
            }
            $log .= '<p>INS post('.$post_id.') - ' . $csv[$start_line]['TITLE'] . '</p>';
            $responce = array(
                'log' => $log,
            );
        }
    // }else{
    //     $log .= '<p>' . $csv[$start_line]['TITLE'] . ' - already exists</p>';
    //             $responce = array(
    //             'log' => $log,
    //     );
    // }
    wp_send_json($responce);
    wp_die();
}





add_action('wp_ajax_actionDishesOldToNew', 'callbackDishesOldToNew');
add_action('wp_ajax_nopriv_actionDishesOldToNew', 'callbackDishesOldToNew');
function callbackDishesOldToNew(){
    $log ='';

    $post_id   = (int)$_POST['post_id'];
    $slug = get_post_field( 'post_name', $post_id );

    $args = array(
        'post_type' => 'dishes',
        // 'post_type' => 'ndish',
        // 'posts_per_page' => 1,
        'name' => $slug,
    );

    $dishes_query = new WP_Query($args);
    $dishes = $dishes_query->posts;
    

    if( count($dishes) > 0){
        $dish = $dishes[0];
        $old_post_id = $dish->ID;
       
        // GET
        // ---------------------------------------------------
        // post_content
        $updated_post['ID'] = $post_id;
        $updated_post['post_content'] = $dish->post_content;
        //featured image
        $thumbnail_id = get_post_thumbnail_id( $old_post_id );
        //post_meta
        $old_post_meta_ = get_post_meta( $old_post_id );

        // SET
        // ---------------------------------------------------
        wp_update_post( wp_slash($updated_post) );
        set_post_thumbnail( $post_id, $thumbnail_id );

        foreach( $old_post_meta_ as $old_post_meta_key => $old_post_meta_value) {
            // if( strpos( $old_post_meta_key, '_edit') != false ){
            //     echo '<pre>';
            //     var_dump( $old_post_meta_key );
            //     var_dump( $old_post_meta_value );
            //     echo '</pre>';
                
                update_post_meta( $post_id, $old_post_meta_key, $old_post_meta_value);
            // }
        };


        $log = '<p>' .$post_id . ' | ' . $old_post_id. ' | "ndish"  updated from old "Dishes" | ' . $slug . ' | ' . $dish->post_name . '</p>';
        $responce = array(
            'log' => $log,
        );
        wp_send_json($responce);
        
    }
    

   
    wp_die();
}

add_action('wp_ajax_actionConvertRestaurants', 'callbackConvertRestaurants');
add_action('wp_ajax_nopriv_actionConvertRestaurants', 'callbackConvertRestaurants');
function callbackConvertRestaurants(){
    $post_id   = (int)$_POST['post_id'];
    $post = get_post($post_id);
/*
    // ----------------------------------------------------
    // post_author_override
    // ----------------------------------------------------
    $author_id=$post->post_author;
    // update_field('post_author_override', $author_id, $post_id);

    // ----------------------------------------------------
    // rf_season
    // ----------------------------------------------------
    $rf_seasons = get_field('rf_seasons', $post_id);
    $rf_seasons_ = explode(',', $rf_seasons);
    $nrf_seasons_ = array();
    foreach ($rf_seasons_ as $key => $value) {
        if( $value == 'on'){
            array_push($nrf_seasons_, 'rf_season_'.($key+1));
        }   
    }
    // $nrf_seasons_ = serialize($nrf_seasons_);


    // ----------------------------------------------------
    // rf_meals
    // ----------------------------------------------------
    $rf_meals = get_field('rf_meals', $post_id);
    $rf_meals_ = explode(',', $rf_meals);
    $nrf_meals_ = array();
    foreach ($rf_meals_ as $key => $value) {
        if( $value == 'on'){
            array_push($nrf_meals_, 'rf_meal_'.($key+1));
        }   
    }
    // $nrf_meals_ = serialize($nrf_meals_);

    // ----------------------------------------------------
    // rf_policies
    // ----------------------------------------------------
    $rf_policies = get_field('rf_policies', $post_id);
    $rf_policies_ = explode(',', $rf_policies);
    $nrf_policies_ = array();
    foreach ($rf_policies_ as $key => $value) {
        if( $value == 'on'){
            array_push($nrf_policies_, 'rf_other_'.($key+1));
        }   
    }
    // $nrf_policies_ = serialize($nrf_policies_);

    // ----------------------------------------------------
    // rf_rstrnt_images
    // ----------------------------------------------------

    $rf_rstrnt_images = get_field('rf_rstrnt_images', $post_id);
    $nrf_rstrnt_images_ = explode(',', $rf_rstrnt_images);

    // ----------------------------------------------------
    // rf_dishes
    // ----------------------------------------------------
    $rf_dishes = get_field('rf_dishes', $post_id);
    $rf_dishes_ = json_decode($rf_dishes);
    $nrf_dishes_ = array();
    foreach ($rf_dishes_->dishes as $dish) {
        array_push($nrf_dishes_, (string)$dish->dish_id);
    }
    $nrf_dishes = serialize($nrf_dishes_);

    // rf_days_hours
    // ----------------------------------------------------
    // 
    // 
    $rf_days_hours_old = get_field('rf_days_hours', $post_id);
    $rf_days_hours_old_ = explode(';',$rf_days_hours_old);
    foreach ($rf_days_hours_old_ as $key => $rf_days_hours_old_value) {
        $value_day = null;
        $value_open = null;
        $value_close = null;

        $index = $key + 1;
        $rf_days_hours_old_value_ = explode(',', $rf_days_hours_old_value);
        if($rf_days_hours_old_value_[0] == 'on'){
            $value_day = 1;
        }else{
            $value_day = 0;
        }
        $value_open = $rf_days_hours_old_value_[1];
        $value_close = $rf_days_hours_old_value_[2];
        
        // echo '<pre>';
        // var_dump( $rf_days_hours_old_value_ );
        // var_dump( $value_day );
        // var_dump( $value_open );
        // var_dump( $value_close );
        // echo '</pre>';
        // wp_die();

        
        update_field('rf_day_'.$index,   $value_day,  $post_id);
        update_field('rf_open_'.$index,  $value_open, $post_id);
        update_field('rf_close_'.$index, $value_close, $post_id);
        
        
     }
  // ["rf_days_hours"]=>
  // array(1) {
  //   [0]=>
  //   string(100) "on,8 am,10:45 pm;on,8 am,10:45 pm;on,8 am,10:45 pm;on,8 am,2:30 am;on,8 am,12 am;on,8 am,10:30 pm;,,"
  // }

*/

    // ----------------------------------------------------
    // update fields
    // ----------------------------------------------------
    // update_field('nrf_seasons',         $nrf_seasons_,          $post_id);
    // update_field('nrf_meals',           $nrf_meals_,            $post_id);
    // update_field('nrf_policies',        $nrf_policies_,         $post_id);
    // update_field('nrf_rstrnt_images',   $nrf_rstrnt_images_,    $post_id);
    // update_field('nrf_dishes',          $nrf_dishes_,           $post_id);
    
    
    
    $place_info = get_field('rf_place_info', $post_id);
    update_field('rf_price_level', $place_info['price_level'], $post_id);
    // echo '<pre>';
    // var_dump( $place_info['price_level'] );
    // echo '</pre>';

    $log = $post_id . ' | "restaurant"  converted"';
    $responce = array(
        'log' => $log,
    );
    wp_send_json($responce);

    wp_die();
}


add_action('wp_ajax_actionImportCSVRowAddNdishToDish', 'callbackImportCSVRowAddNdishToDish');
add_action('wp_ajax_nopriv_actionImportCSVRowAddNdishToDish', 'callbackImportCSVRowAddNdishToDish');
function callbackImportCSVRowAddNdishToDish(){
    $i = 0;
    $start_line = $_POST['count'];
    $filepath = $_POST['filepath'];
    $n_lines = $_POST['rows'];
    $csv = read_csv_update_row($start_line, $n_lines, $filepath);

    foreach ($csv as $csv_row) {
        
        $fp = fopen(__DIR__.'/add-ndish-to-dish_report.csv', 'a');
        $post_id = (int)$csv_row['dish_id'];
        $post = get_post($post_id);
        

        if( (get_class($post_id) !== 'WP_Error') && ( $post_id != 0 ) && ($post->post_type == 'dish')  ){
            update_field('ndish', (int)$csv_row['ndish_id'] , $post_id);
            $message = '"'.$csv_row['dish_id'].'","updated"';
        }else{
            $message = '"'.$csv_row['dish_id'].'","failed"';
        }
        fwrite($fp, $message.PHP_EOL);


        fclose($fp);
    }
    $response =array(
        'log' => '<p>'.$message.'</p>',
    );
    wp_send_json($response);
    wp_die();
}

add_action('wp_ajax_actionImportDishesContent', 'callbackImportDishesContent');
add_action('wp_ajax_nopriv_actionImportDishesContent', 'callbackImportDishesContent');
function callbackImportDishesContent(){
    $i = 0;
    $start_line = $_POST['count'];
    $filepath = $_POST['filepath'];
    $n_lines = $_POST['rows'];
    $csv = read_csv_update_row($start_line, $n_lines, $filepath);

    foreach ($csv as $csv_row) {
        
        $fp = fopen(__DIR__.'/add-content-to-dishes_report.csv', 'a');
        $post_id = (int)$csv_row['ndish_id'];
        $post = get_post($post_id);
        

        if( (get_class($post_id) !== 'WP_Error') && ( $post_id != 0 ) && ($post->post_type == 'ndish')  ){
            // Создаем массив данных
            $my_post = array();
            $my_post['ID'] = $post_id;
            $my_post['post_content'] = $csv_row['content'];

            // Обновляем данные в БД
            wp_update_post( wp_slash($my_post) );

            $message = '"'.$csv_row['ndish_id'].'","updated"';
        }else{
            $message = '"'.$csv_row['ndish_id'].'","failed"';
        }
        fwrite($fp, $message.PHP_EOL);


        fclose($fp);
    }
    $response =array(
        'log' => '<p>'.$message.'</p>',
    );
    wp_send_json($response);
    wp_die();
}



add_action('wp_ajax_actionImportDishesContent2', 'callbackImportDishesContent2');
add_action('wp_ajax_nopriv_actionImportDishesContent2', 'callbackImportDishesContent2');
function callbackImportDishesContent2(){
    $i = 0;
    $start_line = $_POST['count'];
    $filepath = $_POST['filepath'];
    $n_lines = $_POST['rows'];
    $csv = read_csv_update_row($start_line, $n_lines, $filepath);


    // 'ndish_id',
    // 'related_recipes',
    // 'instagram_hash',

    foreach ($csv as $csv_row) {
        
        $fp = fopen(__DIR__.'/add-content-to-dishes_report2.csv', 'a');
        $post_id = (int)$csv_row['ndish_id'];
        $post = get_post($post_id);
        

        if( (get_class($post_id) !== 'WP_Error') && ( $post_id != 0 ) && ($post->post_type == 'ndish')  ){

            if( !is_null($csv_row['instagram_hash']) ){
                update_field('instagram_hashtag', $csv_row['instagram_hash'] , $post_id);    
            }
            if( !is_null($csv_row['related_recipes']) ){
                // if( is_numeric($csv_row['related_recipes']) ){
                //     $related_recipes = $csv_row['related_recipes'];
                // }else{
                //     $related_recipes = explode(';',$csv_row['related_recipes']);
                // }
                $related_recipes = explode(';',$csv_row['related_recipes']);
                // $related_recipes = serialize($related_recipes);
                // echo '<pre>';
                // print_r( $related_recipes );
                // echo '</pre>';
                
                update_field('related_recipes', $related_recipes, $post_id);    

            }

            
            // update_field('instagram_hash', $csv_row['instagram_hash'] ,'user_'.$post_id);

            $message = '"'.$csv_row['ndish_id'].'","updated"';
        }else{
            $message = '"'.$csv_row['ndish_id'].'","failed"';
        }
        fwrite($fp, $message.PHP_EOL);


        fclose($fp);
    }
    $response =array(
        'log' => '<p>'.$message.'</p>',
    );
    wp_send_json($response);
    wp_die();
}


add_action('wp_ajax_actionGetAllMedia', 'callbackGetAllMedia');
add_action('wp_ajax_nopriv_actionGetAllMedia', 'callbackGetAllMedia');
function callbackGetAllMedia(){
    $query_images_args = array(
        'post_type'      => 'attachment',
        'post_mime_type' => 'image',
        'post_status'    => 'inherit',
        'posts_per_page' => - 1,
    );

    $query_images = new WP_Query( $query_images_args );

    $images = array();
    foreach ( $query_images->posts as $image ) {
        // $images[] = wp_get_attachment_url( $image->ID );
        $images[] = $image->ID;
    }
    wp_send_json($images);
    
    wp_die();
}






function mycopy($s1,$s2) {
    $path = pathinfo($s2);
    if (!file_exists($path['dirname'])) {
        mkdir($path['dirname'], 0775, true);
    }   
    copy($s1,$s2);
    // if (!copy($s1,$s2)) {
    //     echo "copy failed \n";
    // }
}
function load_image($filename, $type) {
    if( ( $type == IMAGETYPE_JPEG ) || ($type == 2) ) {
        $image = imagecreatefromjpeg($filename);
    }
    elseif( ( $type == IMAGETYPE_PNG ) || ( $type == 3) ) {
        $image = imagecreatefrompng($filename);
    }
    elseif( ( $type == IMAGETYPE_GIF ) ||( $type == 1) ) {
        $image = imagecreatefromgif($filename);
    }
    return $image;
}
function resize_image($new_width, $new_height, $image, $width, $height) {
    $new_image = imagecreatetruecolor($new_width, $new_height);
    imagecopyresampled($new_image, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
    return $new_image;
}
function resize_image_to_width($new_width, $image, $width, $height) {
    $resize_ratio = $new_width / $width;
    $new_height = $height * $resize_ratio;
    return resize_image($new_width, $new_height, $image, $width, $height);
}
function save_image($new_image, $new_filename, $new_type='jpeg', $quality=80) {
    if(( $new_type == 'jpeg' ) || ( $new_type == 2 )) {
        imagejpeg($new_image, $new_filename, $quality);
     }
     elseif(( $new_type == 'png' )||( $new_type == 3 )) {
        imagepng($new_image, $new_filename);
     }
     elseif(( $new_type == 'gif' )||( $new_type == 1 )) {
        imagegif($new_image, $new_filename);
     }
}

add_action('wp_ajax_actionOptimizeMedia', 'callbackOptimizeMedia');
add_action('wp_ajax_nopriv_actionOptimizeMedia', 'callbackOptimizeMedia');
function callbackOptimizeMedia(){
    $WPIMG_MAX_WIDTH = 2560;
    $media_id = $_POST['media_id'];
    // $media_id = 3032982;
    $post_meta = get_post_meta($media_id);
    $upload_dir = wp_get_upload_dir();

    $upload_dir_path = $upload_dir['basedir'] . '/';
    $upload_dir_path_new = str_replace('/uploads/','/uploads_new/',$upload_dir_path);
    $filename = get_post_meta($media_id, '_wp_attached_file');
    $filename = $filename[0];
    $source = $upload_dir_path . $filename;
    $filename_new = str_replace('-scaled', '', $filename);
    $destination = $upload_dir_path_new . $filename_new;

    mycopy( $source, $destination );
    
    list($width, $height, $type) = getimagesize($destination);
    
    if( $width > $WPIMG_MAX_WIDTH ){
        // $old_image = load_image($destination, $type);
        
        // $image_width_fixed = resize_image_to_width($WPIMG_MAX_WIDTH, $old_image, $width, $height);
        // echo '<pre>';
        // var_dump( $upload_dir_path_new . 'a-'.basename($destination) );
        // var_dump( $type );
        // echo '</pre>';

        // // imagejpeg($new_image, $new_filename, $quality);
        // imagejpeg($image_width_fixed, $upload_dir_path_new . 'a-'.basename($destination), 75);
        // // save_image($image_width_fixed, $upload_dir_path_new . 'a-'.basename($destination), $type, 75);
        // 
        // // Grab the editor for the file and the size of the original image.
        // $filename = '/Users/tommcfarlin/Projects/acme/wp-content/uploads/2018/06/original-image.jpg';

        $filename = $destination;
        if (!file_exists($filename)) {
          return;
        }

        $editor = wp_get_image_editor( $filename, array() );

        if (!is_wp_error($editor)) {
            // if editor works         
        }else{
            require_once('ResizeImage.php');
            $resize = new ResizeImage($filename);
            $resize->resizeTo(2560, 2560, 'maxwidth');
            $resize->saveImage($filename);
            
            $editor = wp_get_image_editor( $filename, array() );
        }


        
        // Get the dimensions for the size of the current image.
        $dimensions = $editor->get_size();
        $resize_ratio = $WPIMG_MAX_WIDTH / $dimensions['width'];
        $new_height = $dimensions['height'] * $resize_ratio;

        $newWidth = $WPIMG_MAX_WIDTH;
        $newHeight = $new_height;

        // Resize the image.
        $result = $editor->resize($newWidth, $newHeight, true);

        // If there's no problem, save it; otherwise, print the problem.
        if (!is_wp_error($result)) {
          // $editor->save($editor->generate_filename());
          $editor->save($filename);

      } else {
           // Handle the problem however you deem necessary.
      }

        

    }


    $log =  $media_id . ' - copied';
    $responce = array(
        'media_id' => $media_id,
        'log' => $log,
    );

    wp_send_json($responce);

    wp_die();
}

add_action('wp_ajax_actionConvertStops', 'callbackConvertStops');
add_action('wp_ajax_nopriv_actionConvertStops', 'callbackConvertStops');
function callbackConvertStops(){
    $post_id = $_POST['post_id'];

    $post_type = get_post_type( $post_id );
    switch ($post_type) {
        case 'bests':
            $key = 'rf_best_data';
            break;
        case 'tours':
            $key = 'rf_tour_data';
            break;
        default:
            $key = '';
            break;
    }
    if( $key !== '' ){
        $stops_data = get_post_meta($post_id, $key )[0];

        

        if($stops_data){
            $stops_data_ = json_decode($stops_data);
            if( property_exists($stops_data_, 'tour_stops') ){
                $stops_data = $stops_data_->tour_stops;
                $stops_ = [];
                foreach ($stops_data as $item) {
                    $stops_[] = $item->review_id;
                }
                // echo '<pre>';
                // print_r( $stops_ );
                // echo '</pre>';

                update_field('nrf_stops',$stops_, $post_id);
                $responce = array(
                    'log' => 'post_id = ' . $post_id . ' 
                    converted',
                );
            }else{
                $responce = array(
                    'log' => 'post_id = ' . $post_id . ' no stops',
                );
            }
        }else{
            $responce = array(
                    'log' => 'post_id = ' . $post_id . ' data missed',
                );
        }
        
    }else{
        // echo 'failed';
    }
    

    wp_send_json($responce);

    wp_die();
}


