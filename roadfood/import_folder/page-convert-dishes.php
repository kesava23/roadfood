<?php
/**
* Template Name: Convert Dishes
*
* @package WordPress
*/
wp_head();
// $filepath = __DIR__ . '/add-ndish-to-dish.csv';
// $filepath = __DIR__ . '/dishes_content.csv';
$filepath = __DIR__ . '/ndish_newdata.csv';

?>

<form action="#">
	<button id="import">Add data to ndish</button>
	<button id="stop">Stop</button>
</form>
<div id="report"></div>
<script>
	var import_status = false;
	function import_csv_file(){
		var count_step = 1;
		jQuery.ajax({
		    method: 'POST',
		    // url: myajax.url,
		    url : '<?php echo admin_url( 'admin-ajax.php' );?>',
		    data: {
		        'action': 'actionImportCSV',
		        'filepath' : '<?php echo $filepath;?>',
		    }, 
		    success: function (response) {
		    	count_step_max = response;
		    	// count_step_max = 2;
		    	import_csv_row(2, count_step_max);
		    },
		    error: function (error) {
		        console.log('error= '+ error);
		    }
		});
	}

	function import_csv_row(row, count_step_max){
		rows_per = 1;
		jQuery.ajax({
		    method: 'POST',
		    // url: myajax.url,
		    url : '<?php echo admin_url( 'admin-ajax.php' );?>',
		    data: {
		        // 'action': 'actionImportCSVRow',
		        // 'action': 'actionImportCSVRowAddNdishToDish',
		        // 'action': 'actionImportDishesContent',
		        'action': 'actionImportDishesContent2',
		        'count' : row,
		        'rows' : rows_per,
		        'filepath' : '<?php echo $filepath;?>',
		    }, 
		    success: function (response) {
		    	row =  row + 1;
		    	jQuery('#report').prepend(response.log);
		    	// if( row <= 3) {
		    	if( (row <= count_step_max) && (import_status != false) ) {
		    		import_csv_row(row, count_step_max);
		    	}
		    	
		    },
		    error: function (error) {
		        console.log('error= '+ error);
		    }
		});
	}

	jQuery(document).ready(function(){
		jQuery('#import').click(function(e){
			e.preventDefault();
			e.stopPropagation();
			import_status = true;
			import_csv_file();
		})
		jQuery('#stop').click(function(e){
			e.preventDefault();
			e.stopPropagation();
			import_status = false;
		})
	})


</script>



<?php
wp_footer();