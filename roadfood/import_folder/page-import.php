<?php
/**
* Template Name: Import Page
*
* @package WordPress
*/
/*
<form action="<?php echo get_stylesheet_directory_uri()?>/import-categories.php" method="get">
<form action="#" method="get">
<form action="<?php echo $_SERVER["PHP_SELF"]; ?>" method="post" enctype="multipart/form-data">
*/



// opcache_reset();
require_once(ABSPATH . 'wp-load.php' );
require_once(ABSPATH . 'wp-admin/includes/taxonomy.php');
require_once(ABSPATH . 'wp-admin/includes/post.php');
wp_head();

?>
<form action="/import-page/" id="import" method="post" enctype="multipart/form-data">
	<p>
		<input type="submit" name="submit" value="Start import">
	</p>
</form>
<div id="report"></div>
<?php  

	// $filepath = __DIR__ . '/test.csv';
	// $filepath = __DIR__ . '/failed.csv';
	// $filepath = __DIR__ . '/fresh_import.csv';
	// $filepath = __DIR__ . '/import.csv';
	$filepath = __DIR__ . '/new_dishes.csv';
	?>
<style>
	p{
		padding: 0;
		margin: 0;
		font-size: 14px;
	}
</style>	
<script>
	function import_csv_file(){
		var count_step = 1;
		jQuery.ajax({
		    method: 'POST',
		    // url: myajax.url,
		    url : '<?php echo admin_url( 'admin-ajax.php' );?>',
		    data: {
		        'action': 'actionImportCSV',
		        'filepath' : '<?php echo $filepath;?>',
		    }, 
		    success: function (response) {
		    	count_step_max = response;
		    	// count_step_max = 2;
		    	import_csv_row(2, count_step_max);
		    },
		    error: function (error) {
		        console.log('error= '+ error);
		    }
		});
	}

	function import_csv_row(row, count_step_max){
		rows_per = 1;
		jQuery.ajax({
		    method: 'POST',
		    // url: myajax.url,
		    url : '<?php echo admin_url( 'admin-ajax.php' );?>',
		    data: {
		        // 'action': 'actionImportCSVRow',
		        'action': 'actionImportCSVRowDish',
		        'count' : row,
		        'rows' : rows_per,
		        'filepath' : '<?php echo $filepath;?>',
		    }, 
		    success: function (response) {
		    	row =  row + 1;
		    	jQuery('#report').prepend(response.log);
		    	// if( row <= 3) {
		    	if( row <= count_step_max) {
		    		import_csv_row(row, count_step_max);
		    	}
		    	
		    },
		    error: function (error) {
		        console.log('error= '+ error);
		    }
		});
	}

	jQuery(document).ready(function(){
		jQuery('#import').submit(function(e){
			e.preventDefault();
			e.stopPropagation();
			import_csv_file();
		})
	})
</script>
<?php

?>