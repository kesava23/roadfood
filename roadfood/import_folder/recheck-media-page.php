<?php
/**
* Template Name: Recheck Media
*
* @package WordPress
*/
wp_head();
?>

<form action="">
	<button id="start">Start</button>
	<button id="stop">Stop</button>
</form>
<div id="report"></div>
<style>
	p{
		font-size: 16px;
		padding: 0;
		margin: 0;
	}
</style>
<script>
	media_ids_ = [];
	var count_step_max;
	var import_status = false;
	var row = 11000;

	jQuery('#start').click( function(e){
		e.preventDefault();
		e.stopPropagation();
		import_status = true;
		get_all_media_ids();
	})
	jQuery('#stop').click( function(e){
		e.preventDefault();
		e.stopPropagation();
		import_status = false;
	
	})

	function get_all_media_ids(){
		jQuery.ajax({
		    method: 'POST',
		    // url: myajax.url,
		    url : '<?php echo admin_url( 'admin-ajax.php' );?>',
		    data: {
		        'action': 'actionGetAllMedia',
		    }, 
		    success: function (response) {
		    	media_ids_ = response;
		    	count_step_max = response.length;
		    	console.log( media_ids_ );
		    	optimize_media_by_id(media_ids_[row])
		    },
		    error: function (error) {
		        console.log('error= '+ error);
		    }
		});
	}

	function optimize_media_by_id(media_id){
		// if( import_status == false) return;
		jQuery.ajax({
		    method: 'POST',
		    // url: myajax.url,
		    url : '<?php echo admin_url( 'admin-ajax.php' );?>',
		    data: {
		        'action': 'actionOptimizeMedia',
		        'media_id' : media_id,
		    }, 
		    success: function (response) {
		    	
		    	log = '<p>'+row+'/'+ count_step_max+' - '+response.log+'</p>'
		    	jQuery('#report').prepend(log);
		    	row =  row + 1;
		    	// if( row < 2) {
		    	if( ( row <= count_step_max) && (import_status != false)  ){
		    		optimize_media_by_id(media_ids_[row]);
		    	}
		    	
		    },
		    error: function (error) {
		        console.log('error= '+ error);
		    }
		});
	}
</script>