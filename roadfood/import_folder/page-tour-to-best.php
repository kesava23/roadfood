<?php
/**
* Template Name: Tours to Bests
*
* @package WordPress
*/
wp_head();

$tour_slugs = [
	'a-day-in-the-low-country',
	'americas-byways-michigans-copper-country-trail',
	'arizona-border-eats-2',
	'banana-pudding-at-its-best',
	'best-of-western-new-york',
	'boston-hidden-gems',
	'breakfast-in-sedona',
	'buffalo-beef-on-weck',
	'buffalo-sweet-shops',
	'canadas-smoked-meat',
	'central-connecticuts-steamed-cheeseburger',
	'charlottesville-virginia-fried-chicken',
	'chesapeake-bay-crab-cakes',
	'chicago-italian-beef',
	'chicago-must-eats-humble-grand',
	'chicago-red-hots',
	'chicago-without-utensils',
	'chicagos-many-pizzas',
	'cincinnati-chili',
	'clam-chowders-of-the-northeast',
	'coffee-milk-love-in-rhode-island',
	'delectable-des-moines',
	'delicious-day-in-tulsa',
	'delta-tamales',
	'eastward-ho-from-seattle',
	'fall-river-massachusetts-hidden-gems',
	'floridas-forgotten-coast',
	'frito-pie-quest',
	'hash-tour-of-south-carolina',
	'heart-of-texas-barbecue-belt',
	'hidden-gems-las-vegas',
	'hidden-gems-savannah',
	'hidden-gems-seattle',
	'horseshoes-of-springfield-illinois',
	'hot-chile-day-in-southern-new-mexico',
	'hot-nashville',
	'hunting-huckleberries',
	'idaho-panhandle',
	'illinois-valley-chicken-dinner',
	'iowa-i-80',
	'iowa-loosemeats',
	'iowa-tenderloins',
	'iowas-big-buns',
	'kansas-city-barbecue-and-more',
	'los-angeles-open-all-night',
	'louisiana-cajun-country-boudin-sausage',
	'massachusetts-roast-beef',
	'michigans-upper-peninsula',
	'milwaukee-fish-fries',
	'mississippis-delicious-dives',
	'nashville-four-alarm-fare',
	'nebraskas-comfort-food-meat-pie',
	'new-haven-pizza',
	'new-orleans-po-boys',
	'new-york-upstate-michigans',
	'newarks-italian-hot-dogs',
	'north-from-santa-fe',
	'north-of-milwaukee',
	'oklahoma-hamburgers',
	'oklahoma-route-66',
	'orlando-soul',
	'pasties-of-the-u-p',
	'philadelphias-choice-cheese-steaks',
	'phoenix-shacks',
	'pies-across-arkansas',
	'pimento-cheeseburgers',
	'pittsburghs-strip',
	'ployes-aroostook-countys-daily-bread',
	'a-days-delights-in-san-diego',
	'atlanta-vegetable-orgy',
	'birmingham-alabamas-best',
	'burlingtons-best',
	'new-york-must-eats',
	'dc-in-a-day',
	'essential-memphis',
	'indianapolis-in-a-day',
	'jewish-montreal-in-a-day',
	'knoxville-spree',
	'old-forge-pizza',
	'the-best-of-south-tucson',
	'rhode-island-new-york-system',
	'rhode-island-snail-salad',
	'santa-fe-in-a-day',
	'skyline-drive-detours',
	'sopaipilla-quest',
	'south-carolina-barbecue',
	'st-augustine-minorcan-clam-chowder',
	'st-louis-essentials',
	'superior-slaws-of-north-carolina',
	'augusta-georgia-barbecue',
	'charleston-soul-food',
	'the-other-peach-state',
	'tucson-green-corn-tamales',
	'tucson-sonoran-hot-dogs',
	'tucsons-mexican-gems',
	'west-virginia-italian',
	'wisconsin-pie-primer',
	'yankee-indian-pudding',
	'i-70-across-kansas',
	'orlando-unique-eats',
	'charlotte-sampler-one-days-delights',
	'classic-creole-in-a-day',
	'tahoetomontana',
	'dallas-austin-hill-country',
	'phoenix-to-grand-canyon',
	'las-vegas-nv-to-palm-springs',
	'michaels-hometown-must-eats',
	'northeast-pizza-throwdown',
	'one-day-in-milwaukee-must-eats',
	'oregons-north-coast',
	'albuquerque-route-66',
	'road-trip-day-around-new-orleans',
	'americas-byways-oregons-pacific-coast-scenic-byway',
	'santa-barbara-wine-country'
];

foreach ($tour_slugs as $tour_slug) {
	$args = [
		'post_type' => 'tours',
		'posts_per_page' => -1,
		'name' => $tour_slug,
	];
	$tours = new WP_Query($args);
	$the_post = $tours->posts[0];

	
	

	$my_post = [];
	$my_post['ID'] = $the_post->ID;
	// $my_post['post_type'] = $the_post->post_type;
	$my_post['post_type'] = 'bests';
	
	wp_update_post( wp_slash($my_post) );
	
	echo '<pre>';
	var_dump( $my_post );
	echo '</pre>';

}


