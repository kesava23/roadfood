<?php

// Do not delete these lines
if (!empty($_SERVER['SCRIPT_FILENAME']) && 'comments.php' == basename($_SERVER['SCRIPT_FILENAME']))
	die ('Please do not load this page directly. Thanks!');

if ( post_password_required() ) {
	?> <p><?php _e('This post is password protected. Enter the password to view comments.', 'base'); ?></p> <?php
	return;
}
	
function theme_comment($comment, $args, $depth) {
	$GLOBALS['comment'] = $comment; ?>
	
	<li>
		<div <?php comment_class(); ?> id="comment-<?php comment_ID(); ?>">
			<?php echo get_avatar( $comment, 48 ); ?>
			<p class="meta"><?php comment_date('F d, Y'); ?> <?php _e('at', 'base'); ?> <?php comment_time('g:i a'); ?>, <?php comment_author_link(); ?> <?php _e('said:', 'base'); ?></p>
			<?php if ($comment->comment_approved == '0') : ?>
			<p><?php _e('Your comment is awaiting moderation.', 'base'); ?></p>
			<?php else: ?>
			<?php comment_text(); ?>
			<?php endif; ?>
			
			<?php
				comment_reply_link(array_merge( $args, array(
					'reply_text' => __('Reply', 'base'),
					'before' => '<p>',
					'after' => '</p>',
					'depth' => $depth,
					'max_depth' => $args['max_depth']
				))); ?>
		</div>
	<?php }
	
	function theme_comment_end() { ?>
		</li>
	<?php }
?>

<?php if ( have_comments() ) : ?>

<div class="section comments" id="comments">

	<h2><?php comments_number(__('No Responses', 'base'), __('One Response', 'base'), __('% Responses', 'base') );?> <?php _e('to', 'base'); ?> &#8220;<?php the_title(); ?>&#8221;</h2>

	<ol class="commentlist">
		<?php wp_list_comments(array(
			'callback' => 'theme_comment',
			'end-callback' => 'theme_comment_end'
			)); ?>
	</ol>

	<div class="navigation">
		<div class="next"><?php previous_comments_link(__('&laquo; Older Comments', 'base')) ?></div>
		<div class="prev"><?php next_comments_link(__('Newer Comments &raquo;', 'base')) ?></div>
	</div>

</div>

 <?php else : // this is displayed if there are no comments so far ?>

	<?php if ( comments_open() ) : ?>
		<!-- If comments are open, but there are no comments. -->

	 <?php else : // comments are closed ?>
		<!-- If comments are closed. -->
		<p><?php _e('Comments are closed.', 'base'); ?></p>

	<?php endif; ?>
	
<?php endif; ?>


<?php if ( comments_open() ) : ?>

<div class="section respond" id="respond">
	<form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform">
		<fieldset>
			<h2><?php comment_form_title( __('Leave a Reply', 'base'), __('Leave a Reply to %s', 'base') ); ?></h2>
	
			<div class="cancel-comment-reply"><?php cancel_comment_reply_link(); ?></div>
		
			<?php if ( get_option('comment_registration') && !is_user_logged_in() ) : ?>
			<p>You must be <a href="<?php echo wp_login_url( get_permalink() ); ?>"><?php _e('logged in', 'base'); ?></a> <?php _e('to post a comment.', 'base'); ?></p>
			<?php else : ?>
			
			<?php if ( is_user_logged_in() ) : ?>

			<p><?php _e('Logged in as', 'base'); ?> <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>. <a href="<?php echo wp_logout_url(get_permalink()); ?>" title="<?php _e('Log out of this account', 'base'); ?>"><?php _e('Log out &raquo;', 'base'); ?></a></p>
			
			<dl>

			<?php else : ?>
			
			<dl>
				<dt><label for="author"><?php _e('Name', 'base'); ?></label></dt>
				<dd><input type="text" name="author" id="author" value="<?php echo esc_attr($comment_author); ?>" /></dd>
				<dt><label for="email"><?php _e('E-Mail (will not be published)', 'base'); ?></label></dt>
				<dd><input type="text" name="email" id="email" value="<?php echo esc_attr($comment_author_email); ?>" /></dd>
				<dt><label for="url"><?php _e('Website', 'base'); ?></label></dt>
				<dd><input type="text" name="url" id="url" value="<?php echo esc_attr($comment_author_url); ?>" /></dd>
			<?php endif; ?>
				<dt><label for="comment"><?php _e('Comment', 'base'); ?></label></dt>
				<dd><textarea name="comment" id="comment" cols="50" rows="10"></textarea></dd>
				<dd><input name="submit" type="submit" id="submit" value="<?php _e('Submit Comment', 'base'); ?>" /></dd>
			</dl>
			
			<?php
				comment_id_fields();
				do_action('comment_form', $post->ID);
			?>
			
			<?php endif; ?>

		</fieldset>
	</form>
</div>

<?php endif; ?>