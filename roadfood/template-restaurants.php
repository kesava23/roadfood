<?php
/*
Template Name: Restaurants Template
*/
get_header(); ?>
<div class="main">
	<section class="section-breadcrumb">
		<div class="container">
			<?php
				if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('
					<div id="breadcrumbs">','</div>');
				}	
			?>
		</div>		
	</section>
	
	<?php if (have_posts()) : ?>
	<?php while (have_posts()) : the_post(); ?>
		<section class="section-heading">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-8  text-center">
						<h1 class="text-uppercase"><?php the_title(); ?></h1>

						<p><?php echo get_field('sub-heading'); ?></p>
					</div><!-- /.col-lg-8 -->
				</div><!-- /.row -->
			</div><!-- /.container -->
		</section>
		<section class="section-1 section-dishes section-dishes--with-top-border">
			<div class="container">
				<header class="section__head">
					<div class="header__head-aside">
						<h2><?php echo get_field('section1_heading'); ?></h2>

						<div class="section__tags">
							<span>Roadfood <br> Favorites</span>
						</div><!-- /.section__tags -->
					</div><!-- /.header__head-aside -->

					<div class="header__head-actions">
						<a href="#restaurant_filter" class="btn-scroll-element">Browse All Restaurants</a>
					</div><!-- /.header__head-actions -->
				</header><!-- /.section__head -->

				<div class="section__body">
					<div class="slider-restaurants js-slider-restaurants">
						<div class="slider__clip">
							<div class="slider__slides">
								<?php
								$posts = get_field('restaurants_card');	
								
								if( $posts ):								
									foreach( $posts as $post):									
									?>	
								
								<div class="slider__slide">
									<div class="box">
										<div class="box__inner">
											
										
										<a href="<?php the_permalink()?>" class="box__link" tabindex="0"></a>

											<div class="box__image bg-parent js-image-fit">
											
												<?php if(has_post_thumbnail()){ ?>
													<img src="<?php echo the_post_thumbnail_url('Medium'); ?>" alt="" class="bg-image">
												<?php }else{ ?>
													<img src="/wp-content/uploads/2020/11/no-image-icon.jpg" alt="" class="bg-image" style="border: 1px solid #ddd;">
												<?php } ?>	 
											
											</div><!-- /.box__image -->

											<div class="box__content" style="height: 75px;">
												<h5><?php the_title(); ?></h5>

												<p><?php echo get_field('restaurant_city'); ?>, <?php echo get_field('restaurant_state'); ?></p>
											</div><!-- /.box__content -->
										
										</div>
									</div><!-- /.slider__slide-inner -->
								</div><!-- /.slider__slide -->
								<?php endforeach;?>
								<?php endif; ?>
								<?php wp_reset_query(); ?>  
							</div><!-- /.slider__slides -->
						</div><!-- /.slider__clip -->
					</div><!-- /.slider-dishes js-slider-dishes -->
				</div><!-- /.section__body -->

				<div class="section__actions">
					<a href="#restaurant_filter" class="btn-scroll-element">Browse All Restaurants</a>
				</div><!-- /.section__actions -->
			</div>
		</section>
	
		<section class="section-base section-base--fullwidth">
			<div class="container">
				<header class="section__head">
					<div class="header__head-aside">
						<h2><?php echo get_field('section2_heading'); ?></h2>
					</div><!-- /.header__head-aside -->

					<div class="header__head-actions">
						<a href="<?php echo get_field('view_all_url'); ?>">View All</a>
					</div><!-- /.header__head-actions -->
				</header>

				<div class="section__body">
					<div class="boxes">
						<div class="row">
						<?php
							$posts = get_field('blogs_card');								
							if( $posts ):								
							foreach( $posts as $post):									
						?>									
							<div class="col-md-3 col-6">
								<div class="box">
									<div class="box__inner">
										<a href="<?php the_permalink()?>" class="box__link"></a>

										<div class="box__image bg-parent js-image-fit">
											<?php if(has_post_thumbnail()){ ?>
												<img src="<?php echo the_post_thumbnail_url('Medium'); ?>" alt="" class="bg-image">
											<?php }else{ ?>
												<img src="/wp-content/uploads/2020/11/no-image-icon.jpg" alt="" class="bg-image" style="border: 1px solid #ddd;">
											<?php } ?>
										</div><!-- /.box__image -->

										<div class="box__content">
											<h5><?php the_title(); ?></h5>

<!-- 											<p>SEATTLE, WA</p> -->
										</div><!-- /.box__content -->
									</div><!-- /.box__inner -->
								</div><!-- /.box -->
							</div><!-- /.col-md-3 -->
							<?php 
							endforeach;
							endif;
							?>
							<?php wp_reset_query(); ?>  
						</div><!-- /.row -->
					</div><!-- /.boxes -->
				</div><!-- /.section__body -->
			</div><!-- /.container -->
		</section>
	
		<?php if(get_field('advertisement_widget', 'options')):?>
		<section class="section-banner-mobile d-block d-lg-none">
			<?php the_field('advertisement_widget', 'options')?>							
		</section>
		<?php endif;?>
		
		<section id="restaurant_filter" class="section-layout-filter">
			<div class="container">
				<div class="row">
					<div class="col-lg-12">
						<div class="container__wrap">
							<div class="section__inner">
								<div class="section__filter">
									<p>Filter results by:</p>

									<ul>
										<li>
											<a href="javascript:;" class="roadfood_modal" data-filter="state">Region / State</a>
										</li>

										<li>
											<a href="javascript:;" class="roadfood_modal" data-filter="restaurant_type">Restaurant Type</a>
										</li>

										<li>
											<a href="javascript:;" class="roadfood_modal" data-filter="dish_type">Dish Type</a>
										</li>
									</ul>
								</div><!-- /.section__filter -->

								<div class="section__search">
									<div class="form-search-in">
										<form action="?" method="post">
											<div class="form__body">
												<div class="form__row">
													<label for="field-search-in" class="form__label sr-only">Search Restaurants</label>

													<div class="form__controls">
														<input type="saerch" class="field" name="field-search-in" id="field-search-in" value="" placeholder="Search Restaurants">
													</div><!-- /.form__controls -->
												</div><!-- /.form__row -->
											</div><!-- /.form__body -->

											<div class="form__actions">
												<input type="submit" value="Search" class="form__btn">
											</div><!-- /.form__actions -->
										</form>
									</div><!-- /.form-search-in -->
								</div><!-- /.section__search -->
							</div><!-- /.section__inner -->
						</div><!-- /.container__wrap -->
					</div>
				</div><!-- /.row -->
			</div><!-- /.container -->
		</section>
	
		<section class="section-base-layout section-base-layout--with-border">
			<div class="container">
				<div class="row">
					<div class="col-lg-8">
						<div class="section__content">
							<section class="section-sort">
								<div class="row align-items-center">
									<div class="col-md-6">
										<div class="section__count">
											<?php 
												$total_num_restaurants = wp_count_posts( 'restaurants' )->publish;
												
												$current_shown = $total_num_restaurants > 10 ? 10 : $total_num_restaurants;
											?>
											<p>Showing 1 - <span class="total-best-items"><?php echo $current_shown; ?></span> of <?php echo $total_num_restaurants; ?> results</p>
										</div><!-- /.section__count -->
									</div><!-- /.col-md-6 -->

									<div class="col-md-6">
										<div class="section__actions">
											<a href="#" class="link-find">
												<svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 8.59 11.86"><path d="M4.29,0A4.29,4.29,0,0,0,0,4.29c0,3.07,4.29,7.57,4.29,7.57s4.3-4.5,4.3-7.57A4.3,4.3,0,0,0,4.29,0Zm0,5.72A1.43,1.43,0,1,1,5.72,4.29,1.43,1.43,0,0,1,4.29,5.72Z" style="fill:#0b2a4a"></path></svg>

												Restaurants Near Me
											</a>
										</div><!-- /.section__actions -->
									</div><!-- /.col-md-6 -->
								</div><!-- /.row -->
							</section><!-- /.section-sort -->

							<section class="section-best-items">
								<div class="items-best-list">									
									<?php
									 $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
									 $args = array(
										'post_type' => array('restaurants'),
										'posts_per_page' => 10,
										'orderby' => 'date',
										'order'   => 'DESC',
										'post_status' => 'publish',
										'paged' => $paged,
										 
									);
									$query = new WP_Query( $args );	
									?>
									<?php if ($query->have_posts()) : ?>
									<?php while ($query->have_posts()) : $query->the_post(); 
									
									?>
									<div class="items-best">
										<div class="item-best">
											<div class="item__best-inner">
												<div class="item__best-image bg-parent js-image-fit">
													<?php if(has_post_thumbnail()){ ?>
														<img src="<?php echo the_post_thumbnail_url('Medium'); ?>" alt="" class="bg-image">
													<?php }else{ ?>
														<img src="/wp-content/uploads/2020/11/no-image-icon.jpg" alt="" class="bg-image" style="border: 1px solid #ddd;">
													<?php } ?>	
												</div><!-- /.item__best-image -->

												<div class="item__best-content">
													<h4>
														<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
													</h4>

													<h6>
														<a href="<?php the_permalink(); ?>"><?php echo get_field('restaurant_city'); ?>, <?php echo get_field('restaurant_state'); ?></a>
													</h6>
												</div><!-- /.item__best-content -->
											</div><!-- /.item__best-inner -->

											<div class="item__best-rating">
												<ul>
													<?php $rating = get_field('rf_rstrnt_rating');
														$num_star = 0;
														if($rating == 'legendary'){
															$num_star = 5;
														}else if($rating == 'best'){
															$num_star = 4;
														}else if($rating == 'excellent'){
															$num_star = 3;
														}else if($rating == 'good'){
															$num_star = 2;
														}else if($rating == 'ok'){
															$num_star = 1;
														}else{
															$num_star = 0;
														}

														for($x = 1; $x <= 5; $x++){
															if($x <= $num_star){
																echo '<li><img src="/wp-content/themes/roadfood/assets/images/star-orange.svg" alt=""></li>';	
															}else{
																echo '<li><img src="/wp-content/themes/roadfood/assets/images/star-gray.svg" alt=""></li>';	
															}

														}
													?>												

												</ul>
											</div><!-- /.item__best-rating -->
										</div><!-- /.item-best -->

									</div><!-- /.items-best -->

									<?php endwhile; ?>	
									<?php endif; ?>
									<?php wp_reset_postdata(); ?> 
								</div>
								<div class="section__actions">
									<?php if($total_num_restaurants > 10){ ?>
									<a href="javascript:;" class="btn-show-more roadfood-loadmore-restaurant">Show More</a>
									<?php } ?>
								</div><!-- /.section__actions -->
								
							</section><!-- /.section-best-items -->
						</div><!-- /.section__content -->
					</div>

					<?php if(get_field('advertisement_widget', 'options')):?>
						<div class="col-lg-4 d-none d-lg-block">
							<div class="section__sidebar">
								<ul class="widgets">
									<li class="widget widget--banner">
										<?php the_field('advertisement_widget', 'options')?>
									</li><!-- /.widget widget--banner -->
								</ul><!-- /.widgets -->
							</div><!-- /.section__sidebar -->
						</div><!-- /.col-md-4 -->
					<?php endif;?>
					
				</div><!-- /.row -->
			</div><!-- /.container -->
		</section>
		
		<section class="section-explore">
			<div class="container">
				<div class="section__head">
					<h2>Explore</h2>
				</div><!-- /.section__head -->

				<div class="section__body">
					<div class="tabs js-tabs">
						<div class="tabs__head">
							<h5>Browse by:</h5>

							<nav class="tabs__nav">
								<ul>
									<li>
										<a href="#popular-cities">Popular Cities</a>
									</li>

									<li class="current">
										<a href="#state">State</a>
									</li>
								</ul>
							</nav><!-- /.tabs__nav -->
						</div><!-- /.tabs__head -->

						<div class="tabs__body">
							<div class="tab" id="popular-cities">
								<div class="row">
									<div class="col-lg-2 col-md-4 col-6">
										<ul>
											<?php 
												$restaurant_states = array();
												$restaurant_cities = array();
												$args = array(
													'post_type' => array('restaurants'),
													'posts_per_page' => -1,													
													'post_status' => 'publish'													
												);
												$query = new WP_Query( $args );
												$count = 0;
												$max_cities = 0;
											
												if ($query->have_posts()) : 
												while ($query->have_posts()) : $query->the_post(); 
													$city = get_field('restaurant_city');
													$state = get_field('restaurant_state');
											
													//show unique cities
													if (!in_array($city, $restaurant_cities)) {	
														//if 60 cities break the loop
														if($max_cities != 60){
															$max_cities++;
															$count++;
															echo '<li><a href="#">'. $city .'</a></li>';
															//array initialize
															$restaurant_cities[] = $city;
														}
														
													}
													//show unique states
													if (!in_array($state, $restaurant_states)) {												
														//array initialize
														$restaurant_states[] = $state;
													}
													
													//break to another column
													if($count == 10){
														$count = 0;
														echo '</ul></div><div class="col-lg-2 col-md-4 col-6"><ul>';
													}
	
												endwhile;
												endif;
												wp_reset_postdata(); 
											?>
											
										</ul>
									</div><!-- /.col-lg-2 col-md-4 -->

								</div><!-- /.row -->
							</div><!-- /.tab -->

							<div class="tab current" id="state">
								<div class="row">
									<div class="col-lg-2 col-md-4 col-6">
										<ul>
											<?php 
												$count = 0;
												$max_states = 0;
												foreach($restaurant_states as $state){
													echo '<li><a href="#">'. $state .'</a></li>';
													$count++;
													$max_states++;
													if($count == 10){
														$count = 0;
														echo '</ul></div><div class="col-lg-2 col-md-4 col-6"><ul>';
													}
													//if 60 cities break the loop
													if($max_states == 60){
														break;
													}
												}
											?>
											
										</ul>
									</div><!-- /.col-lg-2 col-md-4 -->

								</div><!-- /.row -->
							</div><!-- /.tab -->
						</div><!-- /.tabs__body -->
					</div><!-- /.tabs js-tabs -->
				</div><!-- /.section__body -->
			</div><!-- /.container -->
		</section>
	<?php endwhile; ?>
	<?php endif; ?>
</div>
<!-- modal -->
<div class="modal fade" id="filter-modal" tabindex="-1" role="dialog" aria-labelledby="filterModalLabel" aria-hidden="true">
  <div class="modal-dialog-centered" role="document" style="max-width: 950px;margin: 0 auto;">
    <div class="modal-content">
      <div class="modal-header">
       
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		  <span aria-hidden="true">×</span>
		  </button>
      </div>
      <div class="modal-body">		  
          <div class="filter-selection row">
			  
			  
		  </div>
      </div>
      
    </div>
  </div>
</div>
	
<?php get_footer(); ?>