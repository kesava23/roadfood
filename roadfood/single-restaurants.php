<?php get_header(); ?>

<div class="main">
	<section class="section-breadcrumb">
		<div class="container">
			<?php
				if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('
					<div id="breadcrumbs">','</div>');
				}	
			?>
		</div>		
	</section>
 <?php if (have_posts()) : ?>	
	<?php while (have_posts()) : the_post(); ?>
	  <?php 
	//custom page functions
	//set for open now time
	function get_opening_hours(){
		for($x = 1; $x < 7; $x++){
			if(get_field('rf_day_'. $x)){
				return get_field('rf_open_'. $x). ' - '. get_field('rf_close_'. $x);
				break;
			}
		}
	}
	//set for all week days
	function day_opening_hours($day = 'Monday'){
		switch ($day) {
		  case 'Monday':
			if(get_field('rf_day_1')){
				return get_field('rf_open_1'). ' - '. get_field('rf_close_1');
			}else{
				return 'Closed';
			}
			break;
		  case 'Tuesday':
			if(get_field('rf_day_2')){
				return get_field('rf_open_2'). ' - '. get_field('rf_close_2');
			}else{
				return 'Closed';
			}
			break;
		  case 'Wednesday':
			if(get_field('rf_day_3')){
				return get_field('rf_open_3'). ' - '. get_field('rf_close_3');
			}else{
				return 'Closed';
			}
			break;
		  case 'Thursday':
			if(get_field('rf_day_4')){
				return get_field('rf_open_4'). ' - '. get_field('rf_close_4');
			}else{
				return 'Closed';
			}
			break;
		  case 'Friday':
			if(get_field('rf_day_5')){
				return get_field('rf_open_5'). ' - '. get_field('rf_close_5');
			}else{
				return 'Closed';
			}
			break;
		 case 'Saturday':
			if(get_field('rf_day_6')){
				return get_field('rf_open_6'). ' - '. get_field('rf_close_6');
			}else{
				return 'Closed';
			}
			break;
		 case 'Sunday':
			if(get_field('rf_day_7')){
				return get_field('rf_open_7'). ' - '. get_field('rf_close_7');
			}else{
				return 'Closed';
			}
			break;
		  default:
			return 'undefined';
			break;
		}
	}
  ?>
	<div class="post" id="post-<?php the_ID(); ?>">
	<section class="section-single-restaurant" >
		<div class="container">
			<div class="section__head">
				<div class="section__tag">
					<?php $rating = get_field('rf_rstrnt_rating');
						if($rating == 'legendary'){
							echo '<p><strong>Legendary</strong> <span>|</span> Worth driving from anywhere</p>';
						}else if($rating == 'best'){
							echo '<p><strong>Memorable</strong> <span>|</span> One of the Best</p>';
						}else if($rating == 'excellent'){
							echo '<p><strong>Excellent</strong> <span>|</span> Worth a Detour</p>';
						}else if($rating == 'good'){
							echo '<p><strong>Good</strong> <span>|</span> Worth a Return</p>';
						}else if($rating == 'ok'){
							echo '<p><strong>Fail</strong> <span>|</span> Won\'t Eat Again</p>';
						}else{
							echo '<p>Rating is not added.</p>';
						}
					?>
					
				</div>	
				<div class="section__head-inner">
					<div class="section__titles">
						
						<?php 
							$terms = get_field('dish_type');

							if( $terms ): ?>
								<ul class="dish-type-taxonomy">
								<?php foreach( $terms as $term ): ?>
									<li>
									<a href="<?php echo esc_url( get_term_link( $term ) ); ?>"><h5><?php echo get_term( $term )->name; ?></h5></a>
									</li>
								<?php endforeach; ?>
								</ul>
							<?php endif; ?>
						<h1><?php the_title(); ?></h1>
					</div><!-- /.section__titles -->

					<div class="section__rating">
						<ul>
							<?php $rating = get_field('rf_rstrnt_rating');
							$num_star = 0;
							if($rating == 'legendary'){
								$num_star = 5;
							}else if($rating == 'best'){
								$num_star = 4;
							}else if($rating == 'excellent'){
								$num_star = 3;
							}else if($rating == 'good'){
								$num_star = 2;
							}else if($rating == 'ok'){
								$num_star = 1;
							}else{
								$num_star = 0;
							}

							for($x = 1; $x <= 5; $x++){
								if($x <= $num_star){
									echo '<li><img src="/wp-content/themes/roadfood/assets/images/star-orange.svg" alt=""></li>';	
								}else{
									echo '<li><img src="/wp-content/themes/roadfood/assets/images/star-gray.svg" alt=""></li>';	
								}

							}
						?>	
							
						</ul>
					</div><!-- /.section__rating -->
				</div>
				<div class="instagram-smashballoon">
					<?php echo do_shortcode(get_field('instagram_shortcode')); ?>
				</div>
			</div>		
		</div><!-- /.container -->
	</section>
	<?php if(get_field('advertisement_widget', 'options')):?>
	<section class="section-banner-mobile d-block d-lg-none">
		<?php the_field('advertisement_widget', 'options')?>							
	</section>
	<?php endif;?>
	
	<section class="section-base-layout">
		<div class="container">
			<div class="row">
				<div class="col-lg-8">
					<div class="section__content">
						<section class="section-plain-text">
							<div class="section__save-btn">
								<a href="#" class="btn-save">
									<svg width="15px" height="13px" viewBox="0 0 15 13" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="Asset-1" transform="translate(-1.000000, -1.000000)" fill="#EC6727" fill-rule="nonzero"><path d="M15.87,6 C16.1687684,4.8646284 15.9536625,3.65532667 15.2816971,2.69262544 C14.6097316,1.7299242 13.548761,1.11103607 12.38,1 C10.9679489,0.89124172 9.60702981,1.55258722 8.82,2.73 C8.74707701,2.83467338 8.62757068,2.89706503 8.5,2.89706503 C8.37242932,2.89706503 8.25292299,2.83467338 8.18,2.73 C7.35467241,1.46676173 5.87468328,0.79297721 4.38,1 C3.25128047,1.16872603 2.24838177,1.81155257 1.62372459,2.76668588 C0.9990674,3.72181919 0.812143511,4.89829325 1.11,6 C1.23815815,6.54993152 1.51891931,7.05252866 1.92,7.45 L8.22,13.77 C8.29345511,13.8457611 8.3944756,13.8885226 8.5,13.8885226 C8.6055244,13.8885226 8.70654489,13.8457611 8.78,13.77 L15.07,7.45 C15.4719358,7.05469957 15.7499395,6.55081786 15.87,6 Z" id="Path"></path></g></g>
									</svg>

									<span>Save</span>
								</a>
							</div><!-- /.section__save-btn -->

							<div class="section__entry entry-max-hidden ">
								<?php the_content(); ?>
							</div><!-- /.section__entry -->

							<div class="section__actions">
								<a href="#" class="read-more continue-reading">Continue Reading</a>
							</div><!-- /.section__actions -->
						</section>
						
						<section class="section-directions-hours">
							<div class="row">
								<div class="col-xl-6 col-lg-12 col-md-6">
									<h3>Directions &amp; Hours</h3>

									<div class="info">
										<div class="info__row">
											<img src="/wp-content/themes/roadfood/assets/images/pin-gray.svg" alt="">

											<p><?php echo get_field('rf_address_entry'); ?> <a href="#"><?php echo get_field('restaurant_city') . ', ' . get_field('restaurant_state') . ' ' .get_field('rf_zip_code_entry'); ?> (Get Directions)</a></p>
										</div><!-- /.info__row -->

										<div class="info__row">
											<img src="/wp-content/themes/roadfood/assets/images/phone.svg" alt="">

											<p><a href="tel:<?php echo get_field('rf_phone_entry'); ?>"><?php echo get_field('rf_phone_entry'); ?></a></p>
										</div><!-- /.info__row -->

										<div class="info__row info__row--center">
											<img src="/wp-content/themes/roadfood/assets/images/clock.svg" alt="">

											<span class="info__label info__label--green">Open Now</span>

											<span class="info__hours js-info-hours"><?php echo get_opening_hours(); ?></span>

											<ul class="info__hours-list">
												<li><span>Sunday</span><span><?php echo day_opening_hours('Sunday'); ?></span></li>
												<li><span>Monday</span><span><?php echo day_opening_hours('Monday'); ?></span></li>
												<li><span>Tuesday</span><span><?php echo day_opening_hours('Tuesday'); ?></span></li>
												<li><span>Wednesday</span><span><?php echo day_opening_hours('Wednesday'); ?></span></li>
												<li><span>Thursday</span><span><?php echo day_opening_hours('Thursday'); ?></span></li>
												<li><span>Friday</span><span><?php echo day_opening_hours('Friday'); ?></span></li>
												<li><span>Saturday</span><span><?php echo day_opening_hours('Saturday'); ?></span></li>																							
											</ul><!-- /.info__hours-list -->
										</div><!-- /.info__row -->
									</div><!-- /.info -->
								</div><!-- /.col-md-6 -->

								<div class="col-xl-6 col-lg-12 col-md-6">
									<div class="section__map">
										<?php 
										$location = get_field('rf_google_map_entry');
										if( $location ): ?>
										<div id="googlemap" class="googlemap-item" data-lng="<?php echo esc_attr($location['lat']); ?>" data-lat="<?php echo esc_attr($location['lng']); ?>" data-zoom="16" style="position: relative; overflow: hidden;"></div>
										<?php endif; ?>											
									</div><!-- /.section__map -->
								</div><!-- /.col-md-6 -->
							</div><!-- /.row -->
						</section>
						
						<section class="section-information">
							<h3>Information</h3>
							<div class="section__table">
								<table>
									<tbody>
									<tr>
										<td>Featured</td>
										<td><?php echo get_field('rf_featured') ? 'Yes' : 'No'; ?></td>
									</tr>
									<tr>
										<td>Highlight</td>
										<td><?php echo get_field('rf_highlight') ? 'Yes' : 'No'; ?></td>
									</tr>
									<tr>
										<td>Price</td>
										<td><?php echo get_field('rf_price_level'); ?></td>
									</tr>
									
									<?php if(is_array(get_field('rf_seasons'))): ?>	
									<tr>
										<td>Seasons</td>
										<td>
										<?php 
											$seasons = array(
												'rf_season_1' => 'Summer', 
												'rf_season_2' => 'Fall', 
												'rf_season_3' => 'Winter',
												'rf_season_4' => 'Spring');
											$seasons_arr = array('rf_season_1', 'rf_season_2', 'rf_season_3', 'rf_season_4');
											$count = 0;
											$arraysAreEqual = ($seasons_arr == get_field('rf_seasons')); 
											foreach ($seasons as $season  => $value) {
												if(in_array($season, get_field('rf_seasons') )): 
													$count++;
													if($count > 1):
														echo ', ';
													endif;
													//check if all seasons selected then display All
													if($arraysAreEqual):
														echo 'All';
														break;
													else:
														echo $value;
													endif;
												endif;												
											}
										wp_reset_postdata();
										?>
										</td>
									</tr>
									<?php endif; ?>
										
									<?php if(is_array(get_field('rf_meals'))): ?>
									<tr>
										<td>Meals Served</td>
										<td>
										<?php 
											$meals = array(
												'rf_meal_1' => 'Breakfast', 
												'rf_meal_2' => 'Lunch', 
												'rf_meal_3' => 'Dinner',
												'rf_meal_4' => 'Dessert',
												'rf_meal_5' => 'Late Night');
											$meals_arr = array('rf_meal_1', 'rf_meal_2', 'rf_meal_3', 'rf_meal_4', 'rf_meal_5');
											$count = 0;
											$arraysAreEqual = ($meals_arr == get_field('rf_meals')); 
											foreach ($meals as $meal  => $value) {
												if(in_array($meal, get_field('rf_meals') )): 
													$count++;
													if($count > 1):
														echo ', ';
													endif;
													//check if all meals selected then display All
													if($arraysAreEqual):
														echo 'All';
														break;
													else:
														echo $value;
													endif;
												endif;												
											}
										
										?>
										</td>
									</tr>
									<?php endif; ?>
										
									<?php if(is_array(get_field('rf_other'))): ?>		
									<tr>										
										<td>Credit Cards Accepted</td>
										<td><?php echo in_array('rf_other_1', get_field('rf_other') ) ? 'Yes' : 'No' ?></td>
									</tr>
									<tr>
										<td>Alcohol Served</td>
										<td><?php echo in_array('rf_other_2', get_field('rf_other') ) ? 'Yes' : 'No' ?></td>
									</tr>
									<tr>
										<td>Outdoor Seating</td>
										<td><?php echo in_array('rf_other_3', get_field('rf_other') ) ? 'Yes' : 'No' ?></td>
									</tr>
									<?php endif; ?>
									
									<?php if(!empty(get_field('rf_website_entry'))): ?>		
									<tr>
										<td>Website</td>
										<td>
											<a href="<?php echo esc_url(get_field('rf_website_entry')); ?>">
												<?php echo esc_url(get_field('rf_website_entry')); ?></a>
										</td>
									</tr>
									<?php endif; ?>	
								</tbody></table>
							</div><!-- /.table -->
						</section>
						
						<section class="section-base section-base-no-actions what-to-eat">
							<div class="section__head">
								<h3>What To Eat</h3>
							</div><!-- /.section__head -->

							<div class="section__body">
								<div class="boxes-large">
									<div class="row">
										<?php
										
										$query = new WP_Query(array(											
											"post_type" => "dish",											
											'orderby' => 'date',
											'order'   => 'DESC',
											'post_status' => 'publish',											
											"meta_query" => array(
												array(
												'key' => 'rf_rstrnt_review_id',
												'value' => get_the_ID()
												)
											)
										));							
									   if ($query->have_posts()) : 
										
										 while ($query->have_posts()) : $query->the_post();
										
											$images = get_field('dish_image');   ?>
											<div class="col-md-6">
												<div class="box-horizontal">
													<?php 
														if(is_array($images)){
															$g_image = $images[0];
														}else{
															$g_image = $images;
														}
																										
													?>
													<div class="box__image bg-parent js-image-fit">
														<a href="<?php echo esc_url(wp_get_attachment_url($g_image)); ?>" class="box__image-link" data-lightbox="what_to_eat" data-title="<?php the_title(); ?>"></a>
														<?php if($images){ ?>
															<img src="<?php echo esc_url(wp_get_attachment_image_src($g_image, 'thumbnail')[0]); ?>" alt="" class="bg-image">
														<?php }else{ ?>
															<img src="/wp-content/uploads/2020/11/no-image-icon.jpg" alt="" class="bg-image" style="border: 1px solid #ddd;">
														<?php } ?>	
													</div><!-- /.box__image -->

													<div class="box__content">
														<h5><?php the_title(); ?></h5>
														<p></p>
														<?php $featured_post = get_field('ndish'); ?>
														<h6><a href="<?php echo get_the_permalink($featured_post); ?>">DISH</a></h6>
													</div><!-- /.box__content -->
												</div><!-- /.box-horizontal -->
											</div><!-- /.col-md-6 -->
										<?php endwhile;
										else: ?>
											<script>
												(function($){
													$('.what-to-eat.section-base').hide();
												})(jQuery)
												
											</script>
										<?php endif; 
										wp_reset_query(); ?>  
										
									</div><!-- /.row -->
								</div><!-- /.boxes-large -->
							</div><!-- /.section__body -->
						</section>
						
						<section class="section-base restaurant-recipes">
							<header class="section__head">
								<h2><?php the_title(); ?> Recipes</h2>

								<p><?php echo get_field('recipes_short_description'); ?></p>
							
								
							</header><!-- /.section__head -->

							<div class="section__body">
								<div class="boxes-type-1">
									<div class="row">
										<?php
										//count number of recipe results										
										$query_all = new WP_Query(array(											
											"post_type" => "recipe",																			
											'post_status' => 'publish',											
											"meta_query" => array(
												array('key' => 'restaurant','value' => get_the_ID()))
										));	
   										$total_num_recipes = $query_all->found_posts;
										wp_reset_query();
										
										$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
									
										$query = new WP_Query(array(
											
											"post_type" => "recipe",
											'posts_per_page' => 8,
											'orderby' => 'date',
											'order'   => 'DESC',
											'post_status' => 'publish',
											'paged' => $paged,
											"meta_query" => array(
												array(
												'key' => 'restaurant',
												'value' => get_the_ID()
													)
											)
										));							
																		
										
										?>		
										<?php if ($query->have_posts()) : ?>
										<?php while ($query->have_posts()) : $query->the_post();?>
										<div class="col-md-3 col-6">
											<div class="box">
												<div class="box__inner">
													<a href="<?php the_permalink(); ?>" class="box__link"></a>
													<div class="box__image bg-parent js-image-fit">
													<?php if(has_post_thumbnail()){ ?>
														<img src="<?php echo the_post_thumbnail_url('thumbnail'); ?>" alt="" class="bg-image">
													<?php }else if(get_field('dish_image')){
														$images = get_field('dish_image'); 
														if(is_array($images)){
															$g_image = $images[0];
														}else{
															$g_image = $images;
														}
													?>
														<img src="<?php echo esc_url(wp_get_attachment_image_src($g_image, 'thumbnail')[0]); ?>" alt="" class="bg-image">
													<?php }else{ ?>
														<img src="/wp-content/uploads/2020/11/no-image-icon.jpg" alt="" class="bg-image" style="border: 1px solid #ddd;">
													<?php } ?>
													</div><!-- /.box__image -->
													<div class="box__content">
														<p><?php the_title(); ?></p>

													</div><!-- /.box__content -->
												</div><!-- /.box__inner -->
											</div><!-- /.box -->
										</div><!-- /.col-md-4 -->
										<?php endwhile;?>
										<?php else: ?>
											<script>
												(function($){
													$('.restaurant-recipes.section-base').hide();
												})(jQuery)
												
											</script>
										<?php endif; ?>
										<?php wp_reset_query(); ?> 					
										
									</div><!-- /.row -->
								</div><!-- /.boxes -->
							</div><!-- /.section__body -->
							<?php if($total_num_recipes > 8): ?>
							<div class="section__actions">
								<a href="#" class="btn btn--with-shape js-loadmore-recipes" data-id="<?php echo get_the_ID(); ?>">
									<span>More <?php the_title(); ?> Recipes</span>

									<svg width="27px" height="20px" viewBox="0 0 27 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="Asset-1" class="svg-color" fill="#FFFFFF" fill-rule="nonzero"><path d="M1.76,10.8 L14.76,13.8 L14.76,18.6 C14.7663086,18.9826391 14.9904405,19.3281223 15.3372831,19.4898403 C15.6841256,19.6515583 16.0928461,19.6011474 16.39,19.36 L26.2,10.54 C26.4186854,10.3500696 26.5442752,10.0746496 26.5442752,9.785 C26.5442752,9.4953504 26.4186854,9.21993042 26.2,9.03 L16.4,0.25 C16.1042499,0.00985886916 15.6977449,-0.0413773288 15.3516561,0.117865966 C15.0055674,0.27710926 14.7800248,0.619164918 14.77,1 L14.77,5.8 L1.77,8.8 C1.21771526,8.7972386 0.76776146,9.24271526 0.764987267,9.795 C0.762238613,10.3472847 1.20771526,10.7972386 1.76,10.8 Z" id="Path"></path></g></g></svg>
								</a>
							</div><!-- /.section__actions -->
							<?php endif; ?>
						</section>
					<?php if(get_field('rf_rstrnt_entry_images') || get_field('brightcove_video_id')): ?>
						<section class="section-gallery section-base">
							<div class="section__inner">
								<div class="section__head">
									<h2>Gallery</h2>
								</div><!-- /.section__head -->

								<div class="section__image">
									<a href="#" class="section__image-link"></a>
									<?php if(get_field('brightcove_video_id')): ?>
										<video-js data-video-id="<?php echo get_field('brightcove_video_id'); ?>"
										  data-account="1507807800001"
										  data-player="BkTGbgSq"
										  data-embed="default"
										  data-application-id
										  class="video-js"
										  controls></video-js>
										  <script src="https://players.brightcove.net/1507807800001/BkTGbgSq_default/index.min.js"></script>
									<?php else: ?>
										<img src="/wp-content/uploads/2020/12/no-video.jpg" alt="No video placeholder">
									<?php endif; ?>
									
									

								</div><!-- /.section__image -->

								<div class="section__aside">
									<div class="section__grid">
										<?php
										$gallery = get_field('rf_rstrnt_entry_images');
										$size = 'thumbnail';
										if( $gallery ):
										foreach( $gallery as $image_id ):
										?>
										<div class="section__grid-item">
											<a href="<?php echo wp_get_attachment_image_url( $image_id, 'full'); ?>" class="section__grid-item-bg bg-parent js-image-fit" data-lightbox="gallery" >				
												<img src="<?php echo wp_get_attachment_image_url( $image_id, $size ); ?>" alt="" class="bg-image">
											</a>
										</div><!-- /.section__grid-item -->
										<?php 
										endforeach;
										endif; ?>
										
									</div><!-- /.section__grid -->

									<div class="section__actions">
										<a href="#" class="btn btn--orange">View Gallery</a>

										<a href="#" class="btn btn--orange-border">Submit Your Own</a>
									</div><!-- /.section__actions -->
								</div><!-- /.section__aside -->
							</div><!-- /.section__inner -->
						</section>
					<?php endif; ?>
						
						<section class="section-base">
							<header class="section__head">
								<h2>Discuss</h2>

								<p>What do you think of <?php the_title(); ?>?</p>
							</header><!-- /.section__head -->

							<div class="section__body">
								<?php comments_template(); ?>
								
							</div><!-- /.section__body -->


							<div class="section__actions">
								<a href="javascript:;" class="btn-show-more js-show-more-discuss">Show More</a>
							</div><!-- /.section__actions -->
						</section><!-- /.section-base -->
						
						<section class="section-base">
							<header class="section__head">
								<h2>Nearby Restaurants</h2>
							</header><!-- /.section__head -->

							<div class="section__body">
								<div class="boxes">
									<div class="row row-small-margin">
										<?php
										 $paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
										 $args = array(
											'post_type' => array('restaurants'),
											'posts_per_page' => 6,
											'orderby' => 'date',
											'order'   => 'DESC',
											'post_status' => 'publish',
											'post__not_in' => array (get_the_ID()),
											'paged' => $paged
										);
										$query = new WP_Query( $args );	
										?>
										<?php if ($query->have_posts()) : ?>
										<?php while ($query->have_posts()) : $query->the_post(); 

										?>
										<div class="col-md-4 col-6 col-small-padding">
											<div class="box">
												<div class="box__inner">
													<a href="<?php the_permalink(); ?>" class="box__link"></a>
													<div class="box__image bg-parent js-image-fit">
														<?php if(!empty(get_the_post_thumbnail())){ ?>
															<img src="<?php echo the_post_thumbnail_url('Medium'); ?>" alt="" class="bg-image">
														<?php }else{ ?>
															<img src="/wp-content/uploads/2020/11/no-image-icon.jpg" alt="" class="bg-image" style="border: 1px solid #ddd;">
														<?php } ?>	
													</div><!-- /.box__image -->

													<div class="box__content">
														<h5><?php the_title(); ?></h5>
														<p><?php echo get_field('restaurant_city'); ?>, <?php echo get_field('restaurant_state'); ?></p>
													</div><!-- /.box__content -->
												</div><!-- /.box__inner -->
											</div><!-- /.box -->
										</div><!-- /.col-md-4 -->
										
										<?php 											
											endwhile;
										endif;
										?>
										<?php wp_reset_query(); ?>  
									</div><!-- /.row -->
								</div><!-- /.boxes -->
							</div><!-- /.section__body -->

							<div class="section__actions">
								<a href="/restaurants/" class="btn btn--with-shape">
									<span>More Restaurants</span>

									<svg width="27px" height="20px" viewBox="0 0 27 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"><g id="Page-1" stroke="none" stroke-width="1" fill="none" fill-rule="evenodd"><g id="Asset-1" class="svg-color" fill="#FFFFFF" fill-rule="nonzero"><path d="M1.76,10.8 L14.76,13.8 L14.76,18.6 C14.7663086,18.9826391 14.9904405,19.3281223 15.3372831,19.4898403 C15.6841256,19.6515583 16.0928461,19.6011474 16.39,19.36 L26.2,10.54 C26.4186854,10.3500696 26.5442752,10.0746496 26.5442752,9.785 C26.5442752,9.4953504 26.4186854,9.21993042 26.2,9.03 L16.4,0.25 C16.1042499,0.00985886916 15.6977449,-0.0413773288 15.3516561,0.117865966 C15.0055674,0.27710926 14.7800248,0.619164918 14.77,1 L14.77,5.8 L1.77,8.8 C1.21771526,8.7972386 0.76776146,9.24271526 0.764987267,9.795 C0.762238613,10.3472847 1.20771526,10.7972386 1.76,10.8 Z" id="Path"></path></g></g></svg>
								</a>
							</div><!-- /.section__actions -->
						</section>
						
						
					</div><!-- /.section__content -->
				</div><!-- /.col-md-8 -->

				<?php if(get_field('advertisement_widget', 'options')):?>
					<div class="col-lg-4 d-none d-lg-block">
						<div class="section__sidebar">
							<ul class="widgets">
								<li class="widget widget--banner">
									<?php the_field('advertisement_widget', 'options')?>
								</li><!-- /.widget widget--banner -->
							</ul><!-- /.widgets -->
						</div><!-- /.section__sidebar -->
					</div><!-- /.col-md-4 -->
				<?php endif;?>
				
			</div><!-- /.row -->
		</div><!-- /.container -->
	</section>
		
	
</div>
	
  <?php endwhile; ?>
	<div class="container" style="display:none;">
		<div class="navigation" >
			<div class="next"><?php previous_post_link(__('%link &raquo;', 'base')) ?></div>
			<div class="prev"><?php next_post_link(__('&laquo; %link', 'base')) ?></div>
		</div>
	</div>
	
<?php else : ?>
	<div class="post">
		<div class="title">
			<h1><?php _e('Not Found', 'base'); ?></h1>
		</div>
		<div class="content">
			<p><?php _e('Sorry, but you are looking for something that isn\'t here.', 'base'); ?></p>
		</div>
	</div>
		
<?php endif; ?>

	<section class="section-articles">
		<div class="container">
			<header class="section__head">
				<h2>Article’s &amp; Guides Tagged <?php the_title();  ?></h2>

				<a href="/articles-guides/" class="view-more">View All</a>
			</header><!-- /.section__head -->

			<div class="section__body">
				<div class="articles">
					<div class="row">
						<?php 
						$page_slug = get_post_field( 'post_name' );
						
						$paged = ( get_query_var( 'paged' ) ) ? get_query_var( 'paged' ) : 1;
						 $args = array(
							'post_type' => array('post'),
							'posts_per_page' => 6,
							'orderby' => 'date',
							'order'   => 'DESC',
							'post_status' => 'publish',
							'paged' => $paged,
							 'tag' => $page_slug
						);
						$query = new WP_Query( $args );								
						?>		
						<?php if ($query->have_posts()) : ?>
						<?php while ($query->have_posts()) : $query->the_post();?>
						<div class="col-md-4 col-6 ">
							<div class="article">
								<div class="article__inner">
									<div class="article__image bg-parent js-image-fit">
										<a href="<?php the_permalink(); ?>" class="article__image-link"></a>
										<?php if(!empty(get_the_post_thumbnail())){ ?>
											<img src="<?php echo the_post_thumbnail_url('Medium'); ?>" alt="" class="bg-image">
										<?php }else{ ?>
											<img src="/wp-content/uploads/2020/11/no-image-icon.jpg" alt="" class="bg-image" style="border: 1px solid #ddd;">
										<?php } ?>	
									</div><!-- /.article__image -->

									<div class="article__contnet">
<!-- 										<div class="article__meta">
											<p>LORASO SIN CASA</p>
										</div><!-- /.article__meta -->
 
										<div class="article__title">
											<a href="<?php the_permalink(); ?>"><?php the_title(); ?></a>
										</div><!-- /.article__title -->

										<div class="article__entry">
											<?php echo excerpt(30); ?>
										</div><!-- /.article__entry -->
									</div><!-- /.article__contnet -->
								</div><!-- /.article__inner -->
							</div><!-- /.article -->
						</div><!-- /.col-md-4 -->
						<?php endwhile; ?>	
						<?php else: ?>
							<script>
								(function($){
									$('.section-articles').hide();
								})(jQuery)

							</script>
						<?php endif; ?>
						<?php wp_reset_query(); ?>
				
					</div><!-- /.row -->
				</div><!-- /.articles -->
			</div><!-- /.section__body -->
		</div><!-- /.container -->
	</section>
	<?php  

	$post_id = get_the_ID();

	echo '<pre style="display: none">';
	var_dump( $author_id=$post->post_author );
	var_dump( get_the_author_meta( 'ID' ) );
	var_dump( get_post_meta( $post_id ) );
	echo '</pre>';

	?>


<?php //get_sidebar(); ?>
	
<script>
(function($){
	discuss = $('.discuss');
	$.each(discuss, function(key,value) {
		$(this)
		  .find('.discuss-item')
		  .slice(3)
		  .addClass('hidden-discuss');
		
	});
	//hide show more button if comments less than 4
	var numdiscuss = $('.discuss-item').length
	if(numdiscuss < 4){
	   $('.js-show-more-discuss').hide();
	 }
	
	$('.js-show-more-discuss').click(function(){		
	  $( ".hidden-discuss" ).slideToggle( "slow", function() {});
	  $(this).text('Show Less');
	});
})(jQuery)
</script>
<?php get_footer(); ?>
