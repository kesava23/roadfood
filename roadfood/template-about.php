<?php
/*
Template Name: About Template
*/
get_header(); ?>
<div class="main">
	<section class="section-breadcrumb">
		<div class="container">
			<?php
				if ( function_exists('yoast_breadcrumb') ) {
					yoast_breadcrumb('
					<div id="breadcrumbs">','</div>');
				}	
			?>
		</div>		
	</section>
	<?php if (have_posts()) : ?>
	<?php while (have_posts()) : the_post(); ?>
		<section class="section-heading">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-8  text-center">
						<h1 class="text-uppercase"><?php the_title(); ?></h1>

						<p><?php echo get_field('sub-heading'); ?></p>
					</div><!-- /.col-lg-8 -->
				</div><!-- /.row -->
			</div><!-- /.container -->
		</section>
		<section class="section-fullwidth-image">
			<?php the_post_thumbnail('full'); ?>
		</section>
		<section class="section-small-text">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-8">
						<?php the_content(); ?>
					</div>
				</div>
			</div>
		</section>
		<section class="section-our-team">
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-lg-8">
						<div class="row align-items-center">
							<div class="col-md-6">
								<h2><?php echo get_field('cta_sub-heading'); ?></h2>

								<p><?php echo get_field('cta_short_text'); ?></p>

								<a href="<?php echo get_field('cta_button_url'); ?>" class="btn"><?php echo get_field('cta_button_text'); ?></a>
							</div><!-- /.col-md-6 -->

							<div class="col-md-6">
							
								<?php $image = wp_get_attachment_image_src(get_field('cta_image'), 'full'); ?>
								<img src="<?php echo $image[0]; ?>" alt="<?php echo get_the_title(get_field('image_test')) ?>" />

							</div><!-- /.col-md-6 -->
						</div><!-- /.row -->
					</div><!-- /.col-lg-8 -->
				</div><!-- /.row -->
			</div><!-- /.container -->
		</section>
	<?php endwhile; ?>
	<?php endif; ?>
</div>
<?php get_footer(); ?>