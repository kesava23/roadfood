<?php get_header(); ?>

<div class="main">
	<div class="container">
		<div class="post pt-5 pb-4">
			<div class="title">
				<h1><?php _e('Not Found', 'base'); ?></h1>
			</div>
			<div class="content">
				<p><?php _e('Sorry, but you are looking for something that isn\'t here.', 'base'); ?></p>
			</div>
		</div>
	</div>
</div>

<?php //get_sidebar(); ?>

<?php get_footer(); ?>
