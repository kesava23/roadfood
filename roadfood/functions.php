<?php
// error_reporting(E_ALL);
// ini_set('display_errors', 1);


include( get_stylesheet_directory().'/includes/functions/cpt.php' );
include( get_stylesheet_directory().'/includes/functions/acf.php' );
include( get_stylesheet_directory().'/includes/functions/admin.php' );
include( get_stylesheet_directory().'/includes/functions/functions.php' );
include( get_stylesheet_directory().'/includes/functions/images.php' );
include( get_stylesheet_directory().'/includes/functions/import.php' );
include( get_stylesheet_directory().'/includes/functions/nav.php' );
include( get_stylesheet_directory().'/includes/functions/permalinks.php' );
include( get_stylesheet_directory().'/import_folder/functions-import.php' );



// function import_media_by_id( $media_id ){
// 	// check if image is already imported
// 	if(get_option('media_imported_'.$media_id) !== false) return;

	
// 	$stage_data_url = 'https://stage.roadfood.com/get_img.php?img_id='.$media_id;
// 	$image_ = json_decode(file_get_contents( $stage_data_url ));
// 	$image_url = $image_->src;

// 	$upload_dir = wp_upload_dir();
// 	$image_data = file_get_contents( $image_url );
	
// 	if( $image_data ){
// 		$filename = basename( $image_url );
// 		if ( wp_mkdir_p( $upload_dir['path'] ) ) {
// 			$file = $upload_dir['path'] . '/' . $filename;
// 		}
// 		else {
// 			$file = $upload_dir['basedir'] . '/' . $filename;
// 		}
// 		file_put_contents( $file, $image_data );
// 		$wp_filetype = wp_check_filetype( $filename, null );
// 		$attachment = array(
// 			'post_mime_type' => $wp_filetype['type'],
// 			'post_title' => $image_->title,
// 			'post_content' => $image_->description,
// 			'post_status' => 'inherit',
// 			'post_excerpt' => $image_->caption,
// 		);
// 		$attach_id = wp_insert_attachment( $attachment, $file );
		
// 		if( $attach_id ){
// 			$attach_data = wp_generate_attachment_metadata( $attach_id, $file );
// 			wp_update_attachment_metadata( $attach_id, $attach_data );

// 			update_post_meta( $attach_id, 'rf-media-dish-name',  $image_->dish_name);
// 			update_post_meta( $attach_id, 'rf-old-id',  $media_id);

// 			// update wp option if media is imported
// 			update_option( 'media_imported_'.$media_id, true );	
// 		}
// 		return $media_id;
// 	}else{
// 		return 'import failed';
// 	}
// }


// add_action('wp_ajax_actionImportMedia', 'callbackImportMedia');
// add_action('wp_ajax_nopriv_actionImportMedia', 'callbackImportMedia');
// function callbackImportMedia(){
//     $media_id = $_POST['media_id'];
//  	$imported = import_media_by_id( $media_id );
//  	$response = array(
//  		'log' => $imported,
//  	);
//  	wp_send_json($response);
//     wp_die();
// }



/**
 * Disable automatic general feed link outputting.
 */
automatic_feed_links( false );
remove_action('wp_head', 'wp_generator');

function roadfood_styles() {
	wp_enqueue_style( 'bundle', get_stylesheet_directory_uri() . '/assets/css/bundle.css');
    wp_enqueue_style( 'theme-styles', get_stylesheet_directory_uri() . '/assets/css/theme-styles.css');
	wp_enqueue_style( 'style', get_stylesheet_directory_uri() . '/style.css');
}
add_action( 'wp_enqueue_scripts', 'roadfood_styles' );

function roadfood_scripts() {
	wp_enqueue_script('jquery');
    wp_enqueue_script( 'bundle', get_template_directory_uri() . '/assets/js/bundle.js', 0, 0, true );
	//wp_enqueue_script( 'main', get_template_directory_uri() . '/js/jquery.main.js', 0, 0, true );
}
add_action( 'wp_enqueue_scripts', 'roadfood_scripts' );




